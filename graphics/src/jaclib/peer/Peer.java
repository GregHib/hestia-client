/* Peer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
package jaclib.peer;

abstract class Peer
{
	protected PeerReference reference;

	protected long a() {
		return reference.b();
	}
	
	public final boolean a(byte b) {
		if (b > -61) {
			reference = null;
		}
		return reference.a();
	}
	
	private static native void init(Class var_class);
	
	protected Peer() {
		/* empty */
	}
	
	static {
		init(jaclib.peer.PeerReference.class);
	}
}
