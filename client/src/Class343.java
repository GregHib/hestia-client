/* Class343 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class343
{
	protected Class119 aClass119_4238;
	protected float aFloat4239;
	protected Class270 aClass270_4240;
	static int anInt4241;
	static int anInt4242;
	static int anInt4243;
	protected float aFloat4244 = 1.0F;
	static int anInt4245;
	static int anInt4246;
	protected int anInt4247;
	protected int anInt4248;
	protected int anInt4249;
	protected float aFloat4250;
	protected float aFloat4251 = 1.0F;
	static int anInt4252;
	protected int anInt4253;
	protected float aFloat4254;
	static short[] aShortArray4255;
	protected float aFloat4256;
	protected int anInt4257;
	protected int anInt4258;
	static int anInt4259;
	
	static Class241 method3962(String string, int i, int i_0_, SignLink signlink) {
		if (i > -102) {
			return null;
		}
		anInt4259++;
		return Class188_Sub2_Sub1.method1910(signlink, string, "openjs", i_0_);
	}
	
	public static void method3963() {
		aShortArray4255 = null;
	}
	
	final void method3964(Buffer buffer) {
		anInt4245++;
		int i_1_ = buffer.readShort(-130546744);
		int i_2_ = buffer.readUnsignedShort(-50);
		int i_3_ = buffer.readUnsignedShort(-82);
		int i_4_ = buffer.readUnsignedShort(-92);
		Class235.anInt5127 = buffer.readShort(-130546744);
		aClass119_4238 = Class181.method1830(i_3_, i_1_, i_2_, i_4_);
	}
	
	final void method3965(Buffer buffer) {
		aFloat4244 = (float) (8 * buffer.readUnsignedByte(255)) / 255.0F;
		anInt4242++;
		aFloat4239 = (float) (8 * buffer.readUnsignedByte(255)) / 255.0F;
		aFloat4251 = (float) (8 * buffer.readUnsignedByte(255)) / 255.0F;
	}
	
	final void method3966(Buffer buffer) {
		anInt4241++;
		int i_6_ = buffer.readUnsignedByte(255);
		if (Class213.aNode_Sub27_2512.aClass320_Sub24_7317.method3773() != 1 || Class229.aGraphicsToolkit2732.e() <= 0) {
			if ((0x1 & i_6_) != 0) {
				buffer.readInt();
			}
			if ((i_6_ & 0x2) != 0) {
				buffer.readShort(-130546744);
			}
			if ((i_6_ & 0x4) != 0) {
				buffer.readShort(-130546744);
			}
			if ((0x8 & i_6_) != 0) {
				buffer.readShort(-130546744);
			}
			aFloat4250 = 1.2F;
			aFloat4254 = 1.1523438F;
			aFloat4256 = 0.69921875F;
			anInt4253 = Class42.anInt649;
		} else {
			if ((i_6_ & 0x1) == 0) {
				anInt4253 = Class42.anInt649;
			} else {
				anInt4253 = buffer.readInt();
			}
			if ((0x2 & i_6_) == 0) {
				aFloat4254 = 1.1523438F;
			} else {
				aFloat4254 = (float) buffer.readShort(-130546744) / 256.0F;
			}
			if ((0x4 & i_6_) == 0) {
				aFloat4256 = 0.69921875F;
			} else {
				aFloat4256 = (float) buffer.readShort(-130546744) / 256.0F;
			}
            if ((i_6_ & 0x8) == 0) {
				aFloat4250 = 1.2F;
			} else {
				aFloat4250 = (float) buffer.readShort(-130546744) / 256.0F;
			}
		}
		if ((0x10 & i_6_) == 0) {
			anInt4249 = -50;
			anInt4248 = -50;
			anInt4247 = -60;
		} else {
			anInt4249 = buffer.readUnsignedShort(-92);
			anInt4247 = buffer.readUnsignedShort(-30);
			anInt4248 = buffer.readUnsignedShort(-104);
		}
        if ((0x20 & i_6_) == 0) {
            anInt4257 = Class320_Sub26.anInt8456;
        } else {
            anInt4257 = buffer.readInt();
        }
        if ((i_6_ & 0x40) == 0) {
            anInt4258 = 0;
        } else {
            anInt4258 = buffer.readShort(-130546744);
        }
        if ((0x80 & i_6_) == 0) {
            aClass270_4240 = GraphicsToolkit.aClass270_1548;
        } else {
            int i_7_ = buffer.readShort(-130546744);
            int i_8_ = buffer.readShort(-130546744);
            int i_9_ = buffer.readShort(-130546744);
            int i_10_ = buffer.readShort(-130546744);
            int i_11_ = buffer.readShort(-130546744);
            int i_12_ = buffer.readShort(-130546744);
            aClass270_4240 = Class320_Sub14.method3736(i_11_, -98, i_8_, i_9_, i_10_, i_12_, i_7_);
        }
    }
	
	static boolean method3967(int i, int i_13_) {
		anInt4243++;
        return (0x40000 & i) != 0 | Class320_Sub7.method3711((byte) 76, i) || Class110.method1135(i, i_13_);
    }
	
	final boolean method3968(Class343 class343_15_) {
		anInt4246++;
		return anInt4253 == class343_15_.anInt4253 && class343_15_.aFloat4254 == aFloat4254 && aFloat4256 == class343_15_.aFloat4256 && aFloat4250 == class343_15_.aFloat4250 && class343_15_.aFloat4239 == aFloat4239 && aFloat4244 == class343_15_.aFloat4244 && aFloat4251 == class343_15_.aFloat4251 && class343_15_.anInt4257 == anInt4257 && anInt4258 == class343_15_.anInt4258 && class343_15_.aClass270_4240 == aClass270_4240 && class343_15_.aClass119_4238 == aClass119_4238;
    }
	
	public Class343() {
		aFloat4239 = 0.25F;
		anInt4247 = -60;
		aClass270_4240 = GraphicsToolkit.aClass270_1548;
		anInt4249 = -50;
		aFloat4250 = 1.2F;
		aClass119_4238 = Class263.aClass119_3340;
		anInt4253 = Class42.anInt649;
		aFloat4256 = 0.69921875F;
		aFloat4254 = 1.1523438F;
		anInt4248 = -50;
		anInt4257 = Class320_Sub26.anInt8456;
		anInt4258 = 0;
	}
	
	Class343(Buffer buffer) {
		aFloat4239 = 0.25F;
		method3966(buffer);
	}
}
