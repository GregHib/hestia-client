/* Node_Sub35 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Node_Sub35 extends Node
{
	static int anInt7412;
	protected int anInt7413;
	static int anInt7414;
	static int anInt7415;
	static int anInt7416;
	static int anInt7417;
	protected int anInt7418;
	static int anInt7419;
	static Class192 aClass192_7420 = new Class192(140, -2);
	static Class135 aClass135_7421 = new Class135();
	
	final int method2743(int i) {
		anInt7415++;
		if (i >= -20) {
			anInt7413 = -17;
		}
		return Animable_Sub4.method925(anInt7418);
	}
	
	final boolean method2744() {
		anInt7416++;
        return (0x1 & anInt7418 >> 22) != 0;
    }
	
	final int method2745() {
        anInt7414++;
		return (0x1d36c1 & anInt7418) >> 18;
	}
	
	final boolean method2746() {
        anInt7419++;
        return (0x1 & anInt7418) != 0;
    }
	
	final boolean method2747() {
        anInt7412++;
        return (0x1 & anInt7418 >> 21) != 0;
    }
	
	final boolean method2748(byte b, int i) {
		if (b >= -33) {
			return true;
		}
		anInt7417++;
        return (0x1 & anInt7418 >> i + 1) != 0;
    }
	
	public static void method2749() {
		aClass135_7421 = null;
		aClass192_7420 = null;
	}
	
	Node_Sub35(int i, int i_0_) {
		anInt7413 = i_0_;
		anInt7418 = i;
	}
}
