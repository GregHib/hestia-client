/* Class238 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class238
{
	static int anInt2910;
	static int anInt2911;
	static GLSprite[] aGLSpriteArray2912;
	static Class192 aClass192_2913 = new Class192(20, -1);
	static int anInt2914;
	static int anInt2915;
	static int anInt2916;
	
	public static void method3018() {
		aGLSpriteArray2912 = null;
		aClass192_2913 = null;
	}
	
	static void method3019(int i, int i_0_, int[] is, Object[] objects) {
		anInt2914++;
		if (i > i_0_) {
			int i_1_ = (i_0_ + i) / 2;
			int i_2_ = i_0_;
			int i_3_ = is[i_1_];
			is[i_1_] = is[i];
			is[i] = i_3_;
			Object object = objects[i_1_];
			objects[i_1_] = objects[i];
			objects[i] = object;
			int i_4_ = i_3_ != 2147483647 ? 1 : 0;
			for (int i_5_ = i_0_; i_5_ < i; i_5_++) {
				if (i_3_ + (i_5_ & i_4_) > is[i_5_]) {
					int i_6_ = is[i_5_];
					is[i_5_] = is[i_2_];
					is[i_2_] = i_6_;
					Object object_7_ = objects[i_5_];
					objects[i_5_] = objects[i_2_];
					objects[i_2_++] = object_7_;
				}
			}
			is[i] = is[i_2_];
			is[i_2_] = i_3_;
			objects[i] = objects[i_2_];
			objects[i_2_] = object;
			method3019(-1 + i_2_, i_0_, is, objects);
			method3019(i, i_2_ + 1, is, objects);
		}
	}
	
	static void method3020(Actor actor) {
		anInt2910++;
		int i_8_ = -Class174.anInt2092 + actor.anInt10892;
		int i_9_ = actor.anInt10879 * 512 + 256 * actor.method853((byte) 99);
		int i_10_ = actor.anInt10883 * 512 + (actor.method853((byte) 58) * 256);
		actor.anInt5940 += (-actor.anInt5940 + i_10_) / i_8_;
		actor.anInt5934 += (-actor.anInt5934 + i_9_) / i_8_;
		actor.anInt10901 = 0;
		if (actor.anInt10885 == 0) {
			actor.method856(8192);
		}
		if (actor.anInt10885 == 1) {
			actor.method856(12288);
		}
		if (actor.anInt10885 == 2) {
			actor.method856(0);
		}
		if (actor.anInt10885 == 3) {
			actor.method856(4096);
		}
	}
	
	static boolean method3021(int i, int i_11_) {
		anInt2916++;
		if (i_11_ < 0 || i < 0 || i_11_ >= CacheNode_Sub11.aByteArrayArrayArray9550[1].length || CacheNode_Sub11.aByteArrayArrayArray9550[1][i_11_].length <= i) {
			return false;
		}
		return (CacheNode_Sub11.aByteArrayArrayArray9550[1][i_11_][i] & 0x2) != 0;
    }
}
