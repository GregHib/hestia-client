/* Class381 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

import com.sun.management.HotSpotDiagnosticMXBean;

import java.lang.management.ManagementFactory;

public class Class381
{
	static int anInt4892;
	static int anInt4893;
	static int anInt4894 = 0;

	static synchronized void method4172() {
		anInt4892++;
		if (Class66_Sub1.anObject8988 == null) {
			try {
				Class66_Sub1.anObject8988 = ManagementFactory.newPlatformMXBeanProxy(ManagementFactory.getPlatformMBeanServer(), "com.sun.management:type=HotSpotDiagnostic", HotSpotDiagnosticMXBean.class);
			} catch (Exception exception) {
				System.out.println("HeapDump setup error:");
				exception.printStackTrace();
			}
		}
	}
	
	static void method4173(Actor actor, int i_4_) {
		Animable_Sub3.method919(actor.aByte5933, actor.anInt5940, i_4_, actor.anInt5934, 0);
		anInt4893++;
	}
}
