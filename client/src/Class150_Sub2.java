/* Class150_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class150_Sub2 extends Class150
{
	static int anInt8955;
	static int anInt8956;
	static int anInt8957;
	protected int anInt8958;
	protected int anInt8959;
	static boolean[][][] aBooleanArrayArrayArray8960;
	static int anInt8961;
	static Class299 aClass299_8962;
	
	static boolean checkRectangleInteract(int targetX, int sizeX, int currentX, int i_2_, int targetSizeX, int targetSizeY, int currentY, int targetY, int sizeY) {
		anInt8961++;
		if (targetX + targetSizeX <= currentX || targetX >= currentX + sizeX) {
			return false;
		}
		if (currentY >= targetY + targetSizeY || sizeY + currentY <= targetY) {
			return false;
		}
        return i_2_ < -12;
    }
	
	public final Class170 method20(int i) {
		anInt8956++;
		if (i <= 81) {
			anInt8958 = 62;
		}
		return Class46.aClass170_680;
	}
	
	static Class357 method1660(int i, GraphicsToolkit graphicstoolkit) {
		anInt8957++;
		Class49 class49 = Class268.method3290(true, graphicstoolkit, true, i);
		if (class49 == null) {
			return null;
		}
		return class49.aClass357_722;
	}
	
	public static void method1661() {
		aBooleanArrayArrayArray8960 = null;
		aClass299_8962 = null;
	}
	
	Class150_Sub2(Class379 class379, Class77 class77, int i, int i_9_, int i_10_, int i_11_, int i_12_, int i_13_, int i_14_, int i_15_, int i_16_) {
		super(class379, class77, i, i_9_, i_10_, i_11_, i_12_, i_13_, i_14_);
		anInt8958 = i_16_;
		anInt8959 = i_15_;
	}
}
