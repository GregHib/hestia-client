/* Class52 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.util.Random;

abstract class Class52
{
	static int anInt784;
	static int anInt785;
	static int anInt786;
	static int anInt787;
	static CacheNode_Sub13 aCacheNode_Sub13_788;
	private Class357 aClass357_789;
	static int anInt790;
	static int anInt791;
	static int anInt792;
	static int anInt793;
	static int anInt794;
	static int anInt795;
	static int anInt796;
	static int anInt797;
	static int anInt798;
	private GraphicsToolkit aGraphicsToolkit799;
	static int anInt800 = 0;
	static int anInt801;
	static int anInt802;
	static int anInt803;
	static int anInt804;
	static int anInt805;
	static int anInt806;
	
	final int method525(GLSprite[] glsprites, int i, int i_1_, String string, int[] is, Random random, int i_4_) {
		anInt792++;
		if (string == null) {
			return 0;
		}
		random.setSeed((long) i);
		int i_5_ = 192 + (random.nextInt() & 0x1f);
		method534(i_5_ << 24 | 16777215, i_5_ << 24 | 0);
		int i_6_ = string.length();
		int[] is_7_ = new int[i_6_];
		int i_8_ = 0;
		for (int i_9_ = 0; i_9_ < i_6_; i_9_++) {
			is_7_[i_9_] = i_8_;
			if ((0x3 & random.nextInt()) == 0) {
				i_8_++;
			}
		}
		method529(is_7_, string, i_1_, glsprites, i_4_, null, is);
		return i_8_;
	}
	
	abstract void method526(char c, int i, int i_10_, int i_11_, boolean bool, aa var_aa, int i_12_, int i_13_);
	
	final void method527(int i, int i_14_, int i_15_, int i_16_, int i_17_, String string) {
		anInt790++;
		if (string != null) {
			method534(i_17_, -16777216);
			double d = -((double) i / 8.0) + 7.0;
			if (d < 0.0) {
				d = 0.0;
			}
			int i_20_ = string.length();
			int[] is = new int[i_20_];
			for (int i_21_ = 0; i_20_ > i_21_; i_21_++)
				is[i_21_] = (int) (Math.sin((double) i_15_ + (double) i_21_ / 1.5) * d);
			method529(null, string, i_16_, null, -(aClass357_789.method4033(string) / 2) + i_14_, is, null);
		}
	}
	
	static void method528(int i_22_) {
		anInt793++;
		CacheNode_Sub2 cachenode_sub2 = Class320_Sub19.method3754(3, 4, (long) i_22_);
		cachenode_sub2.method2291();
	}
	
	private void method529(int[] is, String string, int i, GLSprite[] glsprites, int i_23_, int[] is_24_, int[] is_25_) {
		i -= aClass357_789.anInt4430;
		anInt786++;
		int i_26_ = -1;
		int i_27_ = -1;
		int i_28_ = 0;
		int i_29_ = string.length();
		for (int i_30_ = 0; i_29_ > i_30_; i_30_++) {
			char c = (char) (Class26.method312(string.charAt(i_30_), (byte) -81) & 0xff);
			if (c == 60) {
				i_26_ = i_30_;
			} else {
				if (c == 62 && i_26_ != -1) {
					String string_31_ = string.substring(1 + i_26_, i_30_);
					i_26_ = -1;
					switch (string_31_) {
						case "lt":
							c = '<';
							break;
						case "gt":
							c = '>';
							break;
						case "nbsp":
							c = '\u00a0';
							break;
						case "shy":
							c = '\u00ad';
							break;
						case "times":
							c = '\u00d7';
							break;
						case "euro":
							c = '\u20ac';
							break;
						case "copy":
							c = '\u00a9';
							break;
						default:
							if (!string_31_.equals("reg")) {
								if (string_31_.startsWith("img=")) {
									try {
										int i_32_;
										if (is == null) {
											i_32_ = 0;
										} else {
											i_32_ = is[i_28_];
										}
										int i_33_;
										if (is_24_ == null) {
											i_33_ = 0;
										} else {
											i_33_ = is_24_[i_28_];
										}
										i_28_++;
										int i_34_ = Class350.method3998(string_31_.substring(4), -1);
										GLSprite glsprite = glsprites[i_34_];
										int i_35_ = is_25_ == null ? glsprite.method1186() : is_25_[i_34_];
										glsprite.method1191(i_32_ + i_23_, i_33_ + -i_35_ + (i + aClass357_789.anInt4430), 1, 0, 1);
										i_27_ = -1;
										i_23_ += glsprites[i_34_].method1197();
									} catch (Exception exception) {
										/* empty */
									}
								} else {
									method540(-119, string_31_);
								}
								continue;
							}
							c = '\u00ae';
							break;
					}
                }
				if (i_26_ == -1) {
					if (i_27_ != -1) {
						i_23_ += aClass357_789.method4026(c, i_27_);
					}
					int i_36_;
					if (is == null) {
						i_36_ = 0;
					} else {
						i_36_ = is[i_28_];
					}
					int i_37_;
					if (is_24_ == null) {
						i_37_ = 0;
					} else {
						i_37_ = is_24_[i_28_];
					}
					if (c != 32) {
						if ((CacheNode_Sub4.anInt9465 & -16777216) != 0) {
							fa(c, 1 + i_23_ + i_36_, i_37_ + (i + 1), CacheNode_Sub4.anInt9465, true);
						}
						fa(c, i_23_ + i_36_, i_37_ + i, Class270_Sub2_Sub2.anInt10558, false);
					} else if (Class25.anInt446 > 0) {
						Class150_Sub3.anInt8970 += Class25.anInt446;
						i_23_ += Class150_Sub3.anInt8970 >> 8;
						Class150_Sub3.anInt8970 &= 0xff;
					}
					i_28_++;
					int i_38_ = aClass357_789.method4027(c);
					if (Class339.anInt4204 != -1) {
						aGraphicsToolkit799.method1242((int) (0.7 * (double) aClass357_789.anInt4430) + i, Class339.anInt4204, i_23_, i_38_);
					}
					if (Class370.anInt4568 != -1) {
						aGraphicsToolkit799.method1242(aClass357_789.anInt4430 + i, Class370.anInt4568, i_23_, i_38_);
					}
					i_23_ += i_38_;
					i_27_ = c;
				}
			}
		}
	}
	
	static Node_Sub38 method530(int i) {
		anInt803++;
		while_64_:
		do {
		while_63_:
			do {
			while_62_:
				do {
				while_61_:
					do {
					while_60_:
						do {
						while_59_:
							do {
							while_58_:
								do {
								while_57_:
									do {
									while_56_:
										do {
										while_55_:
											do {
											while_54_:
												do {
												while_53_:
													do {
													while_52_:
														do {
														while_51_:
															do {
															while_50_:
																do {
																while_49_:
																	do {
																	while_48_:
																		do {
																		while_47_:
																			do {
																			while_46_:
																				do {
																				while_45_:
																					do {
																					while_44_:
																						do {
																						while_43_:
																							do {
																							while_42_:
																								do {
																								while_41_:
																									do {
																									while_40_:
																										do {
																										while_39_:
																											do {
																											while_38_:
																												do {
																												while_37_:
																													do {
																													while_36_:
																														do {
																														while_35_:
																															do {
																															while_34_:
																																do {
																																while_33_:
																																	do {
																																	while_32_:
																																		do {
																																		while_31_:
																																			do {
																																			while_30_:
																																				do {
																																				while_29_:
																																					do {
																																					while_28_:
																																						do {
																																						while_27_:
																																							do {
																																								do {
																																									if (i == 0) {
																																										return new Node_Sub38_Sub17();
																																									} else if (i != 1) {
																																										if (i == 2) {
																																											break;
																																										} else if (i != 3) {
																																											if (i == 4) {
																																												break while_28_;
																																											} else if (i != 5) {
																																												if (i == 6) {
																																													break while_30_;
																																												} else if (i != 7) {
																																													if (i == 8) {
																																														break while_32_;
																																													} else if (i != 9) {
																																														if (i == 10) {
																																															break while_34_;
																																														} else if (i != 11) {
																																															if (i == 12) {
																																																break while_36_;
																																															} else if (i != 13) {
																																																if (i == 14) {
																																																	break while_38_;
																																																} else if (i != 15) {
																																																	if (i == 16) {
																																																		break while_40_;
																																																	} else if (i != 17) {
																																																		if (i == 18) {
																																																			break while_42_;
																																																		} else if (i != 19) {
																																																			if (i == 20) {
																																																				break while_44_;
																																																			} else if (i != 21) {
																																																				if (i == 22) {
																																																					break while_46_;
																																																				} else if (i != 23) {
																																																					if (i == 24) {
																																																						break while_48_;
																																																					} else if (i != 25) {
																																																						if (i == 26) {
																																																							break while_50_;
																																																						} else if (i != 27) {
																																																							if (i == 28) {
																																																								break while_52_;
																																																							} else if (i != 29) {
																																																								if (i == 30) {
																																																									break while_54_;
																																																								} else if (i != 31) {
																																																									if (i == 32) {
																																																										break while_56_;
																																																									} else if (i != 33) {
																																																										if (i == 34) {
																																																											break while_58_;
																																																										} else if (i != 35) {
																																																											if (i == 36) {
																																																												break while_60_;
																																																											} else if (i != 37) {
																																																												if (i == 38) {
																																																													break while_62_;
																																																												} else if (i != 39) {
																																																													break while_64_;
																																																												}
																																																												break while_63_;
																																																											}
																																																											break while_61_;
																																																										}
																																																										break while_59_;
																																																									}
																																																									break while_57_;
																																																								}
																																																								break while_55_;
																																																							}
																																																							break while_53_;
																																																						}
																																																						break while_51_;
																																																					}
																																																					break while_49_;
																																																				}
																																																				break while_47_;
																																																			}
																																																			break while_45_;
																																																		}
																																																		break while_43_;
																																																	}
																																																	break while_41_;
																																																}
																																																break while_39_;
																																															}
																																															break while_37_;
																																														}
																																														break while_35_;
																																													}
																																													break while_33_;
																																												}
																																												break while_31_;
																																											}
																																											break while_29_;
																																										}
																																										break while_27_;
																																									}
                                                                                                                                                                    return new Node_Sub38_Sub26();
																																								} while (false);
																																								return new Node_Sub38_Sub33();
																																							} while (false);
																																							return new Node_Sub38_Sub25();
																																						} while (false);
																																						return new Node_Sub38_Sub11();
																																					} while (false);
																																					return new Node_Sub38_Sub12();
																																				} while (false);
																																				return new Node_Sub38_Sub35();
																																			} while (false);
																																			return new Node_Sub38_Sub18();
																																		} while (false);
																																		return new Node_Sub38_Sub37();
																																	} while (false);
																																	return new Node_Sub38_Sub38();
																																} while (false);
																																return new Node_Sub38_Sub14();
																															} while (false);
																															return new Node_Sub38_Sub3();
																														} while (false);
																														return new Node_Sub38_Sub4();
																													} while (false);
																													return new Node_Sub38_Sub34();
																												} while (false);
																												return new Node_Sub38_Sub29();
																											} while (false);
																											return new Node_Sub38_Sub23();
																										} while (false);
																										return new Node_Sub38_Sub39();
																									} while (false);
																									return new Node_Sub38_Sub9();
																								} while (false);
																								return new Node_Sub38_Sub8_Sub1();
																							} while (false);
																							return new Node_Sub38_Sub10();
																						} while (false);
																						return new Node_Sub38_Sub30();
																					} while (false);
																					return new Node_Sub38_Sub2();
																				} while (false);
																				return new Node_Sub38_Sub16();
																			} while (false);
																			return new Node_Sub38_Sub31();
																		} while (false);
																		return new Node_Sub38_Sub21();
																	} while (false);
																	return new Node_Sub38_Sub5();
																} while (false);
																return new Node_Sub38_Sub6();
															} while (false);
															return new Node_Sub38_Sub28();
														} while (false);
														return new Node_Sub38_Sub7();
													} while (false);
													return new Node_Sub38_Sub24();
												} while (false);
												return new Node_Sub38_Sub13();
											} while (false);
											return new Node_Sub38_Sub36();
										} while (false);
										return new Node_Sub38_Sub32();
									} while (false);
									return new Node_Sub38_Sub19();
								} while (false);
								return new Node_Sub38_Sub27();
							} while (false);
							return new Node_Sub38_Sub22();
						} while (false);
						return new Node_Sub38_Sub1();
					} while (false);
					return new Node_Sub38_Sub20();
				} while (false);
				return new Node_Sub38_Sub15();
			} while (false);
			return new Node_Sub38_Sub8();
		} while (false);
		return null;
	}
	
	private void method531(int i, String string) {
		anInt796++;
		int i_41_ = 0;
		boolean bool = false;
		for (int i_43_ = 0; i_43_ < string.length(); i_43_++) {
			char c = string.charAt(i_43_);
			if (c == '<') {
				bool = true;
			} else if (c == '>') {
				bool = false;
			} else if (!bool && c == ' ') {
                i_41_++;
            }
        }
		if (i_41_ > 0) {
			Class25.anInt446 = (i - aClass357_789.method4033(string) << 8) / i_41_;
		}
	}
	
	public static void method532() {
		aCacheNode_Sub13_788 = null;
	}
	
	private void method533(GLSprite[] glsprites, int i, int i_44_, String string, int i_45_, aa var_aa, int i_46_, int[] is) {
		i_46_ -= aClass357_789.anInt4430;
		anInt806++;
		int i_47_ = -1;
		int i_48_ = -1;
		int i_49_ = string.length();
		for (int i_50_ = 0; i_50_ < i_49_; i_50_++) {
			char c = (char) (0xff & Class26.method312(string.charAt(i_50_), (byte) -54));
			if (c == 60) {
				i_47_ = i_50_;
			} else {
				if (c == 62 && i_47_ != -1) {
					String string_51_ = string.substring(1 + i_47_, i_50_);
					i_47_ = -1;
					switch (string_51_) {
						case "lt":
							c = '<';
							break;
						case "gt":
							c = '>';
							break;
						case "nbsp":
							c = '\u00a0';
							break;
						case "shy":
							c = '\u00ad';
							break;
						case "times":
							c = '\u00d7';
							break;
						case "euro":
							c = '\u20ac';
							break;
						case "copy":
							c = '\u00a9';
							break;
						default:
							if (!string_51_.equals("reg")) {
								if (string_51_.startsWith("img=")) {
									try {
										int i_52_ = Class350.method3998(string_51_.substring(4), -1);
										GLSprite glsprite = glsprites[i_52_];
										int i_53_ = is != null ? is[i_52_] : glsprite.method1186();
										if ((Class270_Sub2_Sub2.anInt10558 & -16777216) == -16777216) {
											glsprite.method1191(i_45_, -i_53_ + (aClass357_789.anInt4430 + i_46_), 1, 0, 1);
										} else {
											glsprite.method1191(i_45_, -i_53_ + i_46_ + aClass357_789.anInt4430, 0, Class270_Sub2_Sub2.anInt10558 & -16777216 | 0xffffff, 1);
										}
										i_48_ = -1;
										i_45_ += glsprites[i_52_].method1197();
									} catch (Exception exception) {
										/* empty */
									}
								} else {
									method540(-66, string_51_);
								}
								continue;
							}
							c = '\u00ae';
							break;
					}
                }
				if (i_47_ == -1) {
					if (i_48_ != -1) {
						i_45_ += aClass357_789.method4026(c, i_48_);
					}
					if (c == 32) {
						if (Class25.anInt446 > 0) {
							Class150_Sub3.anInt8970 += Class25.anInt446;
							i_45_ += Class150_Sub3.anInt8970 >> 8;
							Class150_Sub3.anInt8970 &= 0xff;
						}
					} else if (var_aa == null) {
						if ((CacheNode_Sub4.anInt9465 & -16777216) != 0) {
							fa(c, 1 + i_45_, i_46_ + 1, CacheNode_Sub4.anInt9465, true);
						}
						fa(c, i_45_, i_46_, Class270_Sub2_Sub2.anInt10558, false);
					} else {
						if ((CacheNode_Sub4.anInt9465 & -16777216) != 0) {
							method526(c, 1 + i_45_, i_46_ + 1, CacheNode_Sub4.anInt9465, true, var_aa, i_44_, i);
						}
						method526(c, i_45_, i_46_, Class270_Sub2_Sub2.anInt10558, false, var_aa, i_44_, i);
					}
					int i_54_ = aClass357_789.method4027(c);
					if (Class339.anInt4204 != -1) {
						aGraphicsToolkit799.method1242(i_46_ + (int) (0.7 * (double) aClass357_789.anInt4430), Class339.anInt4204, i_45_, i_54_);
					}
					if (Class370.anInt4568 != -1) {
						aGraphicsToolkit799.method1242(1 + (aClass357_789.anInt4430 + i_46_), Class370.anInt4568, i_45_, i_54_);
					}
					i_48_ = c;
					i_45_ += i_54_;
				}
			}
		}
	}
	
	private void method534(int i_55_, int i_56_) {
		Class25.anInt446 = 0;
		Class270_Sub2_Sub2.anInt10558 = Node_Sub25_Sub4.anInt10011 = i_55_;
		anInt784++;
		Class150_Sub3.anInt8970 = 0;
		Class370.anInt4568 = -1;
		Class339.anInt4204 = -1;
		if (i_56_ == -1) {
			i_56_ = 0;
		}
		CacheNode_Sub4.anInt9465 = Node_Sub25_Sub4.anInt10009 = i_56_;
	}
	
	final int method535(int i, int i_57_, String string, aa var_aa, int i_58_, int i_59_, int i_60_, int i_61_, int i_62_, int i_63_, int i_64_, int i_65_, int i_66_, int i_67_) {
		anInt798++;
		if (i_60_ <= 70) {
			method536(98, 77, -16, null, 105, 89, null, null);
		}
		return method539(i_64_, 2, i_57_, i_59_, null, 0, i, i_58_, i_67_, i_61_, var_aa, i_65_, i_66_, i_63_, i_62_, string);
	}
	
	abstract void fa(char c, int i, int i_68_, int i_69_, boolean bool);
	
	final void method536(int i, int i_70_, int i_71_, String string, int i_72_, int i_73_, GLSprite[] glsprites, int[] is) {
		if (i_72_ == -238946248) {
			anInt794++;
			if (string != null) {
				method534(i, i_70_);
				method533(glsprites, 0, 0, string, i_73_, null, i_71_, is);
			}
		}
	}
	
	final void method537(int i, byte b, int i_74_, String string, int i_75_, int i_76_) {
		if (b >= 95) {
			anInt785++;
			if (string != null) {
				method534(i_76_, i);
				method533(null, 0, 0, string, -(aClass357_789.method4033(string) / 2) + i_74_, null, i_75_, null);
			}
		}
	}
	
	final void method538(int i, int i_77_, String string, int i_78_, int i_80_) {
		anInt804++;
		if (string != null) {
			method534(i_80_, i_78_);
			method533(null, 0, 0, string, i, null, i_77_, null);
		}
	}
	
	final int method539(int i, int i_81_, int i_82_, int i_83_, GLSprite[] glsprites, int i_84_, int i_85_, int i_86_, int i_87_, int i_88_, aa var_aa, int i_89_, int i_90_, int i_91_, int i_92_, String string) {
		anInt795++;
		if (string == null) {
			return 0;
		}
		if (i_81_ != 2) {
			method539(-69, 108, -103, 49, null, 53, -66, 62, 11, 81, null, -110, -87, -20, 114, null);
		}
		method534(i_82_, i_83_);
		if (i_89_ == 0) {
			i_89_ = aClass357_789.anInt4430;
		}
		int[] is_93_;
		if (i_87_ < i_89_ + (aClass357_789.anInt4442 + aClass357_789.anInt4434) && i_89_ + i_89_ > i_87_) {
			is_93_ = null;
		} else {
			is_93_ = new int[] { i_90_ };
		}
        int i_94_ = aClass357_789.method4029(Class9.aStringArray167, is_93_, glsprites, string);
		if (i_84_ == -1) {
			i_84_ = i_87_ / i_89_;
			if (i_84_ <= 0) {
				i_84_ = 1;
			}
		}
		if (i_84_ > 0 && i_94_ >= i_84_) {
			i_94_ = i_84_;
			Class9.aStringArray167[i_84_ - 1] = aClass357_789.method4032(Class9.aStringArray167[i_84_ + -1], glsprites, i_90_, i_81_ + -3);
		}
		if (i == 3 && i_94_ == 1) {
			i = 1;
		}
		int i_95_;
		if (i == 0) {
			i_95_ = aClass357_789.anInt4434 + i_86_;
		} else if (i == 1) {
			i_95_ = (-aClass357_789.anInt4442 + -aClass357_789.anInt4434 + (i_87_ - i_89_ * (-1 + i_94_))) / 2 + aClass357_789.anInt4434 + i_86_;
		} else if (i == 2) {
			i_95_ = -(i_89_ * (i_94_ - 1)) + (-aClass357_789.anInt4442 + (i_87_ + i_86_));
		} else {
			int i_96_ = (-aClass357_789.anInt4434 + i_87_ + -aClass357_789.anInt4442 - i_89_ * (i_94_ - 1)) / (1 + i_94_);
			if (i_96_ < 0) {
				i_96_ = 0;
			}
			i_89_ += i_96_;
			i_95_ = i_86_ + (aClass357_789.anInt4434 + i_96_);
		}
        for (int i_97_ = 0; i_97_ < i_94_; i_97_++) {
			if (i_92_ == 0) {
				method533(glsprites, i_85_, i_88_, Class9.aStringArray167[i_97_], i_91_, var_aa, i_95_, null);
			} else if (i_92_ == 1) {
				method533(glsprites, i_85_, i_88_, Class9.aStringArray167[i_97_], i_91_ + ((i_90_ - aClass357_789.method4033(Class9.aStringArray167[i_97_])) / 2), var_aa, i_95_, null);
			} else if (i_92_ == 2) {
				method533(glsprites, i_85_, i_88_, Class9.aStringArray167[i_97_], i_91_ + (i_90_ + -aClass357_789.method4033(Class9.aStringArray167[i_97_])), var_aa, i_95_, null);
			} else if (i_94_ - 1 == i_97_) {
				method533(glsprites, i_85_, i_88_, Class9.aStringArray167[i_97_], i_91_, var_aa, i_95_, null);
			} else {
				method531(i_90_, Class9.aStringArray167[i_97_]);
				method533(glsprites, i_85_, i_88_, Class9.aStringArray167[i_97_], i_91_, var_aa, i_95_, null);
				Class25.anInt446 = 0;
			}
            i_95_ += i_89_;
		}
		return i_94_;
	}
	
	private void method540(int i, String string) {
		try {
			if (string.startsWith("col=")) {
				Class270_Sub2_Sub2.anInt10558 = Class270_Sub2_Sub2.anInt10558 & -16777216 | Class10.method187(string.substring(4)) & 0xffffff;
			} else if (string.equals("/col")) {
				Class270_Sub2_Sub2.anInt10558 = Class270_Sub2_Sub2.anInt10558 & -16777216 | Node_Sub25_Sub4.anInt10011 & 0xffffff;
			}
			if (string.startsWith("argb=")) {
				Class270_Sub2_Sub2.anInt10558 = Class10.method187(string.substring(5));
			} else if (string.equals("/argb")) {
                Class270_Sub2_Sub2.anInt10558 = Node_Sub25_Sub4.anInt10011;
            } else if (string.startsWith("str=")) {
                Class339.anInt4204 = -16777216 & Class270_Sub2_Sub2.anInt10558 | Class10.method187(string.substring(4));
            } else if (string.equals("str")) {
                Class339.anInt4204 = Class270_Sub2_Sub2.anInt10558 & -16777216 | 0x800000;
            } else if (string.equals("/str")) {
                Class339.anInt4204 = -1;
            } else if (string.startsWith("u=")) {
                Class370.anInt4568 = -16777216 & Class270_Sub2_Sub2.anInt10558 | Class10.method187(string.substring(2));
            } else if (string.equals("u")) {
                Class370.anInt4568 = Class270_Sub2_Sub2.anInt10558 & -16777216;
            } else if (string.equals("/u")) {
                Class370.anInt4568 = -1;
            } else if (string.equalsIgnoreCase("shad=-1")) {
                CacheNode_Sub4.anInt9465 = 0;
            } else if (string.startsWith("shad=")) {
                CacheNode_Sub4.anInt9465 = Class270_Sub2_Sub2.anInt10558 & -16777216 | Class10.method187(string.substring(5));
            } else if (string.equals("shad")) {
                CacheNode_Sub4.anInt9465 = -16777216 & Class270_Sub2_Sub2.anInt10558;
            } else if (string.equals("/shad")) {
                CacheNode_Sub4.anInt9465 = Node_Sub25_Sub4.anInt10009;
            } else if (string.equals("br")) {
                method534(Node_Sub25_Sub4.anInt10011, Node_Sub25_Sub4.anInt10009);
            }
        } catch (Exception exception) {
			/* empty */
		}
		anInt797++;
		if (i >= -23) {
			method536(-98, 105, -36, null, 25, 51, null, null);
		}
	}
	
	final void method541(int i_98_, String string, int i_99_, int i_100_, int i_101_) {
		anInt805++;
		if (string != null) {
			method534(i_100_, -16777216);
			int i_103_ = string.length();
			int[] is = new int[i_103_];
			int[] is_104_ = new int[i_103_];
			for (int i_105_ = 0; i_105_ < i_103_; i_105_++) {
				is[i_105_] = (int) (5.0 * Math.sin((double) i_105_ / 5.0 + (double) i_98_ / 5.0));
				is_104_[i_105_] = (int) (5.0 * Math.sin((double) i_105_ / 3.0 + (double) i_98_ / 5.0));
			}
			method529(is, string, i_101_, null, -(aClass357_789.method4033(string) / 2) + i_99_, is_104_, null);
		}
	}
	
	final void method542(int i_106_, int i_107_, String string, int i_108_, int i_109_) {
		anInt787++;
		if (string != null) {
			method534(i_106_, -16777216);
			int i_111_ = string.length();
			int[] is = new int[i_111_];
			for (int i_112_ = 0; i_111_ > i_112_; i_112_++)
				is[i_112_] = (int) (5.0 * Math.sin((double) i_112_ / 2.0 + (double) i_107_ / 5.0));
			method529(null, string, i_109_, null, i_108_ - aClass357_789.method4033(string) / 2, is, null);
		}
	}
	
	final void method543(int i, String string, int i_113_, int i_114_, int i_115_) {
		anInt802++;
		if (string != null) {
			method534(i_113_, i_114_);
			method533(null, 0, 0, string, i + -aClass357_789.method4033(string), null, i_115_, null);
		}
	}
	
	static void method544(int i, int i_118_) {
		anInt791++;
		za_Sub2.anInt10513 = i_118_;
		Node_Sub25_Sub1.anInt9936 = i;
		Class262_Sub4.anInt7722 = 0;
		Class384.anInt4906 = 0;
	}
	
	final int method545(int[] is, int i, int i_120_, int[] is_122_, int i_123_, int i_124_, Random random, String string, int i_125_, int i_126_, GLSprite[] glsprites, int i_127_, int i_128_, int i_129_) {
		anInt801++;
		if (string == null) {
			return 0;
		}
		random.setSeed((long) i_127_);
		int i_130_ = 192 + (0x1f & random.nextInt());
		method534(i_130_ << 24 | 0xffffff & i_123_, i_130_ << 24 | i_126_ & 0xffffff);
		int i_131_ = string.length();
		int[] is_132_ = new int[i_131_];
		int i_133_ = 0;
		for (int i_134_ = 0; i_134_ < i_131_; i_134_++) {
			is_132_[i_134_] = i_133_;
			if ((0x3 & random.nextInt()) == 0) {
				i_133_++;
			}
		}
		int i_135_ = i_125_;
		int i_136_ = aClass357_789.anInt4434 + i_128_;
		if (i_129_ == 1) {
			i_136_ += (-aClass357_789.anInt4434 + i_124_ + -aClass357_789.anInt4442) / 2;
		} else if (i_129_ == 2) {
			i_136_ = i_128_ + i_124_ + -aClass357_789.anInt4442;
		}
        int i_137_ = -1;
		if (i == 1) {
			i_137_ = i_133_ + aClass357_789.method4033(string);
			i_135_ += (-i_137_ + i_120_) / 2;
		} else if (i == 2) {
			i_137_ = i_133_ + aClass357_789.method4033(string);
			i_135_ += i_120_ - i_137_;
		}
        method529(is_132_, string, i_136_, glsprites, i_135_, null, is_122_);
		if (is != null) {
			if (i_137_ == -1) {
				i_137_ = i_133_ + aClass357_789.method4033(string);
			}
			is[3] = aClass357_789.anInt4442 + aClass357_789.anInt4434;
			is[0] = i_135_;
			is[2] = i_137_;
			is[1] = -aClass357_789.anInt4434 + i_136_;
		}
		return i_133_;
	}
	
	Class52(GraphicsToolkit graphicstoolkit, Class357 class357) {
		aGraphicsToolkit799 = graphicstoolkit;
		aClass357_789 = class357;
	}
}
