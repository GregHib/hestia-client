/* Class20_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class20_Sub1 extends Class20
{
	static int anInt5506;
	static float[] aFloatArray5507 = new float[2];
	static HashTable aHashTable5508;
	static Class343 aClass343_5509;
	static int[] anIntArray5510 = new int[4];
	static int anInt5511;
	static Class192 aClass192_5512;
	
	public static void method293() {
		aClass192_5512 = null;
		aClass343_5509 = null;
		aHashTable5508 = null;
		anIntArray5510 = null;
		aFloatArray5507 = null;
	}
	
	static char method294(byte b, byte b_0_) {
		if (b_0_ <= 104) {
			anIntArray5510 = null;
		}
		anInt5506++;
		int i = 0xff & b;
		if (i == 0) {
			throw new IllegalArgumentException("Non cp1252 character 0x" + Integer.toString(i, 16) + " provided");
		}
		if (i >= 128 && i < 160) {
			int i_1_ = Class204.aCharArray2455[-128 + i];
			if (i_1_ == 0) {
				i_1_ = 63;
			}
			i = i_1_;
		}
		return (char) i;
	}
	
	static void method295() {
		Class270_Sub2_Sub1.anInt10543 = 0;
		Node_Sub38_Sub6.anInt10132 = 0;
		anInt5511++;
		Plane.anInt3423++;
		Class262_Sub10.method3173();
		Class91.method1035();
		Class284.method3392();//decode mob masks
		boolean bool = false;
		for (int i_2_ = 0; i_2_ < Class270_Sub2_Sub1.anInt10543; i_2_++) {//Removed mob count
			int i_3_ = FileOnDisk.anIntArray1322[i_2_];//Removed mob indices
			Node_Sub41 node_sub41 = (Node_Sub41) Class12.aHashTable187.method1518((long) i_3_);
			Npc npc = node_sub41.aNpc7518;
			if (npc.anInt10880 != Plane.anInt3423) {//lastUpdate
				if (Class213.aBoolean2510 && Node_Sub23_Sub1.method2640(i_3_)) {//Menu option
					Class260.method3137();
				}
				if (npc.aNpcDefinition11122.method2998()) {
					Node_Sub38_Sub4.method2799(-125, npc);
				}
				npc.method879(null);
				node_sub41.method2160((byte) 86);
				bool = true;
			}
		}
		if (bool) {
			Node_Sub32.anInt7380 = Class12.aHashTable187.method1519();
			Class12.aHashTable187.method1523((byte) -112, Class314.aNode_Sub41Array4017);
		}
		if (Class218.aClass123_2566.anInt1581 != Class218.aClass123_2566.aPacket1570.anInt7002) {
			throw new RuntimeException("gnp1 pos:" + Class218.aClass123_2566.aPacket1570.anInt7002 + " psize:" + Class218.aClass123_2566.anInt1581);
		}
		for (int i_4_ = 0; Node_Sub25_Sub3.anInt9987 > i_4_; i_4_++) {
			if (Class12.aHashTable187.method1518((long) Class54.anIntArray816[i_4_]) == null) {
				throw new RuntimeException("gnp2 pos:" + i_4_ + " size:" + Node_Sub25_Sub3.anInt9987);
			}
		}
		if (-Node_Sub25_Sub3.anInt9987 + Node_Sub32.anInt7380 != 0) {
			throw new RuntimeException("gnp3 mis:" + (Node_Sub32.anInt7380 - Node_Sub25_Sub3.anInt9987));
		}
		for (int i_5_ = 0; Node_Sub32.anInt7380 > i_5_; i_5_++) {
			if (Plane.anInt3423 != Class314.aNode_Sub41Array4017[i_5_].aNpc7518.anInt10880) {
				throw new RuntimeException("gnp4 uk:" + Class314.aNode_Sub41Array4017[i_5_].aNpc7518.anInt10858);
			}
		}
	}
	
	static {
		aHashTable5508 = new HashTable(16);
		aClass192_5512 = new Class192(124, 1);
	}
}
