/* Class377 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

abstract class Class377
{
	static Class318 aClass318_4663;
	static int anInt4664 = 0;
	static int anInt4665;
	static int anInt4666;
	static int anInt4667;
	static int anInt4668;
	static int anInt4669;
	static int anInt4670;
	protected int anInt4671;
	static int anInt4672;
	protected int anInt4673;
	protected int anInt4674;
	static Class379 aClass379_4675;
	
	final boolean method4120() {
		anInt4669++;
        return (anInt4673 & 0x2) != 0;
    }
	
	public static void method4121() {
		aClass318_4663 = null;
		aClass379_4675 = null;
	}
	
	final boolean method4122() {
		anInt4665++;
        return (anInt4673 & 0x4) != 0;
    }
	
	final boolean method4123() {
		anInt4668++;
		return (anInt4673 & 0x1) != 0;
    }
	
	static void method4124(Widget widget, Widget widget_1_) {
		anInt4667++;
		Class3.anInt5162++;
		Node_Sub13 node_sub13 = FloatBuffer.method2250(-386, Class357.aClass318_4441, Class218.aClass123_2566.anIsaacCipher1571);
		node_sub13.aPacket7113.writeShort(widget_1_.anInt4718, -109);
		node_sub13.aPacket7113.writeShortLittle(widget.anInt4687);
		node_sub13.aPacket7113.writeShortAdd(widget.anInt4718);
		node_sub13.aPacket7113.writeIntMiddle(true, widget.anInt4822);
		node_sub13.aPacket7113.writeShortLittle(widget_1_.anInt4687);
		node_sub13.aPacket7113.writeIntInverseMiddle(widget_1_.anInt4822);
		Class218.aClass123_2566.method1514(126, node_sub13);
	}
	
	static void method4125() {
		anInt4670++;
		for (EntityNode_Sub1 entitynode_sub1 = (EntityNode_Sub1) Class82.aClass103_1120.method1106(68); entitynode_sub1 != null; entitynode_sub1 = (EntityNode_Sub1) Class82.aClass103_1120.method1106(35))
			Class373.method4108(entitynode_sub1);
		int i_2_;
		int i_3_;
		if (Class213.aNode_Sub27_2512.aClass320_Sub19_7301.method3751() == 1) {
			i_2_ = 0;
			i_3_ = 3;
		} else {
			i_2_ = i_3_ = Class94.anInt1249;
		}
		if (aa.anInt101 == 3) {
			for (int i_4_ = i_2_; i_4_ <= i_3_; i_4_++)
				client.method106(i_4_);
			client.method104();
		} else {
			client.method107();
			for (int i_5_ = i_2_; i_5_ <= i_3_; i_5_++) {
				client.method117();
				client.method122(i_5_);
				client.method106(i_5_);
			}
			client.method116();
			client.method104();
		}
	}
	
	final boolean method4126() {
		anInt4666++;
        return (0x8 & anInt4673) != 0;
    }
	
	public Class377() {
		/* empty */
	}
	
	static boolean method4127() {
		anInt4672++;
		return false;
	}
	
	static {
		aClass318_4663 = new Class318(52, 8);
		aClass379_4675 = new Class379();
	}
}
