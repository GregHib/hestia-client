/* Callback_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import com.ms.awt.WComponentPeer;
import com.ms.dll.Callback;
import com.ms.dll.Root;
import com.ms.win32.User32;

import java.awt.*;

public class Callback_Sub1 extends Callback
{
	private volatile int anInt1;
	private volatile int anInt2;
	private volatile boolean aBoolean3 = true;
	private int anInt4;
	private boolean aBoolean5;
	
	final synchronized int method80(int i, int i_0_, int i_1_, int i_2_) {
		if (anInt2 != i) {
			int i_3_ = User32.GetWindowLong(i, -4);
			return User32.CallWindowProc(i_3_, i, i_0_, i_1_, i_2_);
		}
		if (i_0_ == 32) {
			int i_4_ = i_2_ & 0xffff;
			if (i_4_ == 1) {
				User32.SetCursor(aBoolean3 ? anInt4 : 0);
				return 0;
			}
		}
		if (i_0_ == 101024) {
			User32.SetCursor(aBoolean3 ? anInt4 : 0);
			return 0;
		}
		if (i_0_ == 1) {
			anInt2 = 0;
			aBoolean3 = true;
		}
		return User32.CallWindowProc(anInt1, i, i_0_, i_1_, i_2_);
	}
	
	final void method81(int i, int i_6_) {
		User32.SetCursorPos(i, i_6_);
	}
	
	final void method82(boolean bool, Component component) {
		WComponentPeer wcomponentpeer = (WComponentPeer) component.getPeer();
		int i = wcomponentpeer.getTopHwnd();
		if (i != anInt2 || bool != aBoolean3) {
			if (!aBoolean5) {
				anInt4 = User32.LoadCursor(0, 32512);
				Root.alloc(this);
				aBoolean5 = true;
			}
			if (i != anInt2) {
				if (anInt2 != 0) {
					aBoolean3 = true;
					User32.SendMessage(i, 101024, 0, 0);
					synchronized (this) {
						User32.SetWindowLong(anInt2, -4, anInt1);
					}
				}
				synchronized (this) {
					anInt2 = i;
					anInt1 = User32.SetWindowLong(anInt2, -4, (Object) this);
				}
			}
			aBoolean3 = bool;
			User32.SendMessage(i, 101024, 0, 0);
		}
	}
	
	Callback_Sub1() {
		/* empty */
	}
}
