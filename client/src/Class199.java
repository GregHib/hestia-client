/* Class199 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.lang.reflect.Field;

public class Class199
{
	private int[] anIntArray2430;
	static int anInt2431;
	static int anInt2432;
	static int anInt2433;
	static int anInt2434;
	private int[] anIntArray2435;
	static int anInt2436;

	static int method2006() {
		anInt2431++;
		int i = 0;
		for (Field field : Node_Sub27.class.getDeclaredFields()) {
			if (Class320.class.isAssignableFrom(field.getType())) {
				i++;
			}
		}
		return i + 1;
	}
	
	static void method2007(int i, byte[] bs, int i_3_, int i_5_, byte b) {
		anInt2434++;
		if (i_5_ > i) {
			i_3_ += i;
			int i_4_ = -i + i_5_ >> 2;
			if (b > 92) {
				while (--i_4_ >= 0) {
					bs[i_3_++] = (byte) 1;
					bs[i_3_++] = (byte) 1;
					bs[i_3_++] = (byte) 1;
					bs[i_3_++] = (byte) 1;
				}
				i_4_ = -i + i_5_ & 0x3;
				while (--i_4_ >= 0)
					bs[i_3_++] = (byte) 1;
			}
		}
	}
	
	final void method2008(Class206 class206, int i_6_) {
		anInt2432++;
		int i_7_ = anIntArray2430[0];
		class206.method2034(i_7_ >>> 16, i_6_, i_7_ & 0xffff);
		Actor actor = class206.method2037(-76);
		actor.anInt10904 = 0;
		for (int i_8_ = -1 + anIntArray2435.length; i_8_ >= 0; i_8_--) {
			int i_9_ = anIntArray2435[i_8_];
			int i_10_ = anIntArray2430[i_8_];
			actor.anIntArray10910[actor.anInt10904] = i_10_ >> 16;
			actor.anIntArray10908[actor.anInt10904] = Node_Sub30.method2723(65535, i_10_);
			byte b = 1;
			if (i_9_ == 0) {
				b = (byte) 0;
			} else if (i_9_ == 2) {
				b = (byte) 2;
			}
			actor.aByteArray10905[actor.anInt10904] = b;
			actor.anInt10904++;
		}
	}
	
	static void method2009(int i, Class312 class312, GraphicsToolkit graphicstoolkit) {
		if (i != -9404) {
			method2006();
		}
		Node_Sub43.aClass312_7541.method3614(i + 8803);
		anInt2436++;
		if (!CacheNode_Sub9.aBoolean9510) {
			for (Node_Sub14 node_sub14 = (Node_Sub14) class312.method3613(65280); node_sub14 != null; node_sub14 = (Node_Sub14) class312.method3620(16776960)) {
				Class79 class79 = Class20.aClass215_322.method2069((byte) 122, node_sub14.anInt7128);
				if (Class220.method2099(class79)) {
					boolean bool = Class339_Sub4.method3933(graphicstoolkit, 0, class79, node_sub14, 0);
					if (bool) {
						Class277_Sub1.method3360(node_sub14, graphicstoolkit, class79);
					}
				}
			}
		}
	}
	
	static void method2010(byte b) {
		if (b > 117) {
			Class23.anInt434 = 0;
			anInt2433++;
			Node_Sub38_Sub19.aClass78Array10284 = new Class78[50];
		}
	}
	
	Class199(Buffer buffer) {
		int i = buffer.readSmart();
		anIntArray2430 = new int[i];
		anIntArray2435 = new int[i];
		for (int i_13_ = 0; i_13_ < i; i_13_++) {
			int i_14_ = buffer.readUnsignedByte(255);
			anIntArray2435[i_13_] = i_14_;
			int i_15_ = buffer.readShort(-130546744);
			int i_16_ = buffer.readShort(-130546744);
			anIntArray2430[i_13_] = i_16_ + (i_15_ << 16);
		}
	}
}
