/* Node_Sub38_Sub39 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Node_Sub38_Sub39 extends Node_Sub38
{
	static int anInt10494;
	private int anInt10495 = 1;
	private int anInt10496 = 204;
	static int anInt10497;
	private int anInt10498 = 1;
	static Class192 aClass192_10499 = new Class192(122, 10);
	static Class302 aClass302_10500;
	
	final void method2780(boolean bool, Buffer buffer, int i) {
		while_269_:
		do {
			do {
				if (i == 0) {
					anInt10498 = buffer.readUnsignedByte(255);
					break while_269_;
				} else if (i != 1) {
                    if (i == 2) {
                        break;
                    }
                    break while_269_;
                }
                anInt10495 = buffer.readUnsignedByte(255);
				break while_269_;
			} while (false);
			anInt10496 = buffer.readShort(-130546744);
		} while (false);
		anInt10494++;
		if (bool) {
			method2917();
		}
	}
	
	public static void method2917() {
		aClass192_10499 = null;
		aClass302_10500 = null;
	}
	
	public Node_Sub38_Sub39() {
		super(0, true);
	}
	
	final int[] method2775(int i, int i_1_) {
		anInt10497++;
		int[] is = aClass146_7460.method1645(i_1_);
		if (aClass146_7460.aBoolean1819) {
			int i_2_ = 0;
			for (/**/; Class339_Sub7.anInt8728 > i_2_; i_2_++) {
				int i_3_ = CacheNode_Sub3.anIntArray9442[i_2_];
				int i_4_ = Node_Sub25_Sub1.anIntArray9941[i_1_];
				int i_5_ = i_3_ * anInt10498 >> 12;
				int i_6_ = i_4_ * anInt10495 >> 12;
				int i_7_ = i_3_ % (4096 / anInt10498) * anInt10498;
				int i_8_ = anInt10495 * (i_4_ % (4096 / anInt10495));
				if (anInt10496 > i_8_) {
					for (i_5_ -= i_6_; i_5_ < 0; i_5_ += 4) {
						/* empty */
					}
					for (/**/; i_5_ > 3; i_5_ -= 4) {
						/* empty */
					}
					if (i_5_ != 1) {
						is[i_2_] = 0;
						continue;
					}
					if (i_7_ < anInt10496) {
						is[i_2_] = 0;
						continue;
					}
				}
				if (anInt10496 > i_7_) {
					for (i_5_ -= i_6_; i_5_ < 0; i_5_ += 4) {
						/* empty */
					}
					for (/**/; i_5_ > 3; i_5_ -= 4) {
						/* empty */
					}
					if (i_5_ > 0) {
						is[i_2_] = 0;
						continue;
					}
				}
				is[i_2_] = 4096;
			}
		}
		if (i <= 107) {
			method2917();
		}
		return is;
	}
}
