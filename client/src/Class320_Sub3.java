/* Class320_Sub3 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class320_Sub3 extends Class320
{
	static int anInt8223;
	static int anInt8224;
	static int anInt8225;
	static int anInt8226;
	static int anInt8227;
	static int anInt8228;
	static int anInt8229;
	static int anInt8230;
	static int anInt8231;
	static int anInt8232;
	static Class318 aClass318_8233 = new Class318(17, -1);
	static HashTable aHashTable8234 = new HashTable(16);
	static boolean aBoolean8235 = false;
	
	public static void method3687() {
		aHashTable8234 = null;
		aClass318_8233 = null;
	}
	
	final boolean method3688() {
		anInt8229++;
        return true;
	}
	
	final void method3673(byte b) {
		if (b < -35) {
			anInt8224++;
			if (method3689((byte) 124)) {
				if (aNode_Sub27_4063.aClass320_Sub29_7270.method3789() && !Class188_Sub2_Sub2.method1911(aNode_Sub27_4063.aClass320_Sub29_7270.method3791())) {
					anInt4064 = 1;
				}
				if (aNode_Sub27_4063.aClass320_Sub1_7313.method3678() == 1) {
					anInt4064 = 1;
				}
			}
			if (anInt4064 == 3) {
				anInt4064 = 2;
			}
			if (anInt4064 < 0 || anInt4064 > 3) {
				anInt4064 = method3677(0);
			}
		}
	}
	
	final boolean method3689(byte b) {
		anInt8223++;
		if (b <= 118) {
			aBoolean8235 = false;
		}
		return Class187.method1882(-37, anInt4064);
	}
	
	Class320_Sub3(Node_Sub27 node_sub27) {
		super(node_sub27);
	}
	
	static void method3690(int i_0_) {
		Class320_Sub23.aClass123_8432 = Class218.aClass123_2566;
		anInt8227++;
		Class159.anInt1996 = 2;
		Node_Sub38_Sub23.anInt10347 = i_0_;
		Class129.method1556(Class188_Sub1_Sub1.aString9327.equals(""), Class188_Sub1_Sub1.aString9327, true, "");
    }
	
	final int method3675(int i, byte b) {
		anInt8226++;
		if (b != 54) {
			return -54;
		}
		if (Class187.method1882(-84, i)) {
			if (aNode_Sub27_4063.aClass320_Sub29_7270.method3789() && !Class188_Sub2_Sub2.method1911(aNode_Sub27_4063.aClass320_Sub29_7270.method3791())) {
				return 3;
			}
			if (aNode_Sub27_4063.aClass320_Sub1_7313.method3678() == 1) {
				return 3;
			}
		}
		if (i == 3) {
			return 3;
		}
		if (Class187.method1882(-128, i)) {
			return 2;
		}
		return 1;
	}
	
	Class320_Sub3(int i, Node_Sub27 node_sub27) {
		super(i, node_sub27);
	}
	
	final void method3676(int i) {
		anInt4064 = i;
        anInt8228++;
	}
	
	final int method3677(int i) {
		if (i != 0) {
			aHashTable8234 = null;
		}
		anInt8230++;
		return 0;
	}
	
	final int method3691() {
		anInt8232++;
        return anInt4064;
	}
	
	static void method3692(byte[] bs, boolean bool) {
		anInt8231++;
		if (Class117_Sub2.aBuffer5154 == null) {
			Class117_Sub2.aBuffer5154 = new Buffer(20000);
		}
		Class117_Sub2.aBuffer5154.method2223(bs.length, (byte) 4, bs, 0);
		if (bool) {
			Class361.method4045(Class117_Sub2.aBuffer5154.aByteArray7019);
			Class180.aClass377_Sub1Array2143 = new Class377_Sub1[Class254.anInt3212];
			int i = 0;
			for (int i_1_ = Mobile_Sub4.anInt10972; i_1_ <= Node_Sub9_Sub5.anInt9757; i_1_++) {
				Class377_Sub1 class377_sub1 = Class327.method3823(i_1_);
				if (class377_sub1 != null) {
					Class180.aClass377_Sub1Array2143[i++] = class377_sub1;
				}
			}
			Class82.aBoolean1122 = false;
			Class194.aLong2374 = Class313.method3650();
			Class117_Sub2.aBuffer5154 = null;
		}
	}
}
