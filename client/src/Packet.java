/* Packet - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Packet extends Buffer
{
	static int anInt9386 = 0;
	static int anInt9387;
	static int anInt9388;
	static int anInt9389;
	static int anInt9390;
	static int anInt9391;
	static int anInt9392;
	static int anInt9393;
	static Class299 aClass299_9394;
	static int anInt9395;
	static int anInt9396;
	static int anInt9397;
	static int anInt9398;
	private IsaacCipher anIsaacCipher9399;
	private int anInt9400;
	static int anInt9401;
	static Class42 aClass42_9402;
	
	public static void method2254() {
		aClass299_9394 = null;
		aClass42_9402 = null;
	}
	
	final void method2255() {
		anInt7002 = (7 + anInt9400) / 8;
		anInt9398++;
	}
	
	final int method2256(int i) {
		anInt9388++;
		int i_0_ = anInt9400 >> 3;
		int i_1_ = 8 + -(anInt9400 & 0x7);
		int i_2_ = 0;
		anInt9400 += i;
		for (/**/; i_1_ < i; i_1_ = 8) {
			i_2_ += (CacheNode_Sub17.anIntArray8846[i_1_] & aByteArray7019[i_0_++]) << -i_1_ + i;
			i -= i_1_;
		}
		if (i == i_1_) {
			i_2_ += aByteArray7019[i_0_] & CacheNode_Sub17.anIntArray8846[i_1_];
		} else {
			i_2_ += aByteArray7019[i_0_] >> i_1_ + -i & CacheNode_Sub17.anIntArray8846[i];
		}
        return i_2_;
	}
	
	Packet(int i) {
		super(i);
	}
	
	final boolean method2257() {
		anInt9390++;
		int i = 0xff & aByteArray7019[anInt7002] - anIsaacCipher9399.method1670();
        return i >= 128;
    }
	
	static void method2258() {
		anInt9395++;
		synchronized (Node_Sub36_Sub4.aClass61_10065) {
			Node_Sub36_Sub4.aClass61_10065.method598(5);
		}
		synchronized (CacheNode_Sub3.aClass61_9446) {
			CacheNode_Sub3.aClass61_9446.method598(5);
		}
	}
	
	final void method2259(int[] is) {
		anIsaacCipher9399 = new IsaacCipher(is);
		anInt9401++;
	}
	
	final void method2260(byte[] bs, int i) {
		anInt9391++;
		for (int i_5_ = 0; i > i_5_; i_5_++)
			bs[i_5_] = (byte) (aByteArray7019[anInt7002++] + -anIsaacCipher9399.method1667());
	}
	
	final void method2261() {
		anInt9400 = 8 * anInt7002;
		anInt9397++;
	}
	
	final void method2262(int i, int i_6_) {
		anInt9396++;
		if (i == 1) {
			aByteArray7019[anInt7002++] = (byte) (i_6_ + anIsaacCipher9399.method1667());
		}
	}
	
	static Class257[] method2263(int i) {
		if (i != 31303) {
			method2263(-5);
		}
		anInt9387++;
		return new Class257[] { Node_Sub15_Sub9.aClass257_9838, Node_Sub37.aClass257_7443, Node_Sub38_Sub17.aClass257_10270, Class225.aClass257_2674, Node_Sub38_Sub15.aClass257_10262, Class169_Sub2.aClass257_8804, Actor.aClass257_10823, Class66_Sub1.aClass257_8984, Class189_Sub1.aClass257_6882, Class262_Sub13.aClass257_7801, CacheNode_Sub1.aClass257_9421, client.aClass257_5467, Class262_Sub23.aClass257_7883, Class46.aClass257_674, Class12.aClass257_198 };
	}
	
	final int method2264(int i) {
		anInt9392++;
		return 8 * i - anInt9400;
	}
	
	final int method2265() {
		anInt9389++;
		int i_8_ = 0xff & aByteArray7019[anInt7002++] + -anIsaacCipher9399.method1667();
		if (i_8_ < 128) {
			return i_8_;
		}
		return (0xff & aByteArray7019[anInt7002++] - anIsaacCipher9399.method1667()) + (-128 + i_8_ << 8);
	}
	
	final void method2266(IsaacCipher isaaccipher, int i) {
		anIsaacCipher9399 = isaaccipher;
		anInt9393++;
		if (i >= -44) {
			aClass299_9394 = null;
		}
	}
}
