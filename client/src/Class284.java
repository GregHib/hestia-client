/* Class284 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class284
{
	protected short[] aShortArray3592;
	protected short[] aShortArray3593;
	static int anInt3594;
	protected short[] aShortArray3595;
	static EntityNode_Sub3_Sub2_Sub1[] anEntityNode_Sub3_Sub2_Sub1Array3596;
	static float aFloat3597;
	protected byte[] aByteArray3598;
	static int anInt3599;
	
	public static void method3390() {
		anEntityNode_Sub3_Sub2_Sub1Array3596 = null;
	}
	
	static GraphicsBuffer method3391(int i, java.awt.Canvas canvas, int i_0_, int i_1_) {
		anInt3599++;
		try {
			if (i_0_ < 32) {
				anEntityNode_Sub3_Sub2_Sub1Array3596 = null;
			}
			GraphicsBuffer graphicsbuffer = new GraphicsBuffer_Sub1();
			graphicsbuffer.method2595(i_1_, i, canvas);
			return graphicsbuffer;
		} catch (Throwable throwable) {
			ProducingGraphicsBuffer producinggraphicsbuffer = new ProducingGraphicsBuffer();
			producinggraphicsbuffer.method2595(i_1_, i, canvas);
			return producinggraphicsbuffer;
		}
	}
	
	Class284() {
		/* empty */
	}
	
	static void method3392() {
		anInt3594++;
		Packet packet = Class218.aClass123_2566.aPacket1570;
		for (int i = 0; i < Node_Sub38_Sub6.anInt10132; i++) {
			int i_2_ = Class194_Sub1_Sub1.anIntArray9370[i];
			Npc npc = ((Node_Sub41) Class12.aHashTable187.method1518((long) i_2_)).aNpc7518;
			int i_3_ = packet.readUnsignedByte(255);
			if ((0x80 & i_3_) != 0) {
				i_3_ += packet.readUnsignedByte(255) << 8;
			}
			if ((0x8000 & i_3_) != 0) {
				i_3_ += packet.readUnsignedByte(255) << 16;
			}
			if ((i_3_ & 0x100000) != 0) {
				int i_4_ = packet.readShort(-130546744);
				int i_5_ = packet.readInt();
				if (i_4_ == 65535) {
					i_4_ = -1;
				}
				int i_6_ = packet.readUnsignedByteAdd(4255);
				int i_7_ = i_6_ & 0x7;
				int i_8_ = i_6_ >> 3 & 0xf;
				if (i_8_ == 15) {
					i_8_ = -1;
				}
				boolean bool_9_ = (0x1 & i_6_ >> 7) == 1;
				npc.method860(2, i_7_, bool_9_, i_5_, i_8_, i_4_, -86);
			}
			if ((0x1 & i_3_) != 0) {
				npc.anInt10838 = packet.readShortLittle();
				if (npc.anInt10838 == 65535) {
					npc.anInt10838 = -1;
				}
			}
			if ((i_3_ & 0x20000) != 0) {
				int i_10_ = packet.readShort(-130546744);
				int i_11_ = packet.readInt();
				if (i_10_ == 65535) {
					i_10_ = -1;
				}
				int i_12_ = packet.readUnsignedByte(255);
				int i_13_ = 0x7 & i_12_;
				int i_14_ = i_12_ >> 3 & 0xf;
				if (i_14_ == 15) {
					i_14_ = -1;
				}
				boolean bool_15_ = (i_12_ >> 7 & 0x1) == 1;
				npc.method860(3, i_13_, bool_15_, i_11_, i_14_, i_10_, -123);
			}
			if ((i_3_ & 0x40) != 0) {
				int i_16_ = packet.readUnsignedByteInverse();
				if (i_16_ > 0) {
					for (int i_17_ = 0; i_17_ < i_16_; i_17_++) {
						int i_18_ = -1;
						int i_19_ = -1;
						int i_20_ = packet.readSmart();
						int i_21_ = -1;
						if (i_20_ == 32767) {
							i_20_ = packet.readSmart();
							i_19_ = packet.readSmart();
							i_18_ = packet.readSmart();
							i_21_ = packet.readSmart();
						} else if (i_20_ == 32766) {
                            i_20_ = -1;
                        } else {
                            i_19_ = packet.readSmart();
                        }
                        int i_22_ = packet.readSmart();
						int i_23_ = packet.readUnsignedByte(255);
						npc.method852(i_21_, i_22_, i_23_, (byte) -121, i_19_, Class174.anInt2092, i_18_, i_20_);
					}
				}
			}
			if ((i_3_ & 0x100) != 0) {
				int i_24_ = packet.readShortAdd(-602457616);
				npc.anInt10856 = packet.readUnsignedByteInverse();
				npc.anInt10848 = packet.readUnsignedByteInverse();
				npc.anInt10855 = i_24_ & 0x7fff;
				npc.aBoolean10871 = (0x8000 & i_24_) != 0;
				npc.anInt10835 = npc.anInt10855 + Class174.anInt2092 + npc.anInt10856;
			}
			if ((0x40000 & i_3_) != 0) {
				npc.aString11099 = packet.readString(-1);
				if ("".equals(npc.aString11099) || npc.aString11099.equals(npc.aNpcDefinition11122.aString2821)) {
					npc.aString11099 = npc.aNpcDefinition11122.aString2821;
				}
			}
			if ((i_3_ & 0x20) != 0) {
				if (npc.aNpcDefinition11122.method2998()) {
					Node_Sub38_Sub4.method2799(-126, npc);
				}
				npc.method879(Class366.aClass279_4526.method3376(packet.readShortLittle()));
				npc.method861(npc.aNpcDefinition11122.anInt2811);
				npc.anInt10890 = npc.aNpcDefinition11122.anInt2876 << 3;
				if (npc.aNpcDefinition11122.method2998()) {
					Class262_Sub1.method3150(npc.aByte5933, null, npc.anIntArray10910[0], npc.anIntArray10908[0], npc, null, 0);
				}
			}
			if ((0x2 & i_3_) != 0) {
				npc.method878(packet.readString(-1));
			}
			if ((i_3_ & 0x8) != 0) {
				npc.anInt11107 = packet.readShortLittle();
				npc.anInt11100 = packet.readShortLittle();
			}
			if ((i_3_ & 0x80000) != 0) {
				npc.anInt11127 = packet.readShortLittle();
				if (npc.anInt11127 == 65535) {
					npc.anInt11127 = npc.aNpcDefinition11122.anInt2838;
				}
			}
			if ((i_3_ & 0x2000) != 0) {
				int i_25_ = packet.readUnsignedByteAdd(4255);
				int[] is = new int[i_25_];
				int[] is_26_ = new int[i_25_];
				for (int i_27_ = 0; i_27_ < i_25_; i_27_++) {
					int i_28_ = packet.readShort(-130546744);
					if ((i_28_ & 0xc000) == 49152) {
						int i_29_ = packet.readShortAdd(-602457616);
						is[i_27_] = Node_Sub16.method2590(i_29_, i_28_ << 16);
					} else {
						is[i_27_] = i_28_;
					}
					is_26_[i_27_] = packet.readShort(-130546744);
				}
				npc.method866(is_26_, is, (byte) 121);
			}
			if ((i_3_ & 0x10000) != 0) {
				int i_30_ = npc.aNpcDefinition11122.anIntArray2847.length;
				int i_31_ = 0;
				if (npc.aNpcDefinition11122.aShortArray2829 != null) {
					i_31_ = npc.aNpcDefinition11122.aShortArray2829.length;
				}
				if (npc.aNpcDefinition11122.aShortArray2874 != null) {
					i_31_ = npc.aNpcDefinition11122.aShortArray2874.length;
				}
				int i_32_ = 0;
				int i_33_ = packet.readByteSubtract((byte) 125);
				if ((0x1 & i_33_) != 1) {
					int[] is = null;
					if ((i_33_ & 0x2) == 2) {
						is = new int[i_30_];
						for (int i_34_ = 0; i_30_ > i_34_; i_34_++)
							is[i_34_] = packet.readUnsignedShortAddLittle();
					}
					short[] ses = null;
					if ((i_33_ & 0x4) == 4) {
						ses = new short[i_31_];
						for (int i_35_ = 0; i_31_ > i_35_; i_35_++)
							ses[i_35_] = (short) packet.readShortAdd(-602457616);
					}
					short[] ses_36_ = null;
					if ((i_33_ & 0x8) == 8) {
						ses_36_ = new short[i_32_];
					}
					long l = (long) npc.anInt11124++ << 32 | (long) i_2_;
					new Class361(l, is, ses, ses_36_);
				}
			}
			if ((0x400 & i_3_) != 0) {
				npc.anInt10879 = packet.readByteSubtract();
				npc.anInt10883 = packet.readByteSubtract();
				npc.anInt10897 = packet.readByteInverse();
				npc.anInt10891 = packet.readByteInverse();
				npc.anInt10892 = packet.readShort(-130546744) + Class174.anInt2092;
				npc.anInt10887 = packet.readUnsignedShortAddLittle() + Class174.anInt2092;
				npc.anInt10885 = packet.readByteSubtract((byte) 118);
				npc.anInt10891 += npc.anIntArray10908[0];
				npc.anInt10904 = 1;
				npc.anInt10883 += npc.anIntArray10908[0];
				npc.anInt10897 += npc.anIntArray10910[0];
				npc.anInt10900 = 0;
				npc.anInt10879 += npc.anIntArray10910[0];
			}
			if ((0x10 & i_3_) != 0) {
				int[] is = new int[4];
				for (int i_38_ = 0; i_38_ < 4; i_38_++) {
					is[i_38_] = packet.readShort(-130546744);
					if (is[i_38_] == 65535) {
						is[i_38_] = -1;
					}
				}
				int i_39_ = packet.readUnsignedByte(255);
				Class352.method4011(is, i_39_, true, npc, -125);
			}
			if ((0x800 & i_3_) != 0) {
				int i_40_ = npc.aNpcDefinition11122.anIntArray2865.length;
				int i_41_ = 0;
				if (npc.aNpcDefinition11122.aShortArray2829 != null) {
					i_41_ = npc.aNpcDefinition11122.aShortArray2829.length;
				}
				int i_42_ = 0;
				if (npc.aNpcDefinition11122.aShortArray2874 != null) {
					i_42_ = npc.aNpcDefinition11122.aShortArray2874.length;
				}
				int i_43_ = packet.readByteSubtract((byte) 98);
				if ((i_43_ & 0x1) == 1) {
					npc.aClass361_11118 = null;
				} else {
					int[] is = null;
					if ((0x2 & i_43_) == 2) {
						is = new int[i_40_];
						for (int i_44_ = 0; i_44_ < i_40_; i_44_++)
							is[i_44_] = packet.readShort(-130546744);
					}
					short[] ses = null;
					if ((0x4 & i_43_) == 4) {
						ses = new short[i_41_];
						for (int i_45_ = 0; i_45_ < i_41_; i_45_++)
							ses[i_45_] = (short) packet.readShort(-130546744);
					}
					short[] ses_46_ = null;
					if ((i_43_ & 0x8) == 8) {
						ses_46_ = new short[i_42_];
						for (int i_47_ = 0; i_42_ > i_47_; i_47_++)
							ses_46_[i_47_] = (short) packet.readUnsignedShortAddLittle();
					}
					long l = (long) i_2_ | (long) npc.anInt11126++ << 32;
					npc.aClass361_11118 = new Class361(l, is, ses, ses_46_);
				}
			}
			if ((0x4000 & i_3_) != 0) {
				int i_48_ = packet.readUnsignedByte(255);
				int[] is = new int[i_48_];
				int[] is_49_ = new int[i_48_];
				int[] is_50_ = new int[i_48_];
				for (int i_51_ = 0; i_48_ > i_51_; i_51_++) {
					int i_52_ = packet.readShortLittle();
					if (i_52_ == 65535) {
						i_52_ = -1;
					}
					is[i_51_] = i_52_;
					is_49_[i_51_] = packet.readUnsignedByteAdd(4255);
					is_50_[i_51_] = packet.readShort(-130546744);
				}
				Node_Sub38_Sub13.method2831(is_50_, is, is_49_, npc);
			}
			if ((0x1000 & i_3_) != 0) {
				int i_53_ = packet.readShortAdd(-602457616);
				int i_54_ = packet.readInt();
				if (i_53_ == 65535) {
					i_53_ = -1;
				}
				int i_55_ = packet.readUnsignedByte(255);
				int i_56_ = i_55_ & 0x7;
				int i_57_ = (0x7c & i_55_) >> 3;
				if (i_57_ == 15) {
					i_57_ = -1;
				}
				boolean bool_58_ = (i_55_ >> 7 & 0x1) == 1;
				npc.method860(1, i_56_, bool_58_, i_54_, i_57_, i_53_, -107);
			}
			if ((i_3_ & 0x4) != 0) {
				int i_59_ = packet.readShortLittle();
				int i_60_ = packet.readIntInverseMiddle();
				if (i_59_ == 65535) {
					i_59_ = -1;
				}
				int i_61_ = packet.readUnsignedByteInverse();
				int i_62_ = 0x7 & i_61_;
				int i_63_ = (i_61_ & 0x7f) >> 3;
				if (i_63_ == 15) {
					i_63_ = -1;
				}
				boolean bool_64_ = (i_61_ & 0xb7) >> 7 == 1;
				npc.method860(0, i_62_, bool_64_, i_60_, i_63_, i_59_, -98);
			}
			if ((0x200 & i_3_) != 0) {
				npc.aByte10896 = packet.readByteAdd();
				npc.aByte10878 = packet.readByteSubtract();
				npc.aByte10884 = packet.readByte();
				npc.aByte10888 = (byte) packet.readByteSubtract((byte) 99);
				npc.anInt10895 = Class174.anInt2092 + packet.readShortLittle();
				npc.anInt10882 = Class174.anInt2092 + packet.readShort(-130546744);
			}
		}
	}
}
