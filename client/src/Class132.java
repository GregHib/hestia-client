/* Class132 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class132
{
	static Class52 aClass52_1673;
	protected int anInt1674;
	static int anInt1675;
	static int anInt1676;
	static Thread aThread1677;
	static int anInt1678;
	protected int anInt1679;
	static int anInt1680;
	static int anInt1681;
	static int[] anIntArray1682;
	protected int anInt1683;
	static int anInt1684 = 0;
	
	static int method1560(Class139 class139) {
		anInt1680++;
		if (class139 == IsaacCipher.aClass139_1829) {
			return 9216;
		} else {
			if (Class191.aClass139_2349 == class139) {
				return 34065;
			}
			if (Class168.aClass139_2042 == class139) {
				return 34066;
			}
		}
        throw new IllegalArgumentException();
	}
	
	static boolean method1561() {
		anInt1681++;
		return Node_Sub38_Sub8.anInt10163 != 0;
    }
	
	public static void method1562() {
		aClass52_1673 = null;
		aThread1677 = null;
		anIntArray1682 = null;
	}
	
	static void method1563() {
		anInt1675++;
		Class277.aCacheNode_Sub13_3509 = new CacheNode_Sub13(Class22.aClass22_367.method297(-12273, Class35.anInt537), "", Class239.anInt2928, 1012, -1, 0L, 0, 0, true, false, 0L, true);
	}
	
	static int method1564(boolean bool) {
		anInt1678++;
		int i_0_ = Class320_Sub20.anInt8397;
	while_135_:
		do {
			do {
				if (i_0_ == 0) {
					if (!bool) {
						return Node_Sub38_Sub1.anInt10075;
					}
					return 0;
				} else if (i_0_ != 1) {
                    if (i_0_ == 2) {
                        break;
                    }
                    break while_135_;
                }
                return Node_Sub38_Sub1.anInt10075;
			} while (false);
			return 0;
		} while (false);
		return 0;
	}
	
	static void method1565(String string, int i) {
		anInt1676++;
		int i_2_ = Class178.anInt2120;
		int[] is = Class66_Sub1.anIntArray8987;
		boolean bool = false;
		for (int i_3_ = 0; i_3_ < i_2_; i_3_++) {
			Player player = Class270_Sub2.aPlayerArray8038[is[i_3_]];
			if (player != null && Class295.aPlayer3692 != player && player.aString11142 != null && player.aString11142.equalsIgnoreCase(string)) {
				Class318 class318 = null;
				if (i == 1) {
					class318 = Class228.aClass318_2720;
				} else if (i == 4) {
					class318 = Class312.aClass318_3973;
				} else if (i == 5) {
                    class318 = Class359.aClass318_4463;
                } else if (i == 6) {
                    class318 = Class290_Sub4.aClass318_8089;
                } else if (i == 7) {
                    class318 = Node_Sub25_Sub1.aClass318_9953;
                } else if (i == 9) {
                    class318 = CacheNode_Sub15.aClass318_9590;
                }
                if (class318 != null) {
					Class188_Sub1.anInt6852++;
					Node_Sub13 node_sub13 = FloatBuffer.method2250(-386, class318, Class218.aClass123_2566.anIsaacCipher1571);
					node_sub13.aPacket7113.writeByte(0);
					node_sub13.aPacket7113.writeShort(is[i_3_], -105);
					Class218.aClass123_2566.method1514(127, node_sub13);
				}
				bool = true;
				break;
			}
		}
		if (!bool) {
			Class41.method436(Class22.aClass22_384.method297(-12273, Class35.anInt537) + string);
		}
	}
}
