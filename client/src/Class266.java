/* Class266 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;

public class Class266
{
	static int anInt3368;
	static int anInt3369;
	static int anInt3370;
	static int anInt3371;
	static int anInt3372;
	static int anInt3373;
	static int anInt3374;
	static int anInt3375;
	static int anInt3376;
	static int anInt3377;
	static int anInt3378;
	static int anInt3379;
	static HashTable aHashTable3380 = new HashTable(64);
	static int anInt3381;
	static int anInt3382;
	static int anInt3383;
	private Class158 aClass158_3384 = new Class158();
	static boolean aBoolean3385;
	private Class158 aClass158_3386 = new Class158();
	private Class158 aClass158_3387 = new Class158();
	private Class158 aClass158_3388 = new Class158();
	private BufferedConnection aBufferedConnection3389;
	private int anInt3390;
	private long aLong3391;
	private Buffer aBuffer3392 = new Buffer(4);
	volatile int anInt3393;
	volatile int anInt3394 = 0;
	private byte aByte3395;
	private Buffer aBuffer3396;
	private CacheNode_Sub16_Sub1 aCacheNode_Sub16_Sub1_3397;
	
	final void method3233() {
		try {
			aBufferedConnection3389.method430();
		} catch (Exception exception) {
			/* empty */
		}
		anInt3373++;
		aBufferedConnection3389 = null;
		anInt3393 = -1;
		aByte3395 = (byte) (int) (Math.random() * 255.0 + 1.0);
		anInt3394++;
	}
	
	private void method3234() {
		anInt3371++;
		if (aBufferedConnection3389 != null) {
			try {
				aBuffer3392.anInt7002 = 0;
				aBuffer3392.writeByte(6);
				aBuffer3392.writeMedium((byte) 109, 3);
				aBufferedConnection3389.method429(4, aBuffer3392.aByteArray7019);
			} catch (IOException ioexception) {
				try {
					aBufferedConnection3389.method430();
				} catch (Exception exception) {
					/* empty */
				}
				anInt3394++;
				anInt3393 = -2;
				aBufferedConnection3389 = null;
			}
		}
	}
	
	final void method3235() {
		if (aBufferedConnection3389 != null) {
			aBufferedConnection3389.method430();
		}
		anInt3377++;
	}
	
	final int method3236() {
		anInt3375++;
		return aClass158_3384.method1720() + aClass158_3386.method1720();
	}
	
	static void method3237(GraphicsToolkit graphicstoolkit) {
		for (EntityNode_Sub4 entitynode_sub4 = (EntityNode_Sub4) Animable_Sub2.aClass103_9116.method1113(); entitynode_sub4 != null; entitynode_sub4 = (EntityNode_Sub4) Animable_Sub2.aClass103_9116.method1108(65)) {
			if (entitynode_sub4.aBoolean5978) {
				entitynode_sub4.method955(graphicstoolkit);
			}
		}
		anInt3374++;
	}
	
	final boolean method3238() {
		if (aBufferedConnection3389 != null) {
			long l = Class313.method3650();
			int i = (int) (-aLong3391 + l);
			aLong3391 = l;
			if (i > 200) {
				i = 200;
			}
			anInt3390 += i;
			if (anInt3390 > 30000) {
				try {
					aBufferedConnection3389.method430();
				} catch (Exception exception) {
					/* empty */
				}
				aBufferedConnection3389 = null;
			}
		}
		anInt3379++;
		if (aBufferedConnection3389 == null) {
            return method3236() == 0 && method3246((byte) 111) == 0;
        }
		try {
			aBufferedConnection3389.method423();
			CacheNode_Sub16_Sub1 cachenode_sub16_sub1 = (CacheNode_Sub16_Sub1) aClass158_3384.method1723();
			for (/**/; cachenode_sub16_sub1 != null; cachenode_sub16_sub1 = (CacheNode_Sub16_Sub1) aClass158_3384.method1721()) {
				aBuffer3392.anInt7002 = 0;
				aBuffer3392.writeByte(1);
				aBuffer3392.writeMedium((byte) 66, (int) cachenode_sub16_sub1.aLong7037);
				aBufferedConnection3389.method429(4, aBuffer3392.aByteArray7019);
				aClass158_3386.method1719(cachenode_sub16_sub1);
			}
			for (CacheNode_Sub16_Sub1 cachenode_sub16_sub1_0_ = (CacheNode_Sub16_Sub1) aClass158_3387.method1723(); cachenode_sub16_sub1_0_ != null; cachenode_sub16_sub1_0_ = (CacheNode_Sub16_Sub1) aClass158_3387.method1721()) {
				aBuffer3392.anInt7002 = 0;
				aBuffer3392.writeByte(0);
				aBuffer3392.writeMedium((byte) 99, (int) cachenode_sub16_sub1_0_.aLong7037);
				aBufferedConnection3389.method429(4, aBuffer3392.aByteArray7019);
				aClass158_3388.method1719(cachenode_sub16_sub1_0_);
			}
			for (int i = 0; i < 100; i++) {
				int i_1_ = aBufferedConnection3389.method428();
				if (i_1_ < 0) {
					throw new IOException();
				}
				if (i_1_ == 0) {
					break;
				}
				anInt3390 = 0;
				int i_2_ = 0;
				if (aCacheNode_Sub16_Sub1_3397 == null) {
					i_2_ = 8;
				} else if (aCacheNode_Sub16_Sub1_3397.anInt11070 == 0) {
					i_2_ = 1;
				}
				if (i_2_ > 0) {
					int i_3_ = -aBuffer3396.anInt7002 + i_2_;
					if (i_1_ < i_3_) {
						i_3_ = i_1_;
					}
					aBufferedConnection3389.method425(i_3_, aBuffer3396.anInt7002, aBuffer3396.aByteArray7019);
					if (aByte3395 != 0) {
						for (int i_4_ = 0; i_3_ > i_4_; i_4_++)
							aBuffer3396.aByteArray7019[aBuffer3396.anInt7002 + i_4_] = (byte) Node_Sub19.method2612(aBuffer3396.aByteArray7019[aBuffer3396.anInt7002 + i_4_], aByte3395);
					}
					aBuffer3396.anInt7002 += i_3_;
					if (i_2_ <= aBuffer3396.anInt7002) {
						if (aCacheNode_Sub16_Sub1_3397 == null) {
							aBuffer3396.anInt7002 = 0;
							int i_5_ = aBuffer3396.readUnsignedByte(255);
							int i_6_ = aBuffer3396.readShort(-130546744);
							int i_7_ = aBuffer3396.readUnsignedByte(255);
							int i_8_ = aBuffer3396.readInt();
							int i_9_ = 0x7f & i_7_;
							boolean bool = (i_7_ & 0x80) != 0;
							long l = (long) (i_6_ + (i_5_ << 16));
							CacheNode_Sub16_Sub1 cachenode_sub16_sub1_10_;
							if (bool) {
								for (cachenode_sub16_sub1_10_ = (CacheNode_Sub16_Sub1) aClass158_3388.method1723(); cachenode_sub16_sub1_10_ != null; cachenode_sub16_sub1_10_ = (CacheNode_Sub16_Sub1) aClass158_3388.method1721()) {
									if (l == cachenode_sub16_sub1_10_.aLong7037) {
										break;
									}
								}
							} else {
								for (cachenode_sub16_sub1_10_ = (CacheNode_Sub16_Sub1) aClass158_3386.method1723(); cachenode_sub16_sub1_10_ != null; cachenode_sub16_sub1_10_ = (CacheNode_Sub16_Sub1) aClass158_3386.method1721()) {
									if (l == cachenode_sub16_sub1_10_.aLong7037) {
										break;
									}
								}
							}
                            if (cachenode_sub16_sub1_10_ == null) {
								throw new IOException();
							}
							aCacheNode_Sub16_Sub1_3397 = cachenode_sub16_sub1_10_;
							int i_11_ = i_9_ != 0 ? 9 : 5;
							aCacheNode_Sub16_Sub1_3397.aBuffer11072 = new Buffer(i_11_ + i_8_ + aCacheNode_Sub16_Sub1_3397.aByte11069);
							aCacheNode_Sub16_Sub1_3397.aBuffer11072.writeByte(i_9_);
							aCacheNode_Sub16_Sub1_3397.aBuffer11072.writeInt(i_8_);
							aCacheNode_Sub16_Sub1_3397.anInt11070 = 8;
							aBuffer3396.anInt7002 = 0;
						} else if (aCacheNode_Sub16_Sub1_3397.anInt11070 == 0) {
							if (aBuffer3396.aByteArray7019[0] == -1) {
								aBuffer3396.anInt7002 = 0;
								aCacheNode_Sub16_Sub1_3397.anInt11070 = 1;
							} else {
								aCacheNode_Sub16_Sub1_3397 = null;
							}
                        } else {
							throw new IOException();
						}
					}
				} else {
					int i_12_ = -aCacheNode_Sub16_Sub1_3397.aByte11069 + aCacheNode_Sub16_Sub1_3397.aBuffer11072.aByteArray7019.length;
					int i_13_ = 512 - aCacheNode_Sub16_Sub1_3397.anInt11070;
					if (-aCacheNode_Sub16_Sub1_3397.aBuffer11072.anInt7002 + i_12_ < i_13_) {
						i_13_ = -aCacheNode_Sub16_Sub1_3397.aBuffer11072.anInt7002 + i_12_;
					}
					if (i_13_ > i_1_) {
						i_13_ = i_1_;
					}
					aBufferedConnection3389.method425(i_13_, aCacheNode_Sub16_Sub1_3397.aBuffer11072.anInt7002, aCacheNode_Sub16_Sub1_3397.aBuffer11072.aByteArray7019);
					if (aByte3395 != 0) {
						for (int i_14_ = 0; i_14_ < i_13_; i_14_++)
							aCacheNode_Sub16_Sub1_3397.aBuffer11072.aByteArray7019[aCacheNode_Sub16_Sub1_3397.aBuffer11072.anInt7002 + i_14_] = (byte) Node_Sub19.method2612(aCacheNode_Sub16_Sub1_3397.aBuffer11072.aByteArray7019[aCacheNode_Sub16_Sub1_3397.aBuffer11072.anInt7002 + i_14_], aByte3395);
					}
					aCacheNode_Sub16_Sub1_3397.aBuffer11072.anInt7002 += i_13_;
					aCacheNode_Sub16_Sub1_3397.anInt11070 += i_13_;
					if (aCacheNode_Sub16_Sub1_3397.aBuffer11072.anInt7002 == i_12_) {
						aCacheNode_Sub16_Sub1_3397.method2275(-65);
						aCacheNode_Sub16_Sub1_3397.aBoolean9602 = false;
						aCacheNode_Sub16_Sub1_3397 = null;
					} else if (aCacheNode_Sub16_Sub1_3397.anInt11070 == 512) {
						aCacheNode_Sub16_Sub1_3397.anInt11070 = 0;
					}
				}
			}
			return true;
		} catch (IOException ioexception) {
			try {
				aBufferedConnection3389.method430();
			} catch (Exception exception) {
				/* empty */
			}
			aBufferedConnection3389 = null;
			anInt3394++;
			anInt3393 = -2;
            return method3236() == 0 && method3246((byte) 71) == 0;
        }
	}
	
	final boolean method3239(int i) {
		if (i != 255) {
			return false;
		}
		anInt3376++;
        return method3246((byte) 121) >= 20;
    }
	
	final void method3240(boolean bool) {
		anInt3378++;
		if (aBufferedConnection3389 != null) {
			try {
				aBuffer3392.anInt7002 = 0;
				aBuffer3392.writeByte(bool ? 2 : 3);
				aBuffer3392.writeMedium((byte) 85, 0);
				aBufferedConnection3389.method429(4, aBuffer3392.aByteArray7019);
			} catch (IOException ioexception) {
				try {
					aBufferedConnection3389.method430();
				} catch (Exception exception) {
					/* empty */
				}
				aBufferedConnection3389 = null;
				anInt3393 = -2;
				anInt3394++;
			}
		}
	}
	
	final void method3241(boolean bool, BufferedConnection bufferedconnection) {
		anInt3370++;
		if (aBufferedConnection3389 != null) {
			try {
				aBufferedConnection3389.method430();
			} catch (Exception exception) {
				/* empty */
			}
			aBufferedConnection3389 = null;
		}
		aBufferedConnection3389 = bufferedconnection;
		method3234();
		method3240(bool);
		aCacheNode_Sub16_Sub1_3397 = null;
		aBuffer3396.anInt7002 = 0;
		for (;;) {
			CacheNode_Sub16_Sub1 cachenode_sub16_sub1 = (CacheNode_Sub16_Sub1) aClass158_3386.method1717(-119);
			if (cachenode_sub16_sub1 == null) {
				break;
			}
			aClass158_3384.method1719(cachenode_sub16_sub1);
		}
		for (;;) {
			CacheNode_Sub16_Sub1 cachenode_sub16_sub1 = (CacheNode_Sub16_Sub1) aClass158_3388.method1717(-128);
			if (cachenode_sub16_sub1 == null) {
				break;
			}
			aClass158_3387.method1719(cachenode_sub16_sub1);
		}
		if (aByte3395 != 0) {
			try {
				aBuffer3392.anInt7002 = 0;
				aBuffer3392.writeByte(4);
				aBuffer3392.writeByte(aByte3395);
				aBuffer3392.writeShort(0, -85);
				aBufferedConnection3389.method429(4, aBuffer3392.aByteArray7019);
			} catch (IOException ioexception) {
				try {
					aBufferedConnection3389.method430();
				} catch (Exception exception) {
					/* empty */
				}
				anInt3394++;
				aBufferedConnection3389 = null;
				anInt3393 = -2;
			}
		}
		anInt3390 = 0;
		aLong3391 = Class313.method3650();
	}
	
	final void method3242() {
		anInt3369++;
		if (aBufferedConnection3389 != null) {
			try {
				aBuffer3392.anInt7002 = 0;
				aBuffer3392.writeByte(7);
				aBuffer3392.writeMedium((byte) 65, 0);
				aBufferedConnection3389.method429(4, aBuffer3392.aByteArray7019);
			} catch (IOException ioexception) {
				try {
					aBufferedConnection3389.method430();
				} catch (Exception exception) {
					/* empty */
				}
				aBufferedConnection3389 = null;
				anInt3393 = -2;
				anInt3394++;
			}
		}
	}
	
	static int method3243(int i, Npc npc) {
		anInt3383++;
		NpcDefinition npcdefinition = npc.aNpcDefinition11122;
		if (npcdefinition.anIntArray2827 != null) {
			npcdefinition = npcdefinition.method2999(65535, Class24.aClass275_442);
			if (npcdefinition == null) {
				return -1;
			}
		}
		int i_15_ = npcdefinition.anInt2809;
		if (i > -25) {
			return -110;
		}
		Class259 class259 = npc.method868((byte) -124);
		int i_16_ = npc.anAnimator10860.method250();
		if (i_16_ == -1 || npc.aBoolean10867) {
			i_15_ = npcdefinition.anInt2812;
		} else if (class259.anInt3252 != i_16_ && class259.anInt3271 != i_16_ && i_16_ != class259.anInt3293 && class259.anInt3270 != i_16_) {
            if (class259.anInt3262 == i_16_ || class259.anInt3297 == i_16_ || i_16_ == class259.anInt3304 || i_16_ == class259.anInt3269) {
                i_15_ = npcdefinition.anInt2833;
            }
        } else {
            i_15_ = npcdefinition.anInt2810;
        }
        return i_15_;
	}
	
	final boolean method3244() {
		anInt3368++;
        return method3236() >= 20;
    }
	
	final void method3245() {
		if (aBufferedConnection3389 != null) {
			aBufferedConnection3389.method426();
		}
		anInt3382++;
	}
	
	private int method3246(byte b) {
		anInt3381++;
		if (b < 31) {
			return -79;
		}
		return aClass158_3387.method1720() + aClass158_3388.method1720();
	}
	
	final CacheNode_Sub16_Sub1 method3247(int i, int i_17_, boolean bool, byte b_18_) {
		anInt3372++;
		long l = (long) ((i << 16) + i_17_);
		CacheNode_Sub16_Sub1 cachenode_sub16_sub1 = new CacheNode_Sub16_Sub1();
		cachenode_sub16_sub1.aBoolean9598 = bool;
		cachenode_sub16_sub1.aByte11069 = b_18_;
		cachenode_sub16_sub1.aLong7037 = l;
		if (bool) {
			if (method3236() >= 20) {
				throw new RuntimeException();
			}
			aClass158_3384.method1719(cachenode_sub16_sub1);
		} else {
			if (method3246((byte) 85) >= 20) {
				throw new RuntimeException();
			}
			aClass158_3387.method1719(cachenode_sub16_sub1);
		}
		return cachenode_sub16_sub1;
	}
	
	public static void method3248() {
		aHashTable3380 = null;
	}
	
	public Class266() {
		anInt3393 = 0;
		aByte3395 = (byte) 0;
		aBuffer3396 = new Buffer(8);
	}
}
