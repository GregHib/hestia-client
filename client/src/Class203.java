/* Class203 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.math.BigInteger;

public class Class203
{
	static int anInt2449 = 2;
	static int[] anIntArray2450 = new int[1];
	static BigInteger aBigInteger2451 = new BigInteger("8dfc909de44fd5137b68a6e16317de0698aa21cb0b6d8dcfb05855d99f30485e62547932500fba195f1152437551ce45c628797dc9edd7dd79c028e178d55a02bc8734f72b3796271164cbf5486c44f11c14cc26c231ef883bc99f812d15a1e3457e239d17259875fa9ca5ce049a838fe5c61bf645f364b08c2473c923540e55", 16);
	static int anInt2452 = -1;
	
	static void method2028(int i_0_) {
		r var_r = null;
		for (int i_1_ = 1; i_1_ < i_0_; i_1_++) {
			Plane plane = Node_Sub38_Sub37.aPlaneArray10466[i_1_];
			if (plane != null) {
				for (int i_2_ = 0; i_2_ < Node_Sub50.anInt7623; i_2_++) {
					for (int i_3_ = 0; i_3_ < Class328.anInt4115; i_3_++) {
						var_r = plane.fa(i_3_, i_2_, var_r);
						if (var_r != null) {
							int i_4_ = i_3_ << Class36.anInt549;
							int i_5_ = i_2_ << Class36.anInt549;
							for (int i_6_ = i_1_ - 1; i_6_ >= 0; i_6_--) {
								Plane plane_7_ = Node_Sub38_Sub37.aPlaneArray10466[i_6_];
								if (plane_7_ != null) {
									int i_8_ = plane.method3251(i_2_, i_3_) - plane_7_.method3251(i_2_, i_3_);
									int i_9_ = plane.method3251(i_2_, i_3_ + 1) - plane_7_.method3251(i_2_, i_3_ + 1);
									int i_10_ = plane.method3251(i_2_ + 1, i_3_ + 1) - plane_7_.method3251(i_2_ + 1, i_3_ + 1);
									int i_11_ = plane.method3251(i_2_ + 1, i_3_) - plane_7_.method3251(i_2_ + 1, i_3_);
									plane_7_.CA(var_r, i_4_, (i_8_ + i_9_ + i_10_ + i_11_) / 4, i_5_);
								}
							}
						}
					}
				}
			}
		}
	}
	
	public static void method2029() {
		aBigInteger2451 = null;
		anIntArray2450 = null;
	}
}
