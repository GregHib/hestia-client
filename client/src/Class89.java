/* Class89 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class89
{
	static int anInt1194;
	static int[] anIntArray1195 = new int[5];
	static int anInt1196;
	static int anInt1197;
	static int anInt1198 = 0;
	static Class312 aClass312_1199 = new Class312();
	static int anInt1200;
	
	static int method1019(int i, int i_1_) {
		anInt1196++;
		double d = Math.log((double) i_1_) / Math.log(2.0);
		double d_2_ = Math.log((double) i) / Math.log(2.0);
		double d_3_ = d_2_ + Math.random() * (-d_2_ + d);
		return (int) (0.5 + Math.pow(2.0, d_3_));
	}
	
	public static void method1020() {
		aClass312_1199 = null;
		anIntArray1195 = null;
	}
	
	static void method1021(GraphicsToolkit graphicstoolkit) {
		anInt1200++;
		int i = 0;
		int i_4_ = 0;
		if (Class71.aBoolean967) {
			i = Class67.method733(-89);
			i_4_ = Class226.method2112();
		}
		int i_5_ = -10660793;
		Class53.method556(client.anInt5481 + i_4_, Node_Sub6.anInt7043 + i, Class219.anInt2627, i_5_, graphicstoolkit, Class362.anInt4492);
		Class262_Sub4.aClass52_7721.method538(Node_Sub6.anInt7043 + i + 3, client.anInt5481 - (-i_4_ + -14), Class22.aClass22_387.method297(-12273, Class35.anInt537), -1, i_5_);
		int i_7_ = Class106.aClass93_1356.method1050((byte) -17) + i;
		int i_8_ = Class106.aClass93_1356.method1051(true) + i_4_;
		if (Class194_Sub3_Sub1.aBoolean9375) {
			int i_11_ = 0;
			for (CacheNode_Sub4 cachenode_sub4 = (CacheNode_Sub4) Class184.aClass158_2189.method1723(); cachenode_sub4 != null; cachenode_sub4 = (CacheNode_Sub4) Class184.aClass158_2189.method1721()) {
				int i_12_ = i_4_ + client.anInt5481 + (31 + i_11_ * 16);
				if (cachenode_sub4.anInt9462 == 1) {
					Widget.method4139(Class362.anInt4492, i_12_, -256, (CacheNode_Sub13) cachenode_sub4.aClass158_9457.aCacheNode1984.aCacheNode7035, -1, i_8_, i + Node_Sub6.anInt7043, i_7_);
				} else {
					Node_Sub40.method2924(i_8_, Class362.anInt4492, i_12_, -256, i_7_, -1, i + Node_Sub6.anInt7043, cachenode_sub4);
				}
                i_11_++;
			}
			if (Class73.aCacheNode_Sub4_988 != null) {
				Class53.method556(Archive.anInt286, Class367.anInt4539, Class18.anInt309, i_5_, graphicstoolkit, CacheNode.anInt7033);
				i_11_ = 0;
				Class262_Sub4.aClass52_7721.method538(Class367.anInt4539 + 3, Archive.anInt286 + 14, Class73.aCacheNode_Sub4_988.aString9458, -1, i_5_);
				for (CacheNode_Sub13 cachenode_sub13 = (CacheNode_Sub13) Class73.aCacheNode_Sub4_988.aClass158_9457.method1723(); cachenode_sub13 != null; cachenode_sub13 = (CacheNode_Sub13) Class73.aCacheNode_Sub4_988.aClass158_9457.method1721()) {
					int i_13_ = i_11_ * 16 + Archive.anInt286 + 31;
					i_11_++;
					Widget.method4139(CacheNode.anInt7033, i_13_, -256, cachenode_sub13, -1, i_8_, Class367.anInt4539, i_7_);
				}
				Class226.method2114(Class367.anInt4539, Archive.anInt286, CacheNode.anInt7033, Class18.anInt309, -107);
			}
		} else {
			int i_9_ = 0;
			for (CacheNode_Sub13 cachenode_sub13 = (CacheNode_Sub13) Class368.aClass312_4549.method3613(65280); cachenode_sub13 != null; cachenode_sub13 = (CacheNode_Sub13) Class368.aClass312_4549.method3620(16776960)) {
				int i_10_ = (Class315.anInt4035 + (-1 - i_9_)) * 16 + (i_4_ + (client.anInt5481 + 31));
				i_9_++;
				Widget.method4139(Class362.anInt4492, i_10_, -256, cachenode_sub13, -1, i_8_, i + Node_Sub6.anInt7043, i_7_);
			}
		}
        Class226.method2114(i + Node_Sub6.anInt7043, i_4_ + client.anInt5481, Class362.anInt4492, Class219.anInt2627, -123);
	}
	
	static void method1022() {
		anInt1194++;
		if (Class26.method311(Class151.anInt1843) || Class329.method3833(Class151.anInt1843)) {
			Model.method2092(OutputStream_Sub2.anInt97, Class98.anInt5061 >> 12, Node_Sub10.anInt7079 >> 12);
		} else {
			int i = Class295.aPlayer3692.anIntArray10910[0] >> 3;
			int i_14_ = Class295.aPlayer3692.anIntArray10908[0] >> 3;
			if (i < 0 || Node_Sub54.anInt7675 >> 3 <= i || i_14_ < 0 || Class377_Sub1.anInt8774 >> 3 <= i_14_) {
				Model.method2092(0, Node_Sub54.anInt7675 >> 4, Class377_Sub1.anInt8774 >> 4);
			} else {
				Model.method2092(OutputStream_Sub2.anInt97, i, i_14_);
			}
		}
        CacheNode_Sub14.method2349();
		Class313.method3648();
		Class363.method4054();
		Class239.method3022();
	}
}
