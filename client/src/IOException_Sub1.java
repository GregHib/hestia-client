/* IOException_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.io.IOException;

public class IOException_Sub1 extends IOException
{
	static int anInt84 = -1;
	static Class128 aClass128_85;
	static int anInt86;
	static int anInt87;
	
	static boolean method131(byte b) {
		anInt86++;
		int i = b & 0xff;
		if (i == 0) {
			return false;
		}
        return i < 128 || i >= 160 || Class204.aCharArray2455[-128 + i] != 0;
    }
	
	IOException_Sub1(String string) {
		super(string);
	}
	
	public static void method132() {
		aClass128_85 = null;
	}
	
	static void method133(Class302 class302, SignLink signlink) {
		Class239.aClass302_2931 = class302;
		anInt87++;
		Class117_Sub1.aString4931 = "";
		Animable_Sub1_Sub1.aSignLink10616 = signlink;
		if (Class223.aString2660.startsWith("win")) {
			Class117_Sub1.aString4931 += "windows/";
		} else if (Class223.aString2660.startsWith("linux")) {
            Class117_Sub1.aString4931 += "linux/";
        } else if (Class223.aString2660.startsWith("mac")) {
            Class117_Sub1.aString4931 += "macos/";
        }
        if (Animable_Sub1_Sub1.aSignLink10616.aBoolean3985) {
            Class117_Sub1.aString4931 += "msjava/";
        } else if (Class223.aString2656.startsWith("amd64") || Class223.aString2656.startsWith("x86_64")) {
            Class117_Sub1.aString4931 += "x86_64/";
        } else if (Class223.aString2656.startsWith("i386") || Class223.aString2656.startsWith("i486") || Class223.aString2656.startsWith("i586") || Class223.aString2656.startsWith("x86")) {
            Class117_Sub1.aString4931 += "x86/";
        } else if (Class223.aString2656.startsWith("ppc")) {
            Class117_Sub1.aString4931 += "ppc/";
        } else {
            Class117_Sub1.aString4931 += "universal/";
        }
    }
}
