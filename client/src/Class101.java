/* Class101 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class101
{
	static GLSprite[] aGLSpriteArray1301;
	private Class61 aClass61_1302 = new Class61(64);
	static int anInt1303;
	static int anInt1304;
	static int anInt1305;
	static int anInt1306;
	static int anInt1307;
	private Class302 aClass302_1308;
	static int anInt1309;
	protected Class302 aClass302_1310;
	protected Class61 aClass61_1311 = new Class61(60);
	protected int anInt1312;
	static boolean aBoolean1313 = false;
	
	final Class195 method1090(int i_0_) {
		anInt1307++;
		Class195 class195;
		synchronized (aClass61_1302) {
			class195 = (Class195) aClass61_1302.method607((long) i_0_);
		}
		if (class195 != null) {
			return class195;
		}
		byte[] bs;
		synchronized (aClass302_1308) {
			bs = aClass302_1308.method3524(Node_Sub44.method2946(i_0_), Class190.method1938(i_0_));
		}
		class195 = new Class195();
		class195.anInt2378 = i_0_;
		class195.aClass101_2392 = this;
		if (bs != null) {
			class195.method1983(new Buffer(bs));
		}
		synchronized (aClass61_1302) {
			aClass61_1302.method601(class195, 25566, (long) i_0_);
		}
		return class195;
	}
	
	final void method1091(byte b, int i) {
		anInt1304++;
		anInt1312 = i;
		synchronized (aClass61_1311) {
			aClass61_1311.method608(false);
		}
		if (b > -35) {
			method1091((byte) -59, -31);
		}
	}
	
	public static void method1092() {
		aGLSpriteArray1301 = null;
	}
	
	final void method1093() {
		anInt1309++;
		synchronized (aClass61_1302) {
			aClass61_1302.method602((byte) -122);
		}
		synchronized (aClass61_1311) {
			aClass61_1311.method602((byte) -126);
		}
	}
	
	final void method1094() {
		synchronized (aClass61_1302) {
			aClass61_1302.method598(5);
		}
		anInt1303++;
		synchronized (aClass61_1311) {
			aClass61_1311.method598(5);
		}
	}
	
	final void method1095() {
		anInt1305++;
		synchronized (aClass61_1302) {
			aClass61_1302.method608(false);
		}
		synchronized (aClass61_1311) {
			aClass61_1311.method608(false);
		}
	}
	
	Class101(Class302 class302, Class302 class302_4_) {
		aClass302_1310 = class302_4_;
		aClass302_1308 = class302;
		int i_5_ = aClass302_1308.method3526() + -1;
		aClass302_1308.method3537(-2, i_5_);
	}
}
