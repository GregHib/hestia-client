/* Class306 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.lang.reflect.Method;

public class Class306
{
	static GraphicsToolkit method3566(Class302 class302, int i, d var_d, Canvas canvas) {
		GraphicsToolkit graphicstoolkit;
		try {
			if (!Class352.method4012()) {
				throw new RuntimeException("");
			}
			if (!Node_Sub38_Sub2.method2793("jagdx")) {
				throw new RuntimeException("");
			}
			graphicstoolkit = D3DToolkit.createToolkit(canvas, var_d, class302, i);
		} catch (Throwable throwable) {
			throw new RuntimeException("");
		}
		return graphicstoolkit;
	}
}
