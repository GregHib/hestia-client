/* Node_Sub25_Sub4 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import jagtheora.ogg.OggPacket;
import jagtheora.ogg.OggStreamState;

import java.awt.*;

public class Node_Sub25_Sub4 extends Node_Sub25
{
	private String aString9992;
	private String aString9993;
	static int anInt9994;
	private int anInt9995;
	private float aFloat9996;
	private String aString9997;
	static Class192 aClass192_9998 = new Class192(18, 4);
	static int anInt9999;
	static int anInt10000;
	static int anInt10001;
	static int anInt10002;
	private float aFloat10003;
	static int anInt10004;
	static int anInt10005 = -1;
	static Class110[] aClass110Array10006;
	private int anInt10007;
	static int anInt10008;
	static int anInt10009 = 0;
	static Class353 aClass353_10010 = new Class353("game4", "Game 4", 3);
	static int anInt10011 = 0;
	
	final float method2678() {
		anInt10000++;
		return aFloat10003;
	}
	
	static void method2679(SignLink signlink, Frame frame) {
		anInt10001++;
		for (;;) {
			Class241 class241 = signlink.method3639(frame);
			while (class241.anInt2953 == 0)
				Class262_Sub22.method3208(10L);
			if (class241.anInt2953 == 1) {
				break;
			}
			Class262_Sub22.method3208(100L);
		}
		frame.setVisible(false);
		frame.dispose();
	}
	
	final String method2680() {
		anInt10002++;
		return aString9993;
	}
	
	Node_Sub25_Sub4(OggStreamState oggstreamstate) {
		super(oggstreamstate);
	}
	
	final void method2654(OggPacket oggpacket) {
		anInt9999++;
		if (anInt7234 <= 0 || "SUB".equals(aString9997)) {
			Buffer buffer = new Buffer(oggpacket.getData());
			int i_0_ = buffer.readUnsignedByte(255);
			if (anInt7234 > 8) {
				if (i_0_ == 0) {
					long l = buffer.readLong();
					long l_1_ = buffer.readLong();
					long l_2_ = buffer.readLong();
					if (l < 0 || l_1_ < 0L || l_2_ < 0 || l_2_ > l) {
						throw new IllegalStateException();
					}
					aFloat9996 = (float) ((long) anInt9995 * (l + l_1_)) / (float) anInt10007;
					aFloat10003 = (float) ((long) anInt9995 * l) / (float) anInt10007;
					int i_3_ = buffer.method2188(119);
					if (i_3_ < 0 || i_3_ > buffer.aByteArray7019.length - buffer.anInt7002) {
						throw new IllegalStateException();
					}
					aString9992 = Class197.method2001(i_3_, buffer.aByteArray7019, buffer.anInt7002);
				}
			} else if (anInt7234 == 0) {
                buffer.anInt7002 += 23;
                anInt10007 = buffer.method2188(125);
                anInt9995 = buffer.method2188(112);
                if (anInt10007 == 0 || anInt9995 == 0) {
                    throw new IllegalStateException();
                }
                Buffer buffer_4_ = new Buffer(16);
                buffer.method2181(0, 16, buffer_4_.aByteArray7019, -19417);
                aString9993 = buffer_4_.readString(-1);
                buffer_4_.anInt7002 = 0;
                buffer.method2181(0, 16, buffer_4_.aByteArray7019, -19417);
                aString9997 = buffer_4_.readString(-1);
            }
		}
	}
	
	public static void method2681() {
		aClass110Array10006 = null;
		aClass192_9998 = null;
		aClass353_10010 = null;
	}
	
	final String method2682() {
		anInt10008++;
		return aString9992;
	}
	
	final float method2683(int i) {
		anInt10004++;
		if (i > -47) {
			method2656(-1);
		}
		return aFloat9996;
	}
	
	final void method2656(int i) {
		anInt9994++;
		if (i != 100) {
			method2683(121);
		}
	}
	
	static {
		new Class353("game5", "Game 5", 4);
	}
}
