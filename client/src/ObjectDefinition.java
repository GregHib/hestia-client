/* ObjectDefinition - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.event.ActionEvent;

public class ObjectDefinition
{
	protected int anInt2958;
	static int anInt2959;
	private byte aByte2960;
	protected boolean aBoolean2961 = false;
	protected int anInt2962;
	private int anInt2963;
	private int anInt2964;
	static int anInt2965;
	static int anInt2966;
	protected byte[] aByteArray2967;
	private int anInt2968;
	private int anInt2969;
	protected int anInt2970;
	protected int anInt2971;
	protected boolean aBoolean2972 = true;
	private int anInt2973 = 128;
	private byte aByte2974;
	protected int anInt2975;
	static int anInt2976;
	protected int anInt2977;
	static int anInt2978;
	static int anInt2979;
	protected String[] aStringArray2980;
	protected int[] anIntArray2981;
	static int anInt2982;
	protected int anInt2983;
	protected int[] anIntArray2984;
	private int anInt2985;
	protected int anInt2986;
	protected int anInt2987;
	protected short[] aShortArray2988;
	protected int anInt2989;
	protected boolean aBoolean2990;
	static int anInt2991;
	protected boolean aBoolean2992;
	protected boolean aBoolean2993;
	protected int anInt2994;
	private int[] anIntArray2995;
	private byte[] aByteArray2996;
	static int anInt2997;
	protected boolean aBoolean2998;
	protected String aString2999;
	protected boolean aBoolean3000;
	static int anInt3001;
	protected boolean aBoolean3002;
	static int anInt3003;
	static int anInt3004;
	static int anInt3005;
	protected int anInt3006;
	protected boolean aBoolean3007;
	protected int anInt3008;
	private int anInt3009;
	protected int anInt3010;
	protected int anInt3011;
	protected int anInt3012;
	protected int anInt3013;
	private HashTable aHashTable3014;
	protected int anInt3015;
	protected boolean aBoolean3016;
	private int anInt3017;
	private int anInt3018;
	private int[] anIntArray3019;
	protected int anInt3020;
	static int anInt3021;
	static int anInt3022;
	private int anInt3023;
	protected int anInt3024;
	protected short[] aShortArray3025;
	private int anInt3026;
	private byte aByte3027;
	protected Class112 aClass112_3028;
	static int anInt3029;
	protected int anInt3030;
	protected int[][] anIntArrayArray3031;
	protected int anInt3032;
	protected boolean aBoolean3033;
	protected boolean aBoolean3034;
	private int anInt3035;
	protected int[] anIntArray3036;
	private int anInt3037;
	protected int anInt3038;
	protected boolean aBoolean3039;
	protected int anInt3040;
	static int anInt3041;
	private int anInt3042;
	static int anInt3043;
	private short[] aShortArray3044;
	private byte aByte3045;
	static int anInt3046;
	private short[] aShortArray3047;
	static int anInt3048;
	static int anInt3049;
	protected int anInt3050;
	protected boolean aBoolean3051;
	private byte aByte3052;
	protected boolean aBoolean3053;
	static int anInt3054;
	protected int anInt3055;
	protected boolean aBoolean3056;
	protected int anInt3057;
	
	final boolean method3032() {
		anInt3022++;
		return anIntArray3019 != null && anIntArray3019.length > 1;
	}
	
	private void method3033(Buffer buffer, int i) {
		anInt2982++;
		if (i == 1 || i == 5) {
			if (i == 5 && aClass112_3028.aBoolean1424) {
				method3052(buffer);
			}
			int i_1_ = buffer.readUnsignedByte(255);
			aByteArray2967 = new byte[i_1_];
			anIntArrayArray3031 = new int[i_1_][];
			for (int i_2_ = 0; i_2_ < i_1_; i_2_++) {
				aByteArray2967[i_2_] = buffer.readByte();
				int i_3_ = buffer.readUnsignedByte(255);
				anIntArrayArray3031[i_2_] = new int[i_3_];
				for (int i_4_ = 0; i_3_ > i_4_; i_4_++)
					anIntArrayArray3031[i_2_][i_4_] = buffer.readShort(Node_Sub19.method2612(21120, -130526904));
			}
			if (i == 5 && !aClass112_3028.aBoolean1424) {
				method3052(buffer);
			}
		} else if (i == 2) {
			aString2999 = buffer.readString(-1);
		} else if (i == 14) {
			anInt3055 = buffer.readUnsignedByte(255);
		} else if (i == 15) {
			anInt2986 = buffer.readUnsignedByte(255);
		} else if (i == 17) {
			aBoolean3034 = false;
			anInt3010 = 0;
		} else if (i == 18) {
			aBoolean3034 = false;
		} else if (i == 19) {
			anInt3057 = buffer.readUnsignedByte(255);
		} else if (i == 21) {
			aByte3027 = (byte) 1;
		} else if (i == 22) {
			aBoolean3053 = true;
		} else if (i == 23) {
			anInt2977 = 1;
		} else if (i == 24) {
			int i_26_ = buffer.readShort(-130546744);
			if (i_26_ != 65535) {
				anIntArray3019 = new int[]{i_26_};
			}
		} else if (i == 27) {
			anInt3010 = 1;
		} else if (i == 28) {
			anInt3011 = buffer.readUnsignedByte(255) << 2;
		} else if (i == 29) {
			anInt3042 = buffer.readByte();
		} else if (i == 39) {
			anInt3026 = 5 * buffer.readByte();
		} else if (i >= 30 && i < 35) {
			aStringArray2980[i + -30] = buffer.readString(-1);
		} else if (i == 40) {
			int i_24_ = buffer.readUnsignedByte(255);
			aShortArray3025 = new short[i_24_];
			aShortArray3047 = new short[i_24_];
			for (int i_25_ = 0; i_25_ < i_24_; i_25_++) {
				aShortArray3047[i_25_] = (short) buffer.readShort(-130546744);
				aShortArray3025[i_25_] = (short) buffer.readShort(-130546744);
			}
		} else if (i == 41) {
			int i_22_ = buffer.readUnsignedByte(255);
			aShortArray2988 = new short[i_22_];
			aShortArray3044 = new short[i_22_];
			for (int i_23_ = 0; i_22_ > i_23_; i_23_++) {
				aShortArray3044[i_23_] = (short) buffer.readShort(-130546744);
				aShortArray2988[i_23_] = (short) buffer.readShort(-130546744);
			}
		} else if (i == 42) {
			int i_5_ = buffer.readUnsignedByte(255);
			aByteArray2996 = new byte[i_5_];
			for (int i_6_ = 0; i_6_ < i_5_; i_6_++)
				aByteArray2996[i_6_] = buffer.readByte();
		} else if (i == 62) {
			aBoolean3039 = true;
		} else if (i == 64) {
			aBoolean3033 = false;
		} else if (i == 65) {
			anInt2969 = buffer.readShort(-130546744);
		} else if (i == 66) {
			anInt2973 = buffer.readShort(-130546744);
		} else if (i == 67) {
			anInt3009 = buffer.readShort(-130546744);
		} else if (i == 69) {
			anInt3040 = buffer.readUnsignedByte(255);
		} else if (i == 70) {
			anInt2968 = buffer.readUnsignedShort(-69) << 2;
		} else if (i == 71) {
			anInt3037 = buffer.readUnsignedShort(-37) << 2;
		} else if (i == 72) {
			anInt2985 = buffer.readUnsignedShort(-29) << 2;
		} else if (i == 73) {
			aBoolean3051 = true;
		} else if (i == 74) {
			aBoolean3016 = true;
		} else if (i == 75) {
			anInt2970 = buffer.readUnsignedByte(255);
		} else if (i == 77 || i == 92) {
			anInt3017 = buffer.readShort(-130546744);
			if (anInt3017 == 65535) {
				anInt3017 = -1;
			}
			anInt3035 = buffer.readShort(-130546744);
			if (anInt3035 == 65535) {
				anInt3035 = -1;
			}
			int i_19_ = -1;
			if (i == 92) {
				i_19_ = buffer.readShort(-130546744);
				if (i_19_ == 65535) {
					i_19_ = -1;
				}
			}
			int i_20_ = buffer.readUnsignedByte(255);
			anIntArray2984 = new int[i_20_ + 2];
			for (int i_21_ = 0; i_20_ >= i_21_; i_21_++) {
				anIntArray2984[i_21_] = buffer.readShort(-130546744);
				if (anIntArray2984[i_21_] == 65535) {
					anIntArray2984[i_21_] = -1;
				}
			}
			anIntArray2984[1 + i_20_] = i_19_;
		} else if (i == 78) {
			anInt3015 = buffer.readShort(-130546744);
			anInt3012 = buffer.readUnsignedByte(255);
		} else if (i == 79) {
			anInt2989 = buffer.readShort(-130546744);
			anInt2971 = buffer.readShort(-130546744);
			anInt3012 = buffer.readUnsignedByte(255);
			int i_7_ = buffer.readUnsignedByte(255);
			anIntArray3036 = new int[i_7_];
			for (int i_8_ = 0; i_8_ < i_7_; i_8_++)
				anIntArray3036[i_8_] = buffer.readShort(-130546744);
		} else if (i == 81) {
			aByte3027 = (byte) 2;
			anInt3023 = 256 * buffer.readUnsignedByte(255);
		} else if (i == 82) {
			aBoolean2990 = true;
		} else if (i == 88) {
			aBoolean2972 = false;
		} else if (i == 89) {
			aBoolean3000 = false;
		} else if (i == 91) {
			aBoolean3002 = true;
		} else if (i == 93) {
			aByte3027 = (byte) 3;
			anInt3023 = buffer.readShort(-130546744);
		} else if (i == 94) {
			aByte3027 = (byte) 4;
		} else if (i == 95) {
			aByte3027 = (byte) 5;
			anInt3023 = buffer.readUnsignedShort(-67);
		} else if (i == 97) {
			aBoolean3056 = true;
		} else if (i == 98) {
			aBoolean2998 = true;
		} else if (i == 99) {
			anInt2987 = buffer.readUnsignedByte(255);
			anInt3008 = buffer.readShort(-130546744);
		} else if (i == 100) {
			anInt3038 = buffer.readUnsignedByte(255);
			anInt3013 = buffer.readShort(-130546744);
		} else if (i == 101) {
			anInt2958 = buffer.readUnsignedByte(255);
		} else if (i == 102) {
			anInt3006 = buffer.readShort(-130546744);
		} else if (i == 103) {
			anInt2977 = 0;
		} else if (i == 104) {
			anInt3024 = buffer.readUnsignedByte(255);
		} else if (i == 105) {
			aBoolean3007 = true;
		} else if (i == 106) {
			int i_9_ = buffer.readUnsignedByte(255);
			int i_10_ = 0;
			anIntArray3019 = new int[i_9_];
			anIntArray2995 = new int[i_9_];
			for (int i_11_ = 0; i_11_ < i_9_; i_11_++) {
				anIntArray3019[i_11_] = buffer.readShort(-130546744);
				if (anIntArray3019[i_11_] == 65535) {
					anIntArray3019[i_11_] = -1;
				}
				i_10_ += anIntArray2995[i_11_] = buffer.readUnsignedByte(255);
			}
			for (int i_12_ = 0; i_12_ < i_9_; i_12_++)
				anIntArray2995[i_12_] = 65535 * anIntArray2995[i_12_] / i_10_;
		} else if (i == 107) {
			anInt2994 = buffer.readShort(-130546744);
		} else if (i >= 150 && i < 155) {
			aStringArray2980[-150 + i] = buffer.readString(-1);
			if (!aClass112_3028.aBoolean1431) {
				aStringArray2980[-150 + i] = null;
			}
		} else if (i == 160) {
			int i_17_ = buffer.readUnsignedByte(255);
			anIntArray2981 = new int[i_17_];
			for (int i_18_ = 0; i_17_ > i_18_; i_18_++)
				anIntArray2981[i_18_] = buffer.readShort(-130546744);
		} else if (i == 162) {
			aByte3027 = (byte) 3;
			anInt3023 = buffer.readInt();
		} else if (i == 163) {
			aByte2974 = buffer.readByte();
			aByte3045 = buffer.readByte();
			aByte3052 = buffer.readByte();
			aByte2960 = buffer.readByte();
		} else if (i == 164) {
			anInt2964 = buffer.readUnsignedShort(-78);
		} else if (i == 165) {
			anInt2963 = buffer.readUnsignedShort(-20);
		} else if (i == 166) {
			anInt3018 = buffer.readUnsignedShort(-123);
		} else if (i == 167) {
			anInt2983 = buffer.readShort(-130546744);
		} else if (i == 168) {
			aBoolean2961 = true;
		} else if (i == 169) {
			aBoolean2993 = true;
		} else if (i == 170) {
			anInt3032 = buffer.readSmart();
		} else if (i == 171) {
			anInt2962 = buffer.readSmart();
		} else if (i == 173) {
			anInt3050 = buffer.readShort(-130546744);
			anInt3020 = buffer.readShort(-130546744);
		} else if (i == 177) {
			aBoolean2992 = true;
		} else if (i == 178) {
			anInt2975 = buffer.readUnsignedByte(255);
		} else if (i == 249) {
			int i_13_ = buffer.readUnsignedByte(255);
			if (aHashTable3014 == null) {
				int i_14_ = Class320_Sub19.method3753(i_13_);
				aHashTable3014 = new HashTable(i_14_);
			}
			for (int i_15_ = 0; i_15_ < i_13_; i_15_++) {
				boolean bool = buffer.readUnsignedByte(255) == 1;
				int i_16_ = buffer.readMedium(1819759595);
				Node node;
				if (bool) {
					node = new Node_Sub18(buffer.readString(-1));
				} else {
					node = new Node_Sub32(buffer.readInt());
				}
				aHashTable3014.method1515((long) i_16_, node, -125);
			}
		}
	}
	
	final String method3034(String string, int i, boolean bool) {
		anInt3005++;
		if (aHashTable3014 == null) {
			return string;
		}
		if (bool) {
			anInt3035 = 79;
		}
		Node_Sub18 node_sub18 = (Node_Sub18) aHashTable3014.method1518((long) i);
		if (node_sub18 == null) {
			return string;
		}
		return node_sub18.aString7149;
	}
	
	final boolean method3035(int i) {
		anInt3048++;
		if (anIntArray3019 != null && i != -1) {
			for (int i1 : anIntArray3019) {
				if (i == i1) {
					return true;
				}
			}
		}
		return false;
	}
	
	static void method3036(int i, int i_29_, int i_30_, Animable_Sub1 animable_sub1) {
		Class261 class261 = Node_Sub15_Sub6.method2572(i, i_29_, i_30_);
		if (class261 != null) {
			class261.anAnimable_Sub1_3317 = animable_sub1;
			int i_31_ = Class320_Sub10.aPlaneArray8300 == Class368.aPlaneArray4548 ? 1 : 0;
			if (animable_sub1.method814()) {
				if (animable_sub1.method813(0)) {
					animable_sub1.anAnimable5941 = SeekableFile.anAnimableArray3884[i_31_];
					SeekableFile.anAnimableArray3884[i_31_] = animable_sub1;
				} else {
					animable_sub1.anAnimable5941 = Class303.anAnimableArray3827[i_31_];
					Class303.anAnimableArray3827[i_31_] = animable_sub1;
					Class194_Sub1.aBoolean6892 = true;
				}
			} else {
				animable_sub1.anAnimable5941 = Node_Sub36.anAnimableArray7429[i_31_];
				Node_Sub36.anAnimableArray7429[i_31_] = animable_sub1;
			}
		}
	}
	
	final boolean method3037() {
		anInt2965++;
		return anIntArray3019 != null;
	}
	
	final ObjectDefinition method3038(int i, Interface17 interface17) {
		anInt2991++;
		int i_32_ = -1;
		if (i != 13) {
			method3032();
		}
		if (anInt3017 == -1) {
			if (anInt3035 != -1) {
				i_32_ = interface17.method65(anInt3035);
			}
		} else {
			i_32_ = interface17.method64(anInt3017, (byte) -42);
		}
		if (i_32_ < 0 || i_32_ >= -1 + anIntArray2984.length || anIntArray2984[i_32_] == -1) {
			int i_33_ = anIntArray2984[-1 + anIntArray2984.length];
			if (i_33_ != -1) {
				return aClass112_3028.method1145(i_33_, i + 99);
			}
			return null;
		}
		return aClass112_3028.method1145(anIntArray2984[i_32_], 81);
	}
	
	final boolean method3039() {
		anInt2978++;
		if (anIntArrayArray3031 == null) {
			return true;
		}
		boolean bool = true;
		synchronized (aClass112_3028.aClass302_1433) {
			for (int[] ints : anIntArrayArray3031) {
				for (int anInt : ints)
					bool &= aClass112_3028.aClass302_1433.method3515(0, anInt);
			}
		}
		return bool;
	}
	
	final boolean method3040() {
		anInt2976++;
		if (anIntArray2984 == null) {
			return anInt3015 != -1 || anIntArray3036 != null;
		}
		for (int i : anIntArray2984) {
			if (i != -1) {
				ObjectDefinition objectdefinition_37_ = aClass112_3028.method1145(i, 59);
				if (objectdefinition_37_.anInt3015 != -1 || objectdefinition_37_.anIntArray3036 != null) {
					return true;
				}
			}
		}
		return false;
	}
	
	final boolean method3041(int i, int i_38_) {
		anInt3043++;
		if (anIntArrayArray3031 == null) {
			return true;
		}
		synchronized (aClass112_3028.aClass302_1433) {
			if (i_38_ <= 12) {
				return true;
			}
			for (int i_39_ = 0; aByteArray2967.length > i_39_; i_39_++) {
				if (i == aByteArray2967[i_39_]) {
					for (int i_40_ = 0; anIntArrayArray3031[i_39_].length > i_40_; i_40_++) {
						if (!aClass112_3028.aClass302_1433.method3515(0, anIntArrayArray3031[i_39_][i_40_])) {
							return false;
						}
					}
					return true;
				}
			}
		}
		return true;
	}
	
	final void method3042(Buffer buffer) {
		for (;;) {
			int i = buffer.readUnsignedByte(255);
			if (i == 0) {
				break;
			}
			method3033(buffer, i);
		}
		anInt3049++;
	}
	
	final void method3043() {
		anInt2979++;
		if (anInt3057 == -1) {
			anInt3057 = 0;
			if (aByteArray2967 != null && aByteArray2967.length == 1 && aByteArray2967[0] == 10) {
				anInt3057 = 1;
			}
			for (int i_41_ = 0; i_41_ < 5; i_41_++) {
				if (aStringArray2980[i_41_] != null) {
					anInt3057 = 1;
					break;
				}
			}
		}
		if (anInt2970 == -1) {
			anInt2970 = anInt3010 == 0 ? 0 : 1;
		}
		if (method3037() || aBoolean2998 || anIntArray2984 != null) {
			aBoolean2992 = true;
		}
	}
	
	final int method3044() {
		anInt2966++;
		if (anIntArray3019 != null) {
			if (anIntArray3019.length <= 1) {
				return anIntArray3019[0];
			}
			int i_42_ = (int) (65535.0 * Math.random());
			for (int i_43_ = 0; anIntArray3019.length > i_43_; i_43_++) {
				if (i_42_ <= anIntArray2995[i_43_]) {
					return anIntArray3019[i_43_];
				}
				i_42_ -= anIntArray2995[i_43_];
			}
		}
		return -1;
	}
	
	static void method3045(long[] ls, Object[] objects, boolean bool) {
		anInt3046++;
		Class286.method3393(objects, (byte) -66, ls, 0, -1 + ls.length);
		if (!bool) {
			/* empty */
		}
	}
	
	final Class243 method3046(int i, int i_44_, int i_45_, Plane plane, boolean bool, int i_46_, int i_47_, GraphicsToolkit graphicstoolkit, Class121 class121, int i_48_, Plane plane_49_) {
		anInt3003++;
		if (Class243.method3062(i_47_, (byte) 51)) {
			i_47_ = 4;
		}
		long l = (long) (i + ((anInt3030 << 10) + (i_47_ << 3)));
		l |= (long) (graphicstoolkit.anInt1537 << 29);
		if (class121 != null) {
			l |= class121.aLong1522 << 32;
		}
		int i_50_ = i_48_;
		if (aByte3027 == 3) {
			i_50_ |= 0x7;
		} else {
			if (aByte3027 != 0 || anInt2963 != 0) {
				i_50_ |= 0x2;
			}
			if (anInt2964 != 0) {
				i_50_ |= 0x1;
			}
			if (anInt3018 != 0) {
				i_50_ |= 0x4;
			}
		}
		if (bool) {
			i_50_ |= 0x40000;
		}
		Class243 class243;
		synchronized (aClass112_3028.aClass61_1435) {
			class243 = (Class243) aClass112_3028.aClass61_1435.method607(l);
		}
		DrawableModel drawablemodel = class243 != null ? class243.aDrawableModel3064 : null;
		r var_r = null;
		if (drawablemodel == null || graphicstoolkit.b(drawablemodel.ua(), i_50_) != 0) {
			if (drawablemodel != null) {
				i_50_ = graphicstoolkit.c(i_50_, drawablemodel.ua());
			}
			int i_51_ = i_50_;
			if (i_47_ == 10 && i > 3) {
				i_51_ |= 0x5;
			}
			drawablemodel = method3051(graphicstoolkit, i_51_, class121, i, i_47_);
			if (drawablemodel == null) {
				return null;
			}
			if (i_47_ == 10 && i > 3) {
				drawablemodel.a(2048);
			}
			if (bool) {
				var_r = drawablemodel.ba(null);
			}
			drawablemodel.s(i_50_);
			class243 = new Class243();
			class243.aR3072 = var_r;
			class243.aDrawableModel3064 = drawablemodel;
			synchronized (aClass112_3028.aClass61_1435) {
				aClass112_3028.aClass61_1435.method601(class243, 25566, l);
			}
		} else {
			drawablemodel = class243.aDrawableModel3064;
			var_r = class243.aR3072;
			if (bool && var_r == null) {
				var_r = class243.aR3072 = drawablemodel.ba(null);
			}
		}
		boolean bool_52_ = aByte3027 != 0 && (plane != null || plane_49_ != null);
		boolean bool_53_ = anInt2964 != 0 || anInt2963 != 0 || anInt3018 != 0;
		if (bool_52_ || bool_53_) {
			drawablemodel = drawablemodel.method633((byte) 0, i_50_, true);
			if (bool_52_) {
				drawablemodel.p(aByte3027, anInt3023, plane, plane_49_, i_45_, i_46_, i_44_);
			}
			if (bool_53_) {
				drawablemodel.H(anInt2964, anInt2963, anInt3018);
			}
			drawablemodel.s(i_48_);
		} else {
			drawablemodel = drawablemodel.method633((byte) 0, i_48_, true);
		}
        Node_Sub21.aClass243_7185.aDrawableModel3064 = drawablemodel;
		Node_Sub21.aClass243_7185.aR3072 = var_r;
		return Node_Sub21.aClass243_7185;
	}
	
	static int method3047(int i) {
		anInt2997++;
		int i_55_ = (i * i >> 12) * i >> 12;
		int i_56_ = -61440 + 6 * i;
		int i_57_ = 40960 + (i * i_56_ >> 12);
		return i_57_ * i_55_ >> 12;
	}
	
	final DrawableModel method3048(int i, int i_58_, int i_59_, int i_60_, Plane plane, Animator animator, GraphicsToolkit graphicstoolkit, Plane plane_62_, Class121 class121, int i_63_, int i_64_) {
		anInt3021++;
		if (Class243.method3062(i_59_, (byte) 46)) {
			i_59_ = 4;
		}
		long l = (long) ((anInt3030 << 10) + ((i_59_ << 3) + i));
		int i_65_ = i_63_;
		l |= (long) (graphicstoolkit.anInt1537 << 29);
		if (class121 != null) {
			l |= class121.aLong1522 << 32;
		}
		if (animator != null) {
			i_63_ |= animator.method237((byte) -128);
		}
		if (aByte3027 == 3) {
			i_63_ |= 0x7;
		} else {
			if (aByte3027 != 0 || anInt2963 != 0) {
				i_63_ |= 0x2;
			}
			if (anInt2964 != 0) {
				i_63_ |= 0x1;
			}
			if (anInt3018 != 0) {
				i_63_ |= 0x4;
			}
		}
		if (i_59_ == 10 && i > 3) {
			i_63_ |= 0x5;
		}
		DrawableModel drawablemodel;
		synchronized (aClass112_3028.aClass61_1436) {
			drawablemodel = (DrawableModel) aClass112_3028.aClass61_1436.method607(l);
		}
		if (drawablemodel == null || graphicstoolkit.b(drawablemodel.ua(), i_63_) != 0) {
			if (drawablemodel != null) {
				i_63_ = graphicstoolkit.c(i_63_, drawablemodel.ua());
			}
			drawablemodel = method3051(graphicstoolkit, i_63_, class121, i, i_59_);
			if (drawablemodel == null) {
				return null;
			}
			synchronized (aClass112_3028.aClass61_1436) {
				aClass112_3028.aClass61_1436.method601(drawablemodel, 25566, l);
			}
		}
		boolean bool = false;
		if (animator != null) {
			drawablemodel = drawablemodel.method633((byte) 1, i_63_, true);
			bool = true;
			animator.method225(drawablemodel, 0x3 & i);
		}
		if (i_59_ == 10 && i > 3) {
			if (!bool) {
				drawablemodel = drawablemodel.method633((byte) 3, i_63_, true);
				bool = true;
			}
			drawablemodel.a(2048);
		}
		if (aByte3027 != 0) {
			if (!bool) {
				bool = true;
				drawablemodel = drawablemodel.method633((byte) 3, i_63_, true);
			}
			drawablemodel.p(aByte3027, anInt3023, plane_62_, plane, i_60_, i_64_, i_58_);
		}
		if (anInt2964 != 0 || anInt2963 != 0 || anInt3018 != 0) {
			if (!bool) {
				drawablemodel = drawablemodel.method633((byte) 3, i_63_, true);
				bool = true;
			}
			drawablemodel.H(anInt2964, anInt2963, anInt3018);
		}
		if (bool) {
			drawablemodel.s(i_65_);
		}
		return drawablemodel;
	}
	
	static void method3049(SignLink signlink, Object object, byte b) {
		anInt3001++;
		if (b <= 63) {
			method3045(null, null, true);
		}
		if (signlink.anEventQueue4002 != null) {
			for (int i = 0; i < 50; i++) {
				if (signlink.anEventQueue4002.peekEvent() == null) {
					break;
				}
				Class262_Sub22.method3208(1L);
			}
			do {
				try {
					if (object == null) {
						break;
					}
					signlink.anEventQueue4002.postEvent(new ActionEvent(object, 1001, "dummy"));
				} catch (Exception exception) {
					break;
				}
				break;
			} while (false);
		}
	}
	
	static int method3050(int i) {
		anInt3041++;
		return 0xff & i;
	}
	
	private DrawableModel method3051(GraphicsToolkit graphicstoolkit, int i, Class121 class121, int i_66_, int i_67_) {
		anInt3054++;
		int i_68_ = 64 + anInt3042;
		int i_69_ = anInt3026 + 850;
		int i_70_ = i;
		boolean bool = aBoolean3039 || i_67_ == 2 && i_66_ > 3;
		if (bool) {
			i |= 0x10;
		}
		if (i_66_ == 0) {
			if (anInt2969 != 128 || anInt2968 != 0) {
				i |= 0x1;
			}
			if (anInt3009 != 128 || anInt2985 != 0) {
				i |= 0x4;
			}
		} else {
			i |= 0xd;
		}
		if (anInt2973 != 128 || anInt3037 != 0) {
			i |= 0x2;
		}
		if (aShortArray3047 != null) {
			i |= 0x4000;
		}
		if (aShortArray3044 != null) {
			i |= 0x8000;
		}
		if (aByte2960 != 0) {
			i |= 0x80000;
		}
		DrawableModel drawablemodel = null;
		if (aByteArray2967 == null) {
			return null;
		}
		int i_71_ = -1;
		for (int i_72_ = 0; aByteArray2967.length > i_72_; i_72_++) {
			if (aByteArray2967[i_72_] == i_67_) {
				i_71_ = i_72_;
				break;
			}
		}
		if (i_71_ == -1) {
			return null;
		}
		int[] is = class121 == null || class121.anIntArray1526 == null ? anIntArrayArray3031[i_71_] : class121.anIntArray1526;
		int i_73_ = is.length;
		if (i_73_ > 0) {
			long l = (long) graphicstoolkit.anInt1537;
			for (int i1 : is) l = l * 67783L + (long) i1;
			synchronized (aClass112_3028.aClass61_1434) {
				drawablemodel = (DrawableModel) aClass112_3028.aClass61_1434.method607(l);
			}
			if (drawablemodel != null) {
				if (drawablemodel.WA() != i_68_) {
					i |= 0x1000;
				}
				if (drawablemodel.da() != i_69_) {
					i |= 0x2000;
				}
			}
			if (drawablemodel == null || graphicstoolkit.b(drawablemodel.ua(), i) != 0) {
				int i_75_ = i | 0x1f01f;
				if (drawablemodel != null) {
					i_75_ = graphicstoolkit.c(i_75_, drawablemodel.ua());
				}
				Model model = null;
				synchronized (Animable_Sub3_Sub1.aModelArray11008) {
					for (int i_76_ = 0; i_76_ < i_73_; i_76_++) {
						synchronized (aClass112_3028.aClass302_1433) {
							model = Renderer.method3448(is[i_76_] & 0xffff, 7, aClass112_3028.aClass302_1433);
						}
						if (model == null) {
							return null;
						}
						if (model.anInt2614 < 13) {
							model.method2081();
						}
						if (i_73_ > 1) {
							Animable_Sub3_Sub1.aModelArray11008[i_76_] = model;
						}
					}
					if (i_73_ > 1) {
						model = new Model(Animable_Sub3_Sub1.aModelArray11008, i_73_);
					}
				}
				drawablemodel = graphicstoolkit.a(model, i_75_, aClass112_3028.anInt1437, i_68_, i_69_);
				synchronized (aClass112_3028.aClass61_1434) {
					aClass112_3028.aClass61_1434.method601(drawablemodel, 25566, l);
				}
			}
		}
		if (drawablemodel == null) {
			return null;
		}
		DrawableModel drawablemodel_77_ = drawablemodel.method633((byte) 0, i, true);
		if (i_68_ != drawablemodel.WA()) {
			drawablemodel_77_.C(i_68_);
		}
		if (drawablemodel.da() != i_69_) {
			drawablemodel_77_.LA(i_69_);
		}
		if (bool) {
			drawablemodel_77_.v();
		}
		if (i_67_ == 4 && i_66_ > 3) {
			drawablemodel_77_.k(2048);
			drawablemodel_77_.H(180, 0, -180);
		}
		i_66_ &= 0x3;
		if (i_66_ == 1) {
			drawablemodel_77_.k(4096);
		} else if (i_66_ == 2) {
			drawablemodel_77_.k(8192);
		} else if (i_66_ == 3) {
			drawablemodel_77_.k(12288);
		}
		if (aShortArray3047 != null) {
			short[] ses;
			if (class121 == null || class121.aShortArray1523 == null) {
				ses = aShortArray3025;
			} else {
				ses = class121.aShortArray1523;
			}
			for (int i_78_ = 0; aShortArray3047.length > i_78_; i_78_++) {
				if (aByteArray2996 != null && aByteArray2996.length > i_78_) {
					drawablemodel_77_.ia(aShortArray3047[i_78_], EntityNode_Sub3_Sub1.aShortArray9165[0xff & aByteArray2996[i_78_]]);
				} else {
					drawablemodel_77_.ia(aShortArray3047[i_78_], ses[i_78_]);
				}
			}
		}
		if (aShortArray3044 != null) {
			short[] ses;
			if (class121 == null || class121.aShortArray1524 == null) {
				ses = aShortArray2988;
			} else {
				ses = class121.aShortArray1524;
			}
			for (int i_79_ = 0; aShortArray3044.length > i_79_; i_79_++)
				drawablemodel_77_.aa(aShortArray3044[i_79_], ses[i_79_]);
		}
		if (aByte2960 != 0) {
			drawablemodel_77_.method626(aByte2974, aByte3045, aByte3052, aByte2960 & 0xff);
		}
		if (anInt2969 != 128 || anInt2973 != 128 || anInt3009 != 128) {
			drawablemodel_77_.O(anInt2969, anInt2973, anInt3009);
		}
		if (anInt2968 != 0 || anInt3037 != 0 || anInt2985 != 0) {
			drawablemodel_77_.H(anInt2968, anInt3037, anInt2985);
		}
		drawablemodel_77_.s(i_70_);
		return drawablemodel_77_;
	}
	
	private void method3052(Buffer buffer) {
		anInt3004++;
		int i = buffer.readUnsignedByte(255);
		for (int i_80_ = 0; i > i_80_; i_80_++) {
			buffer.anInt7002++;
			int i_81_ = buffer.readUnsignedByte(255);
			buffer.anInt7002 += 2 * i_81_;
		}
	}
	
	final int method3053(int i, int i_82_, int i_83_) {
		anInt3029++;
		if (aHashTable3014 == null) {
			return i;
		}
		Node_Sub32 node_sub32 = (Node_Sub32) aHashTable3014.method1518((long) i_83_);
		if (i_82_ < 6) {
			method3034(null, 124, true);
		}
		if (node_sub32 == null) {
			return i;
		}
		return node_sub32.anInt7381;
	}
	
	public ObjectDefinition() {
		anInt2963 = 0;
		aByte2960 = (byte) 0;
		anInt2983 = 0;
		anInt2971 = 0;
		anInt2986 = 1;
		anInt2987 = -1;
		anInt2975 = 0;
		anInt2964 = 0;
		anInt2969 = 128;
		aString2999 = "null";
		anInt2985 = 0;
		aBoolean3002 = false;
		anInt2970 = -1;
		anInt2994 = -1;
		anInt2977 = -1;
		anInt3012 = 0;
		anInt3015 = -1;
		anInt3017 = -1;
		aBoolean3016 = false;
		aBoolean3007 = false;
		anInt3009 = 128;
		anInt3018 = 0;
		anInt3024 = 255;
		anInt2958 = 0;
		anIntArray3019 = null;
		anInt3023 = -1;
		anInt2989 = 0;
		anInt3008 = -1;
		anInt3032 = 960;
		aBoolean3000 = true;
		anInt3011 = 64;
		anIntArray2995 = null;
		aByte3027 = (byte) 0;
		anInt3020 = 256;
		anInt3038 = -1;
		anInt2968 = 0;
		anInt3042 = 0;
		anInt3010 = 2;
		aBoolean2993 = false;
		aBoolean2998 = false;
		anInt3037 = 0;
		anInt3040 = 0;
		anInt3050 = 256;
		anInt3026 = 0;
		aBoolean3051 = false;
		aBoolean2990 = false;
		aBoolean3034 = true;
		aBoolean2992 = false;
		aBoolean3053 = false;
		anInt3055 = 1;
		anInt2962 = 0;
		anInt3006 = -1;
		anInt3035 = -1;
		anInt3013 = -1;
		aBoolean3033 = true;
		aBoolean3056 = false;
		aBoolean3039 = false;
		anInt3057 = -1;
	}
}
