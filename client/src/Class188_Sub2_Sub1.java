/* Class188_Sub2_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.net.URL;

public class Class188_Sub2_Sub1 extends Class188_Sub2
{
	static int anInt9350;
	private byte[] aByteArray9351;
	static int anInt9352;
	static int anInt9353;
	static Class229 aClass229_9354;
	static int anInt9355;
	static Class192 aClass192_9356 = new Class192(70, -2);
	static int anInt9357;
	static int anInt9358;
	
	public Class188_Sub2_Sub1() {
		super();
	}
	
	final void method1905(byte b, int i, byte b_0_) {
		if (b != -11) {
			aByteArray9351 = null;
		}
		anInt9355++;
		b_0_ = (byte) ((b_0_ >> 1 & 0x7f) + 127);
		int i_1_ = i * 2;
		aByteArray9351[i_1_++] = b_0_;
		aByteArray9351[i_1_] = b_0_;
	}
	
	public static void method1906() {
		aClass229_9354 = null;
		aClass192_9356 = null;
	}
	
	static void method1907(int i, byte b) {
		anInt9353++;
		CacheNode_Sub7.method2316();
		if (b >= 92) {
			int i_2_ = aClass229_9354.method2123(i).anInt3682;
			if (i_2_ != 0) {
				int i_3_ = Class24.aClass275_442.anIntArray5417[i];
				if (i_2_ == 5) {
					Class121.anInt1521 = i_3_;
				}
				if (i_2_ == 6) {
					Class134_Sub1.anInt9014 = i_3_;
				}
			}
		}
	}
	
	static int method1908(int i) {
		if (i != 3) {
			method1907(127, (byte) 11);
		}
		anInt9352++;
		if (DrawableModel.aFrame907 != null) {
			return 3;
		}
		if (!EntityNode.aBoolean1128) {
			return 1;
		}
		return 2;
	}
	
	final byte[] method1909() {
		aByteArray9351 = new byte[524288];
		anInt9358++;
		this.method1884();
		return aByteArray9351;
	}
	
	static Class241 method1910(SignLink signlink, String string, String string_7_, int i) {
		anInt9357++;
		if (i == 0) {
			return signlink.method3636(string);
		}
		if (i == 1) {
			try {
				Object object = Class2.method166(Class96.anApplet1270, string_7_, new Object[] { new URL(Class96.anApplet1270.getCodeBase(), string).toString() });
				if (object == null) {
					throw new RuntimeException();
				}
				Class241 class241 = new Class241();
				class241.anInt2953 = 1;
				return class241;
			} catch (Throwable throwable) {
				Class241 class241 = new Class241();
				class241.anInt2953 = 2;
				return class241;
			}
		}
		if (i == 2) {
			try {
				Class96.anApplet1270.getAppletContext().showDocument(new URL(Class96.anApplet1270.getCodeBase(), string), "_blank");
				Class241 class241 = new Class241();
				class241.anInt2953 = 1;
				return class241;
			} catch (Exception exception) {
				Class241 class241 = new Class241();
				class241.anInt2953 = 2;
				return class241;
			}
		}
		if (i == 3) {
			try {
				Class2.method167("loggedout", Class96.anApplet1270, (byte) -88);
			} catch (Throwable throwable) {
				/* empty */
			}
			try {
				Class96.anApplet1270.getAppletContext().showDocument(new URL(Class96.anApplet1270.getCodeBase(), string), "_top");
				Class241 class241 = new Class241();
				class241.anInt2953 = 1;
				return class241;
			} catch (Exception exception) {
				Class241 class241 = new Class241();
				class241.anInt2953 = 2;
				return class241;
			}
		}
		throw new IllegalArgumentException();
	}
}
