/* Class41_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

import sun.net.www.protocol.http.AuthenticationInfo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.*;
import java.nio.charset.Charset;
import java.util.List;

public class Class41_Sub2 extends Class41
{
	private ProxySelector aProxySelector5548 = ProxySelector.getDefault();
	
	private Socket method438(Proxy proxy) throws IOException {
		if (proxy.type() == Proxy.Type.DIRECT) {
			return method434();
		}
		java.net.SocketAddress socketaddress = proxy.address();
		if (!(socketaddress instanceof InetSocketAddress)) {
			return null;
		}
        InetSocketAddress inetsocketaddress = (InetSocketAddress) socketaddress;
		if (proxy.type() == Proxy.Type.HTTP) {
			String string = null;
			try {
				Method method = AuthenticationInfo.class.getDeclaredMethod("getProxyAuth", String.class, Integer.TYPE);
				method.setAccessible(true);
				Object object = method.invoke(null, inetsocketaddress.getHostName(), inetsocketaddress.getPort());
				if (null != object) {
					Method method_0_ = AuthenticationInfo.class.getDeclaredMethod("supportsPreemptiveAuthorization");
					method_0_.setAccessible(true);
					if ((Boolean) method_0_.invoke(object, new Object[0])) {
						Method method_1_ = AuthenticationInfo.class.getDeclaredMethod("getHeaderName");
						method_1_.setAccessible(true);
						Method method_2_ = AuthenticationInfo.class.getDeclaredMethod("getHeaderValue", URL.class, String.class);
						method_2_.setAccessible(true);
						String string_3_ = (String) method_1_.invoke(object, new Object[0]);
						String string_4_ = (String) method_2_.invoke(object, new Object[] { new URL("https://" + aString625 + "/"), "https" });
						string = string_3_ + ": " + string_4_;
					}
				}
			} catch (Exception exception) {
				/* empty */
			}
			return method439(inetsocketaddress.getHostName(), inetsocketaddress.getPort(), string);
		}
		if (proxy.type() == Proxy.Type.SOCKS) {
			Socket socket = new Socket(proxy);
			socket.connect(new InetSocketAddress(aString625, anInt624));
			return socket;
		}
		return null;
	}
	
	private Socket method439(String string, int i, String string_5_) throws IOException {
		Socket socket = new Socket(string, i);
		socket.setSoTimeout(10000);
		OutputStream outputstream = socket.getOutputStream();
		if (string_5_ == null) {
			outputstream.write(("CONNECT " + aString625 + ":" + anInt624 + " HTTP/1.0\n\n").getBytes(Charset.forName("ISO-8859-1")));
		} else {
			outputstream.write(("CONNECT " + aString625 + ":" + anInt624 + " HTTP/1.0\n" + string_5_ + "\n\n").getBytes(Charset.forName("ISO-8859-1")));
		}
		outputstream.flush();
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		String string_6_ = bufferedreader.readLine();
		if (string_6_ != null) {
			if (string_6_.startsWith("HTTP/1.0 200") || string_6_.startsWith("HTTP/1.1 200")) {
				return socket;
			}
			if (string_6_.startsWith("HTTP/1.0 407") || string_6_.startsWith("HTTP/1.1 407")) {
				int i_7_ = 0;
				String string_8_ = "proxy-authenticate: ";
				for (string_6_ = bufferedreader.readLine(); string_6_ != null && i_7_ < 50; string_6_ = bufferedreader.readLine()) {
					if (string_6_.toLowerCase().startsWith(string_8_)) {
						string_6_ = string_6_.substring(string_8_.length()).trim();
						int i_9_ = string_6_.indexOf(' ');
						if (-1 != i_9_) {
							string_6_ = string_6_.substring(0, i_9_);
						}
						throw new IOException_Sub1(string_6_);
					}
					i_7_++;
				}
				throw new IOException_Sub1("");
			}
		}
		outputstream.close();
		bufferedreader.close();
		socket.close();
		return null;
	}
	
	final Socket method435() throws IOException {
		boolean bool = Boolean.parseBoolean(System.getProperty("java.net.useSystemProxies"));
		if (!bool) {
			System.setProperty("java.net.useSystemProxies", "true");
		}
		boolean bool_10_ = 443 == anInt624;
		List list;
		List list_11_;
		try {
			list = aProxySelector5548.select(new URI((bool_10_ ? "https" : "http") + "://" + aString625));
			list_11_ = aProxySelector5548.select(new URI((!bool_10_ ? "https" : "http") + "://" + aString625));
        } catch (URISyntaxException urisyntaxexception) {
			return method434();
		}
		list.addAll(list_11_);
		Object[] objects = list.toArray();
		IOException_Sub1 ioexception_sub1 = null;
        for (Object object : objects) {
            Proxy proxy = (Proxy) object;
            Socket socket;
            try {
                Socket socket_14_ = method438(proxy);
                if (null == socket_14_) {
                    continue;
                }
                socket = socket_14_;
            } catch (IOException_Sub1 ioexception_sub1_15_) {
                ioexception_sub1 = ioexception_sub1_15_;
                continue;
            } catch (IOException ioexception) {
                continue;
            }
            return socket;
        }
		if (ioexception_sub1 != null) {
			throw ioexception_sub1;
		}
		return method434();
	}
}
