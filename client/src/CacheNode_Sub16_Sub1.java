/* CacheNode_Sub16_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class CacheNode_Sub16_Sub1 extends CacheNode_Sub16
{
	static int anInt11068;
	protected byte aByte11069;
	protected int anInt11070;
	static int anInt11071;
	protected Buffer aBuffer11072;
	static Class192 aClass192_11073 = new Class192(61, 20);
	static int anInt11074;
	static int anInt11075;
	static Class124 aClass124_11076 = new Class124(0);
	
	static void method2387() {
		if (AnimableAnimator.aClass42_5498 != null) {
			AnimableAnimator.aClass42_5498.method453((byte) -112);
		}
		anInt11074++;
		if (Packet.aClass42_9402 != null) {
			Packet.aClass42_9402.method453((byte) -115);
		}
	}
	
	final byte[] method2385() {
		anInt11071++;
		if (aBoolean9602 || aBuffer11072.anInt7002 < aBuffer11072.aByteArray7019.length - aByte11069) {
			throw new RuntimeException();
		}
		return aBuffer11072.aByteArray7019;
	}
	
	public static void method2388() {
		aClass192_11073 = null;
		aClass124_11076 = null;
	}
	
	final int method2386() {
		anInt11068++;
		if (aBuffer11072 == null) {
			return 0;
		}
		return aBuffer11072.anInt7002 * 100 / (-aByte11069 + aBuffer11072.aByteArray7019.length);
	}
	
	static Class377_Sub1 method2389() {
		anInt11075++;
		if (Class180.aClass377_Sub1Array2143.length > Node_Sub5.anInt7029) {
			return Class180.aClass377_Sub1Array2143[Node_Sub5.anInt7029++];
		}
		return null;
	}
	
	CacheNode_Sub16_Sub1() {
		/* empty */
	}
}
