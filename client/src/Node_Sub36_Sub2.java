/* Node_Sub36_Sub2 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Node_Sub36_Sub2 extends Node_Sub36
{
	private byte aByte10041;
	static int anInt10042 = 50;
	private String aString10043;
	static Class59[] aClass59Array10044 = new Class59[anInt10042];
	static int anInt10045;
	static int anInt10046 = 0;
	static int[][] anIntArrayArray10047;
	static int anInt10048;
	private byte aByte10049;
	static int[] anIntArray10050;
	static int[] anIntArray10051 = new int[anInt10042];
	static int[] anIntArray10052;
	static int anInt10053;
	
	public static void method2760() {
		aClass59Array10044 = null;
		anIntArray10050 = null;
		anIntArray10052 = null;
		anIntArray10051 = null;
		anIntArrayArray10047 = null;
	}
	
	final void method2751(Node_Sub43 node_sub43) {
		if (aString10043 != null) {
			node_sub43.aByte7532 = aByte10049;
			node_sub43.aByte7540 = aByte10041;
		}
		anInt10048++;
		node_sub43.aString7544 = aString10043;
	}
	
	final void method2756(Buffer buffer) {
		aString10043 = buffer.method2240();
		anInt10045++;
		if (aString10043 != null) {
			buffer.readUnsignedByte(255);
			aByte10041 = buffer.readByte();
			aByte10049 = buffer.readByte();
		}
	}
	
	static void method2761() {
		Class290_Sub7.aClass71_8134.method740();
		anInt10053++;
		r.aClass58_9578.method574();
		Class42.aClass181_643.method1827();
		Class186.aClass112_2256.method1142(118);
		Class366.aClass279_4526.method3370((byte) -40);
		EntityNode_Sub3_Sub1.aClass86_9166.method1000();
		Class18.aClass37_306.method398();
		Class16.aClass101_228.method1095();
		IOException_Sub1.aClass128_85.method1552((byte) 101);
		Class188_Sub2_Sub1.aClass229_9354.method2125();
		Class336.aClass315_4173.method3656();
		Class291_Sub1.aClass13_8198.method213();
		InputStream_Sub2.aClass281_83.method3379();
		Node_Sub54.aClass338_7671.method3908();
		Class304.aClass215_3834.method2071();
		Class146.aClass32_1812.method357();
		Class188_Sub2_Sub2.aClass36_9366.method393();
		Class171.aClass278_2062.method3365();
		CacheNode_Sub6.aClass57_9480.method569();
		Class32.aClass359_508.method4037();
		Class186.aClass239_2249.method3027();
		Node_Sub9_Sub4.aClass180_9727.method1818();
		Class296.aClass186_5429.method1871();
		Node_Sub24.method2642();
		Class359.method4036();
		Class262_Sub2.method3152();
		Node_Sub38_Sub14.method2835();
		if (Node_Sub38_Sub1.aClass329_10086 != Class240.aClass329_2943) {
			for (int i = 0; Class93.aByteArrayArray1244.length > i; i++)
				Class93.aByteArrayArray1244[i] = null;
			Class57.anInt849 = 0;
		}
		StandardDrawableModel.method700();
		Class320_Sub27.method3782(false);
		Class198.method2004();
		Class296.method3476(-110);
		Class226.method2113();
		Class305.aClass61_3867.method608(false);
		Class93.aGraphicsToolkit1241.q();
		Class145.method1639();
		Class59.method591();
		Class181.aClass302_2158.method3521();
		Class75.aClass302_1003.method3521();
		Class4.aClass302_122.method3521();
		Class293.aClass302_3681.method3521();
		Class148.aClass302_1827.method3521();
		Animable_Sub1_Sub1.aClass302_10618.method3521();
		SeekableFile.aClass302_3881.method3521();
		Class107.aClass302_1364.method3521();
		Node_Sub38_Sub39.aClass302_10500.method3521();
		Class282.aClass302_3583.method3521();
		Class250.aClass302_3179.method3521();
		Class179.aClass302_2132.method3521();
		Class21.aClass302_357.method3521();
		AnimableAnimator_Sub1.aClass302_9091.method3521();
		Class78.aClass302_1033.method3521();
		Class218.aClass302_2563.method3521();
		Class247.aClass302_3143.method3521();
		Class14.aClass302_219.method3521();
		Class174.aClass302_2093.method3521();
		Class205.aClass302_5104.method3521();
		Class262_Sub23.aClass302_7886.method3521();
		Node_Sub15_Sub5.aClass302_9805.method3521();
		Class127.aClass302_1646.method3521();
		r_Sub1.aClass302_11047.method3521();
		Node_Sub38_Sub4.aClass302_10116.method3521();
		Node_Sub18.aClass302_7150.method3521();
		Node_Sub38_Sub10.aClass302_10197.method3521();
		CacheNode_Sub5.aClass302_9477.method3521();
		Class176.aClass302_2102.method3521();
		Node_Sub15_Sub10.aClass302_9853.method3521();
		Class262_Sub3.aClass302_7715.method3521();
		Class326.aClass302_4096.method3521();
		Class262_Sub2.aClass302_7707.method3521();
		Node_Sub38_Sub38.aClass302_10484.method3521();
		Class125.aClass61_1609.method608(false);
		Node_Sub51.aClass61_7628.method608(false);
		Class262_Sub18.aClass61_7842.method608(false);
		CacheNode_Sub12.aClass61_9556.method608(false);
	}
	
	Node_Sub36_Sub2() {
		/* empty */
	}
	
	static {
		anIntArray10050 = new int[anInt10042];
		anIntArrayArray10047 = new int[128][128];
		anIntArray10052 = new int[anInt10042];
	}
}
