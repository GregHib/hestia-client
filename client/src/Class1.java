/* Class1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class1
{
	protected boolean aBoolean105 = true;
	protected short[][] aShortArrayArray106;
	static int anInt107;
	protected int[] anIntArray108 = null;
	static int[] anIntArray109 = { 19, 55, 38, 155, 255, 110, 137, 205, 76 };
	protected int anInt110 = -1;
	protected boolean aBoolean111;
	protected int anInt112;
	protected short[][][] aShortArrayArrayArray113;
	static int anInt114;
	static int anInt115 = -1;
	protected int anInt116;
	protected int anInt117 = 4;
	protected int anInt118;
	protected int[] anIntArray119;
	protected int anInt120;
	
	private void method163(Buffer buffer) {
		anInt114++;
		boolean bool = false;
		for (;;) {
			int i_0_ = buffer.readUnsignedByte(255);
			if (i_0_ == 0) {
				break;
			}
			if (i_0_ == 1) {
				if (anIntArray119 == null) {
					anIntArray119 = new int[4];
					anIntArray108 = new int[4];
					anInt117 = 4;
				}
				bool = true;
				for (int i_7_ = 0; i_7_ < anIntArray119.length; i_7_++) {
					anIntArray119[i_7_] = buffer.readUnsignedShort(Node_Sub19.method2612(10200, -10172));
					anIntArray108[i_7_] = buffer.readUnsignedShort(-31);
				}
			} else if (i_0_ == 2) {
                anInt110 = buffer.readShort(-130546744);
            } else if (i_0_ == 3) {
                anInt117 = buffer.readUnsignedByte(255);
                anIntArray108 = new int[anInt117];
                anIntArray119 = new int[anInt117];
            } else if (i_0_ != 4) {
                if (i_0_ == 5) {
                    anInt120 = buffer.readMedium(1819759595);
                } else if (i_0_ == 6) {
                    anInt112 = buffer.readMedium(1819759595);
                } else if (i_0_ == 7) {
                    aShortArrayArrayArray113 = new short[10][4][];
                    aShortArrayArray106 = new short[10][4];
                    for (int i_1_ = 0; i_1_ < 10; i_1_++) {
                        for (int i_2_ = 0; i_2_ < 4; i_2_++) {
                            int i_3_ = buffer.readShort(-130546744);
                            if (i_3_ == 65535) {
                                i_3_ = -1;
                            }
                            aShortArrayArray106[i_1_][i_2_] = (short) i_3_;
                            int i_4_ = buffer.readShort(-130546744);
                            aShortArrayArrayArray113[i_1_][i_2_] = new short[i_4_];
                            for (int i_5_ = 0; i_4_ > i_5_; i_5_++) {
                                int i_6_ = buffer.readShort(-130546744);
                                if (i_6_ == 65535) {
                                    i_6_ = -1;
                                }
                                aShortArrayArrayArray113[i_1_][i_2_][i_5_] = (short) i_6_;
                            }
                        }
                    }
                } else if (i_0_ == 8) {
                    aBoolean105 = false;
                } else if (i_0_ == 9) {
                    anInt116 = buffer.readUnsignedByte(255);
                } else if (i_0_ == 10) {
                    aBoolean111 = false;
                } else if (i_0_ == 11) {
                    anInt118 = buffer.readUnsignedByte(255);
                }
            }
        }
		if (!bool) {
			if (anIntArray119 == null) {
				anIntArray108 = new int[4];
				anIntArray119 = new int[4];
				anInt117 = 4;
			}
			for (int i_8_ = 0; i_8_ < anIntArray119.length; i_8_++) {
				anIntArray119[i_8_] = 0;
				anIntArray108[i_8_] = i_8_ * 20;
			}
		}
    }
	
	static short[] method164(short[] ses, byte b, int i) {
		anInt107++;
		short[] ses_9_ = new short[i];
		if (b > -11) {
			anIntArray109 = null;
		}
		Class311.method3606(ses, 0, ses_9_, 0, i);
		return ses_9_;
	}
	
	public static void method165() {
		anIntArray109 = null;
	}
	
	Class1(Class302 class302) {
		aBoolean111 = true;
		anInt116 = 2;
		anIntArray119 = null;
		anInt118 = 3;
		try {
			byte[] bs = class302.method3518(3);
			method163(new Buffer(bs));
		} catch (RuntimeException runtimeexception) {
			throw Class126.method1537(runtimeexception, "aba.<init>(" + (class302 != null ? "{...}" : "null") + ')');
		}
	}
}
