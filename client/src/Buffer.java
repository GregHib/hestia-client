/* Buffer - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.math.BigInteger;

class Buffer extends Node
{
	static int anInt6952;
	static int anInt6953;
	static int anInt6954;
	static int anInt6955;
	static int anInt6956;
	static int anInt6957;
	static int anInt6958;
	static int anInt6959;
	static int anInt6960;
	static int anInt6961;
	static int anInt6962;
	static int anInt6963;
	static int anInt6964;
	static int anInt6965;
	static int anInt6966;
	static int anInt6967;
	static int anInt6968;
	static int anInt6969;
	static int anInt6970;
	static int anInt6971;
	static int anInt6972;
	static int anInt6973;
	static int anInt6974;
	static int anInt6975;
	static int anInt6976;
	static int anInt6977;
	static int anInt6978;
	static int anInt6979;
	static int anInt6980;
	static int anInt6981;
	static int anInt6982;
	static int anInt6983;
	static int anInt6984;
	static int anInt6985;
	static int anInt6986;
	static int anInt6987;
	static int anInt6988;
	static int anInt6989;
	static int anInt6990;
	static int anInt6991;
	static int anInt6992;
	static int anInt6993;
	static int anInt6994;
	static int anInt6995;
	static int anInt6996;
	static int anInt6997;
	static int anInt6998;
	static int anInt6999;
	static int anInt7000;
	static int anInt7001;
	protected int anInt7002;
	static int anInt7003;
	static int anInt7004;
	static int anInt7005;
	static int anInt7006;
	static int anInt7007;
	static int anInt7008;
	static int anInt7009;
	static int anInt7010;
	static int anInt7011;
	static int anInt7012;
	static int anInt7013;
	static Class192 aClass192_7014 = new Class192(136, 6);
	static int anInt7015;
	static int anInt7016;
	static int anInt7017;
	static int anInt7018;
	protected byte[] aByteArray7019;
	static int anInt7020;
	static int anInt7021;
	static int anInt7022;
	static int anInt7023;
	static int anInt7024;
	static int anInt7025;
	
	final int readIntLittle(byte b) {
		if (b > -116) {
			method2241(-29);
		}
		anInt7004++;
		anInt7002 += 4;
		return (0xff & aByteArray7019[-4 + anInt7002]) + (((0xff & aByteArray7019[-2 + anInt7002]) << 16) + (aByteArray7019[-1 + anInt7002] << 24 & -16777216)) + ((0xff & aByteArray7019[-3 + anInt7002]) << 8);
	}
	
	final int readIntInverse() {
		anInt7002 += 4;
		anInt7006++;
		return ((aByteArray7019[anInt7002 + -1] & 0xff) << 16) + ((aByteArray7019[anInt7002 - 2] << 24 & -16777216) - (-((aByteArray7019[-4 + anInt7002] & 0xff) << 8) + -(aByteArray7019[-3 + anInt7002] & 0xff)));
	}
	
	final void writeByteInverse(int i) {
		aByteArray7019[anInt7002++] = (byte) -i;
		anInt6973++;
	}
	
	final void writeInt(int i) {
		aByteArray7019[anInt7002++] = (byte) (i >> 24);
		anInt6980++;
		aByteArray7019[anInt7002++] = (byte) (i >> 16);
		aByteArray7019[anInt7002++] = (byte) (i >> 8);
		aByteArray7019[anInt7002++] = (byte) i;
	}
	
	final String method2180() {
		anInt7025++;
		byte b_0_ = aByteArray7019[anInt7002++];
		if (b_0_ != 0) {
			throw new IllegalStateException("Bad version number in gjstr2");
		}
		int i = anInt7002;
		while (aByteArray7019[anInt7002++] != 0) {
			/* empty */
		}
		int i_2_ = anInt7002 + -i + -1;
		if (i_2_ == 0) {
			return "";
		}
		return Class184.method1846(i, aByteArray7019, i_2_, (byte) -127);
	}
	
	final void method2181(int i, int i_3_, byte[] bs, int i_4_) {
		for (int i_5_ = i; i_3_ + i > i_5_; i_5_++)
			bs[i_5_] = aByteArray7019[anInt7002++];
		if (i_4_ == -19417) {
			anInt6959++;
		}
	}
	
	final int readSmarts() {
		anInt6964++;
		int i = 0;
		int i_6_;
		for (i_6_ = readSmart(); i_6_ == 32767; i_6_ = readSmart())
			i += 32767;
		i += i_6_;
		return i;
	}
	
	final int readSmart32(boolean bool) {
		anInt6982++;
		if (aByteArray7019[anInt7002] < 0) {
			return readInt() & 0x7fffffff;
		}
		if (bool) {
			return -12;
		}
		int i = readShort(-130546744);
		if (i == 32767) {
			return -1;
		}
		return i;
	}
	
	final void writeIntInverseMiddle(int i) {
		anInt6955++;
		aByteArray7019[anInt7002++] = (byte) (i >> 16);
		aByteArray7019[anInt7002++] = (byte) (i >> 24);
		aByteArray7019[anInt7002++] = (byte) i;
		aByteArray7019[anInt7002++] = (byte) (i >> 8);
	}
	
	final byte readByteSubtract() {
		anInt6994++;
		return (byte) (128 + -aByteArray7019[anInt7002++]);
	}
	
	final int readInt() {
		anInt7002 += 4;
		anInt6971++;
		return ((0xff & aByteArray7019[anInt7002 + -4]) << 24) - (-((0xff & aByteArray7019[-3 + anInt7002]) << 16) - (aByteArray7019[anInt7002 - 2] << 8 & 0xff00) - (aByteArray7019[-1 + anInt7002] & 0xff));
	}
	
	final void writeIntMiddle(boolean bool, int i) {
		anInt7009++;
		aByteArray7019[anInt7002++] = (byte) (i >> 8);
		aByteArray7019[anInt7002++] = (byte) i;
		aByteArray7019[anInt7002++] = (byte) (i >> 24);
		aByteArray7019[anInt7002++] = (byte) (i >> 16);
		if (!bool) {
			writeIntInverseMiddle(30);
		}
	}
	
	final int method2188(int i) {
		if (i <= 110) {
			return 97;
		}
		anInt7008++;
		anInt7002 += 4;
		return (0xff & aByteArray7019[-4 + anInt7002]) + ((aByteArray7019[anInt7002 + -1] << 24 & -16777216) + ((aByteArray7019[-2 + anInt7002] & 0xff) << 16) + (0xff00 & aByteArray7019[anInt7002 + -3] << 8));
	}
	
	final void method2189(int i) {
		anInt6968++;
		if (i >= 0 && i < 128) {
			writeByte(i);
		} else if (i >= 0 && i < 32768) {
			writeShort(i + 32768, -112);
		} else {
			throw new IllegalArgumentException();
		}
	}
	
	final int method2190(int i) {
		anInt6979++;
		int i_10_ = Class10.method188(anInt7002, i, aByteArray7019);
		writeInt(i_10_);
		return i_10_;
	}
	
	final void method2191(int i, int i_11_) {
		aByteArray7019[anInt7002++] = (byte) i_11_;
		anInt6981++;
		if (i <= -24) {
			aByteArray7019[anInt7002++] = (byte) (i_11_ >> 8);
		}
	}
	
	final void method2192(int[] is, int i) {
		if (i <= 96) {
			anInt7023 = -75;
		}
		anInt6985++;
		int i_12_ = anInt7002 / 8;
		anInt7002 = 0;
		for (int i_13_ = 0; i_12_ > i_13_; i_13_++) {
			int i_14_ = readInt();
			int i_15_ = readInt();
			int i_16_ = -957401312;
			int i_17_ = -1640531527;
			int i_18_ = 32;
			while (i_18_-- > 0) {
				i_15_ -= (i_14_ >>> 5 ^ i_14_ << 4) + i_14_ ^ i_16_ + is[(i_16_ & 0x1834) >>> 11];
				i_16_ -= i_17_;
				i_14_ -= i_16_ + is[i_16_ & 0x3] ^ i_15_ + (i_15_ << 4 ^ i_15_ >>> 5);
			}
			anInt7002 -= 8;
			writeInt(i_14_);
			writeInt(i_15_);
		}
	}
	
	final int readUnsignedShort(int i) {
		if (i > -16) {
			method2223(17, (byte) 70, null, 80);
		}
		anInt6997++;
		anInt7002 += 2;
		int i_19_ = (0xff & aByteArray7019[-1 + anInt7002]) + ((aByteArray7019[-2 + anInt7002] & 0xff) << 8);
		if (i_19_ > 32767) {
			i_19_ -= 65536;
		}
		return i_19_;
	}
	
	final void writeShort(int i) {
		anInt6988++;
		aByteArray7019[anInt7002 + -i - 2] = (byte) (i >> 8);
		aByteArray7019[-1 + (anInt7002 - i)] = (byte) i;
	}
	
	final String readString(int i) {
		anInt6953++;
		int i_21_ = anInt7002;
		while (aByteArray7019[anInt7002++] != 0) {
			/* empty */
		}
		int i_22_ = anInt7002 + (-i_21_ - 1);
		if (i != -1) {
			return null;
		}
		if (i_22_ == 0) {
			return "";
		}
		return Class184.method1846(i_21_, aByteArray7019, i_22_, (byte) -118);
	}
	
	final long readLong() {
		anInt7003++;
		long l = 0xffffffffL & (long) method2188(114);
		long l_23_ = 0xffffffffL & (long) method2188(112);
		return l + (l_23_ << 32);
	}
	
	final int method2197() {
		anInt6975++;
		int i_24_ = aByteArray7019[anInt7002] & 0xff;
		if (i_24_ < 128) {
			return readUnsignedByte(255) - 64;
		}
		return -49152 + readShort(-130546744);
	}
	
	final void method2198(String string) {
		anInt6977++;
		int i_26_ = string.indexOf('\0');
		if (i_26_ >= 0) {
			throw new IllegalArgumentException("NUL character at " + i_26_ + " - cannot pjstr2");
		}
		aByteArray7019[anInt7002++] = (byte) 0;
		anInt7002 += Class173.method1801(string, string.length(), aByteArray7019, anInt7002);
		aByteArray7019[anInt7002++] = (byte) 0;
	}
	
	final void writeShortLittle(int i) {
		aByteArray7019[anInt7002++] = (byte) i;
		anInt7016++;
		aByteArray7019[anInt7002++] = (byte) (i >> 8);
	}
	
	final void writeLong(long l) {
		aByteArray7019[anInt7002++] = (byte) (int) (l >> 56);
		anInt6986++;
		aByteArray7019[anInt7002++] = (byte) (int) (l >> 48);
		aByteArray7019[anInt7002++] = (byte) (int) (l >> 40);
		aByteArray7019[anInt7002++] = (byte) (int) (l >> 32);
		aByteArray7019[anInt7002++] = (byte) (int) (l >> 24);
		aByteArray7019[anInt7002++] = (byte) (int) (l >> 16);
		aByteArray7019[anInt7002++] = (byte) (int) (l >> 8);
		aByteArray7019[anInt7002++] = (byte) (int) l;
	}
	
	final int method2201() {
		anInt6965++;
		if (false) {
			writeMedium((byte) -68, 84);
		}
		if (aByteArray7019[anInt7002] >= 0) {
			return readShort(-130546744);
		}
		return readInt() & 0x7fffffff;
	}
	
	final void method2202() {
		anInt7011++;
		if (aByteArray7019 != null) {
			Class111.method1137(aByteArray7019);
		}
		aByteArray7019 = null;
	}
	
	final void method2203(int[] is, int i, int i_29_) {
		anInt7012++;
		int i_30_ = anInt7002;
		anInt7002 = i;
		int i_31_ = (i_29_ - i) / 8;
		for (int i_32_ = 0; i_32_ < i_31_; i_32_++) {
			int i_33_ = readInt();
			int i_34_ = readInt();
			int i_35_ = 0;
			int i_36_ = -1640531527;
			int i_37_ = 32;
			while (i_37_-- > 0) {
				i_33_ += i_34_ + (i_34_ << 4 ^ i_34_ >>> 5) ^ i_35_ + is[0x3 & i_35_];
				i_35_ += i_36_;
				i_34_ += (i_33_ >>> 5 ^ i_33_ << 4) + i_33_ ^ i_35_ + is[i_35_ >>> 11 & -390070269];
			}
			anInt7002 -= 8;
			writeInt(i_33_);
			writeInt(i_34_);
		}
		anInt7002 = i_30_;
	}
	
	final void method2204(int i) {
		anInt6999++;
		aByteArray7019[-i + (anInt7002 + -4)] = (byte) (i >> 24);
		aByteArray7019[-3 + (-i + anInt7002)] = (byte) (i >> 16);
		aByteArray7019[anInt7002 - i + -2] = (byte) (i >> 8);
		aByteArray7019[anInt7002 + -i - 1] = (byte) i;
	}
	
	static void method2205(Node node, Node node_38_) {
		if (node_38_.aNode2799 != null) {
			node_38_.method2160((byte) 31);
		}
		anInt6960++;
		node_38_.aNode2800 = node;
		node_38_.aNode2799 = node.aNode2799;
		node_38_.aNode2799.aNode2800 = node_38_;
		node_38_.aNode2800.aNode2799 = node_38_;
	}
	
	final void method2206(int i, long l) {
		i--;
		anInt7010++;
		if (i < 0 || i > 7) {
			throw new IllegalArgumentException();
		}
		for (int i_40_ = 8 * i; i_40_ >= 0; i_40_ -= 8)
			aByteArray7019[anInt7002++] = (byte) (int) (l >> i_40_);
	}
	
	final void writeShortAddLittle(int i_41_) {
		aByteArray7019[anInt7002++] = (byte) (i_41_ + 128);
		anInt6991++;
		aByteArray7019[anInt7002++] = (byte) (i_41_ >> 8);
	}
	
	final int readIntInverseMiddle() {
		anInt7024++;
		anInt7002 += 4;
		if (false) {
			anInt7002 = -26;
		}
		return (aByteArray7019[anInt7002 + -4] << 16 & 0xff0000) + (((aByteArray7019[anInt7002 + -3] & 0xff) << 24) + ((0xff & aByteArray7019[anInt7002 + -1]) << 8)) + (aByteArray7019[anInt7002 - 2] & 0xff);
	}
	
	final int readShortLittle() {
		anInt7002 += 2;
		anInt6987++;
		return ((0xff & aByteArray7019[-1 + anInt7002]) << 8) + (0xff & aByteArray7019[anInt7002 + -2]);
	}
	
	final void writeShort(int i, int i_42_) {
		anInt6961++;
		aByteArray7019[anInt7002++] = (byte) (i >> 8);
		aByteArray7019[anInt7002++] = (byte) i;
		if (i_42_ >= -24) {
			readIntInverseMiddle();
		}
	}
	
	final int readUnsignedByteAdd(int i) {
		anInt6984++;
		if (i != 4255) {
			return -56;
		}
		return aByteArray7019[anInt7002++] + -128 & 0xff;
	}
	
	final int readShortAddLittle() {
		anInt7018++;
		anInt7002 += 2;
		int i_43_ = (aByteArray7019[anInt7002 + -1] << 8 & 0xff00) + (0xff & aByteArray7019[-2 + anInt7002] + -128);
		if (i_43_ > 32767) {
			i_43_ -= 65536;
		}
		return i_43_;
	}

	final int readByteSubtract(byte b) {
		if (b <= 97) {
			return 17;
		}
		anInt7013++;
		return 0xff & -aByteArray7019[anInt7002++] + 128;
	}
	
	final byte readByte() {
		anInt6966++;
		return aByteArray7019[anInt7002++];
	}
	
	final void writeIntLittle(int i_44_) {
		aByteArray7019[anInt7002++] = (byte) i_44_;
		anInt6978++;
		aByteArray7019[anInt7002++] = (byte) (i_44_ >> 8);
		aByteArray7019[anInt7002++] = (byte) (i_44_ >> 16);
		aByteArray7019[anInt7002++] = (byte) (i_44_ >> 24);
	}
	
	final boolean method2216() {
		anInt6995++;
		anInt7002 -= 4;
		int i_45_ = Class10.method188(anInt7002, 0, aByteArray7019);
		int i_46_ = readInt();
		return i_45_ == i_46_;
	}
	
	final void writeStringSize(int i) {
		anInt7017++;
		aByteArray7019[anInt7002 - (i + 1)] = (byte) i;
	}
	
	final int method2218() {
		anInt7002 += 2;
		anInt6976++;
		int i_48_ = (0xff & aByteArray7019[-2 + anInt7002]) + (aByteArray7019[anInt7002 - 1] << 8 & 0xff00);
		if (i_48_ > 32767) {
			i_48_ -= 65536;
		}
		return i_48_;
	}
	
	final int readShort(int i) {
		if (i != -130546744) {
			aClass192_7014 = null;
		}
		anInt6996++;
		anInt7002 += 2;
		return (0xff & aByteArray7019[-1 + anInt7002]) + ((aByteArray7019[-2 + anInt7002] & 0xff) << 8);
	}
	
	final int readMedium(int i) {
		anInt7002 += 3;
		if (i != 1819759595) {
			return 70;
		}
		anInt7005++;
		return ((aByteArray7019[anInt7002 - 2] & 0xff) << 8) + ((0xff & aByteArray7019[anInt7002 - 3]) << 16) + (aByteArray7019[-1 + anInt7002] & 0xff);
	}
	
	final void writeByteAdd(int i) {
		anInt6972++;
		aByteArray7019[anInt7002++] = (byte) (i + 128);
	}
	
	final void method2222(BigInteger biginteger, BigInteger biginteger_49_) {
		anInt7001++;
		int i_50_ = anInt7002;
		anInt7002 = 0;
		byte[] bs = new byte[i_50_];
		method2181(0, i_50_, bs, -19417);
		BigInteger biginteger_51_ = new BigInteger(bs);
		BigInteger biginteger_52_ = biginteger_51_.modPow(biginteger_49_, biginteger);
		byte[] bs_53_ = biginteger_52_.toByteArray();
		anInt7002 = 0;
		writeShort(bs_53_.length, -126);
		method2223(bs_53_.length, (byte) 4, bs_53_, 0);
	}
	
	final void method2223(int i, byte b, byte[] bs, int i_54_) {
		if (b == 4) {
			for (int i_55_ = i_54_; i + i_54_ > i_55_; i_55_++)
				aByteArray7019[anInt7002++] = bs[i_55_];
			anInt7021++;
		}
	}
	
	final int readShortAdd(int i) {
		anInt7002 += 2;
		if (i != -602457616) {
			return 12;
		}
		anInt6983++;
		return (0xff00 & aByteArray7019[anInt7002 + -2] << 8) + (-128 + aByteArray7019[anInt7002 - 1] & 0xff);
	}
	
	final void writeMedium(byte b, int i) {
		aByteArray7019[anInt7002++] = (byte) (i >> 16);
		anInt6990++;
		aByteArray7019[anInt7002++] = (byte) (i >> 8);
		if (b < 54) {
			writeIntMiddle(false, 89);
		}
		aByteArray7019[anInt7002++] = (byte) i;
	}
	
	final void writeByte(int i) {
		anInt6969++;
		aByteArray7019[anInt7002++] = (byte) i;
	}
	
	final int readSmart() {
		anInt7007++;
		if (false) {
			readByte();
		}
		int i = aByteArray7019[anInt7002] & 0xff;
		if (i < 128) {
			return readUnsignedByte(255);
		}
		return readShort(-130546744) + -32768;
	}
	
	final void writeString(String string, int i) {
		anInt6963++;
		int i_56_ = string.indexOf('\0');
		if (i_56_ >= 0) {
			throw new IllegalArgumentException("NUL character at " + i_56_ + " - cannot pjstr");
		}
		if (i <= 56) {
			method2237(99, -121);
		}
		anInt7002 += Class173.method1801(string, string.length(), aByteArray7019, anInt7002);
		aByteArray7019[anInt7002++] = (byte) 0;
	}
	
	final int method2229() {
		anInt7000++;
		anInt7002 += 3;
		int i = (0xff & aByteArray7019[-1 + anInt7002]) + (((0xff & aByteArray7019[-3 + anInt7002]) << 16) + ((aByteArray7019[-2 + anInt7002] & 0xff) << 8));
		if (i > 8388607) {
			i -= 16777216;
		}
		return i;
	}
	
	final void writeShortAdd(int i) {
		anInt6989++;
		aByteArray7019[anInt7002++] = (byte) (i >> 8);
		aByteArray7019[anInt7002++] = (byte) (128 + i);
	}
	
	final void method2231(int[] is, int i_59_) {
		anInt6970++;
		int i_61_ = anInt7002;
		anInt7002 = 5;
		int i_62_ = (-5 + i_59_) / 8;
		for (int i_63_ = 0; i_62_ > i_63_; i_63_++) {
			int i_64_ = readInt();
			int i_65_ = readInt();
			int i_66_ = -957401312;
			int i_67_ = -1640531527;
			int i_68_ = 32;
			while (i_68_-- > 0) {
				i_65_ -= (i_64_ << 4 ^ i_64_ >>> 5) + i_64_ ^ is[(0x1efb & i_66_) >>> 11] + i_66_;
				i_66_ -= i_67_;
				i_64_ -= i_65_ + (i_65_ >>> 5 ^ i_65_ << 4) ^ i_66_ + is[i_66_ & 0x3];
			}
			anInt7002 -= 8;
			writeInt(i_64_);
			writeInt(i_65_);
		}
		anInt7002 = i_61_;
	}
	
	final void method2232(int i_69_) {
		if ((i_69_ & -128) != 0) {
			if ((i_69_ & -16384) != 0) {
				if ((-2097152 & i_69_) != 0) {
					if ((-268435456 & i_69_) != 0) {
						writeByte(i_69_ >>> 28 | 0x80);
					}
					writeByte((i_69_ | 0x1012b0d2) >>> 21);
				}
				writeByte((i_69_ | 0x203f43) >>> 14);
			}
			writeByte((i_69_ | 0x403a) >>> 7);
		}
		anInt7022++;
		writeByte(0x7f & i_69_);
	}
	
	final int readUnsignedByte(int i) {
		if (i != 255) {
			return 36;
		}
		anInt6993++;
		return aByteArray7019[anInt7002++] & 0xff;
	}
	
	public static void method2234() {
		aClass192_7014 = null;
	}
	
	final long method2235() {
		anInt7015++;
		long l = 0xffffffffL & (long) readInt();
		long l_70_ = 0xffffffffL & (long) readInt();
		return l_70_ + (l << 32);
	}
	
	final byte readByteInverse() {
		anInt6974++;
		return (byte) -aByteArray7019[anInt7002++];
	}
	
	final void method2237(int i, int i_71_) {
		anInt6958++;
		if (i != 0) {
			aByteArray7019 = null;
		}
		aByteArray7019[anInt7002++] = (byte) i_71_;
		aByteArray7019[anInt7002++] = (byte) (i_71_ >> 8);
		aByteArray7019[anInt7002++] = (byte) (i_71_ >> 16);
		aByteArray7019[anInt7002++] = (byte) (i_71_ >> 24);
	}
	
	final int readUnsignedByteInverse() {
		if (false) {
			readSmart32(true);
		}
		anInt6992++;
		return 0xff & -aByteArray7019[anInt7002++];
	}
	
	final int method2239() {
		anInt6962++;
		int i_72_ = aByteArray7019[anInt7002++];
		int i_73_ = 0;
		for (/**/; i_72_ < 0; i_72_ = aByteArray7019[anInt7002++])
			i_73_ = (i_73_ | 0x7f & i_72_) << 7;
		return i_72_ | i_73_;
	}
	
	final String method2240() {
		anInt6957++;
		if (aByteArray7019[anInt7002] == 0) {
			anInt7002++;
			return null;
		}
		return readString(-1);
	}
	
	final long method2241(int i) {
		anInt6998++;
		if (i > -53) {
			return -50L;
		}
		long l = 0xffffffffL & (long) readUnsignedByte(255);
		long l_74_ = 0xffffffffL & (long) readInt();
		return l_74_ + (l << 32);
	}
	
	Buffer(int i) {
		anInt7002 = 0;
		aByteArray7019 = Class111.method1139(i);
	}
	
	final byte readByteAdd() {
		anInt6952++;
		return (byte) (aByteArray7019[anInt7002++] - 128);
	}
	
	final int readUnsignedShortAddLittle() {
		anInt6956++;
		anInt7002 += 2;
		return ((0xff & aByteArray7019[anInt7002 + -1]) << 8) + (-128 + aByteArray7019[anInt7002 + -2] & 0xff);
	}
	
	final long writeBytes(int i) {
		anInt7020++;
		if (--i < 0 || i > 7) {
			throw new IllegalArgumentException();
		}
		int i_76_ = i * 8;
		long l = 0L;
		for (/**/; i_76_ >= 0; i_76_ -= 8)
			l |= (0xffL & (long) aByteArray7019[anInt7002++]) << i_76_;
		return l;
	}
	
	Buffer(byte[] bs) {
		aByteArray7019 = bs;
		anInt7002 = 0;
	}
	
	final void writeByteSubtract(int i_77_) {
		aByteArray7019[anInt7002++] = (byte) (-i_77_ + 128);
		anInt6954++;
	}
	
	final int readMediumMiddle() {
		anInt6967++;
		anInt7002 += 3;
		return ((0xff & aByteArray7019[anInt7002 - 3]) << 16) - (-(aByteArray7019[anInt7002 - 1] << 8 & 0xff00) - (0xff & aByteArray7019[-2 + anInt7002]));
	}
}
