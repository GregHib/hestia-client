/* Class84 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */

public class Class84
{
	static int anInt1129;
	static int anInt1130;
	static int anInt1131;
	static int anInt1132;
	protected int anInt1133;
	static int anInt1134;
	protected int clippingBaseY;
	static int[] anIntArray1136 = new int[13];
	static int anInt1137;
	protected int[][] objectClippingMap;
	protected int clippingBaseX;
	static int anInt1140;
	static int anInt1141;
	static int anInt1142;
	static int anInt1143;
	static Class61 aClass61_1144 = new Class61(64);
	static int anInt1145;
	protected int anInt1146;
	static int[] anIntArray1147;
	static String aString1148 = null;
	static int anInt1149;
	static int anInt1150;
	static int anInt1151;
	static int anInt1152;
	static int anInt1153;
	
	public static void method981() {
		aString1148 = null;
		aClass61_1144 = null;
		anIntArray1136 = null;
		anIntArray1147 = null;
	}
	
	final boolean checkWallDecorationInteract(int currentX, int targetType, int targetX, int targetY, int targetRotation, int currentY, int sizeXY) {
		anInt1152++;
		if (sizeXY == 1) {
			if (targetX == currentX && currentY == targetY) {
				return true;
			}
		} else if (currentX <= targetX && -1 + sizeXY + currentX >= targetX && targetY <= -1 + sizeXY + targetY) {
			return true;
		}
		targetY -= clippingBaseY;
		currentY -= clippingBaseY;
		currentX -= clippingBaseX;
		targetX -= clippingBaseX;
		if (sizeXY == 1) {
			if (targetType == 6 || targetType == 7) {
				if (targetType == 7) {
					targetRotation = 0x3 & targetRotation + 2;
				}
				if (targetRotation == 0) {
					if (currentX == 1 + targetX && currentY == targetY && (0x80 & objectClippingMap[currentX][currentY]) == 0) {
						return true;
					}
					if (targetX == currentX && currentY == -1 + targetY && (0x2 & objectClippingMap[currentX][currentY]) == 0) {
						return true;
					}
				} else if (targetRotation == 1) {
					if (currentX == -1 + targetX && currentY == targetY && (objectClippingMap[currentX][currentY] & 0x8) == 0) {
						return true;
					}
					if (targetX == currentX && currentY == -1 + targetY && (objectClippingMap[currentX][currentY] & 0x2) == 0) {
						return true;
					}
				} else if (targetRotation == 2) {
					if (currentX == targetX - 1 && targetY == currentY && (objectClippingMap[currentX][currentY] & 0x8) == 0) {
						return true;
					}
					if (targetX == currentX && currentY == 1 + targetY && (0x20 & objectClippingMap[currentX][currentY]) == 0) {
						return true;
					}
				} else if (targetRotation == 3) {
					if (1 + targetX == currentX && currentY == targetY && (objectClippingMap[currentX][currentY] & 0x80) == 0) {
						return true;
					}
					if (targetX == currentX && currentY == 1 + targetY && (0x20 & objectClippingMap[currentX][currentY]) == 0) {
						return true;
					}
				}
			}
			if (targetType == 8) {
				if (targetX == currentX && currentY == 1 + targetY && (0x20 & objectClippingMap[currentX][currentY]) == 0) {
					return true;
				}
				if (currentX == targetX && targetY + -1 == currentY && (objectClippingMap[currentX][currentY] & 0x2) == 0) {
					return true;
				}
				if (currentX == targetX + -1 && targetY == currentY && (0x8 & objectClippingMap[currentX][currentY]) == 0) {
					return true;
				}
				return targetX + 1 == currentX && targetY == currentY && (0x80 & objectClippingMap[currentX][currentY]) == 0;
			}
		} else {
			int sizeX = sizeXY + currentX - 1;
			int sizeY = -1 + currentY + sizeXY;
			if (targetType == 6 || targetType == 7) {
				if (targetType == 7) {
					targetRotation = targetRotation + 2 & 0x3;
				}
				if (targetRotation == 0) {
					if (currentX == targetX + 1 && currentY <= targetY && targetY <= sizeY && (0x80 & objectClippingMap[currentX][targetY]) == 0) {
						return true;
					}
					if (targetX >= currentX && targetX <= sizeX && currentY == targetY + -sizeXY && (0x2 & objectClippingMap[targetX][sizeY]) == 0) {
						return true;
					}
				} else if (targetRotation == 1) {
                    if (currentX == targetX - sizeXY && targetY >= currentY && targetY <= sizeY && (objectClippingMap[sizeX][targetY] & 0x8) == 0) {
                        return true;
                    }
                    if (currentX <= targetX && targetX <= sizeX && currentY == -sizeXY + targetY && (objectClippingMap[targetX][sizeY] & 0x2) == 0) {
                        return true;
                    }
                } else if (targetRotation == 2) {
                    if (-sizeXY + targetX == currentX && targetY >= currentY && targetY <= sizeY && (objectClippingMap[sizeX][targetY] & 0x8) == 0) {
                        return true;
                    }
                    if (targetX >= currentX && sizeX >= targetX && 1 + targetY == currentY && (0x20 & objectClippingMap[targetX][currentY]) == 0) {
                        return true;
                    }
                } else if (targetRotation == 3) {
                    if (currentX == targetX + 1 && currentY <= targetY && targetY <= sizeY && (0x80 & objectClippingMap[currentX][targetY]) == 0) {
                        return true;
                    }
                    if (targetX >= currentX && sizeX >= targetX && currentY == targetY + 1 && (objectClippingMap[targetX][currentY] & 0x20) == 0) {
                        return true;
                    }
                }
            }
			if (targetType == 8) {
				if (currentX <= targetX && sizeX >= targetX && currentY == 1 + targetY && (0x20 & objectClippingMap[targetX][currentY]) == 0) {
					return true;
				}
				if (targetX >= currentX && targetX <= sizeX && currentY == -sizeXY + targetY && (0x2 & objectClippingMap[targetX][sizeY]) == 0) {
					return true;
				}
				if (currentX == targetX + -sizeXY && currentY <= targetY && targetY <= sizeY && (objectClippingMap[sizeX][targetY] & 0x8) == 0) {
					return true;
				}
				return currentX == 1 + targetX && currentY <= targetY && targetY <= sizeY && (0x80 & objectClippingMap[currentX][targetY]) == 0;
			}
		}
		return false;
	}
	
	private void method983(int i, int i_9_, int i_10_) {
		objectClippingMap[i_10_][i] = Node_Sub30.method2723(objectClippingMap[i_10_][i], i_9_ ^ 0xffffffff);
		anInt1130++;
	}
	
	final boolean checkFilledRectangularInteract(int sizeX, int targetY, int sizeY, int currentX, int targetX, int accessBlockFlag, int targetSizeY, int currentY, int targetSizeX) {
		anInt1145++;
		int srcEndX = sizeX + currentX;
		int srcEndY = sizeY + currentY;
		int destEndX = targetSizeX + targetX;
		int destEndY = targetSizeY + targetY;
		if (currentX != destEndX || (0x2 & accessBlockFlag) != 0) {
			if (srcEndX != targetX || (accessBlockFlag & 0x8) != 0) {
				if (destEndY == currentY && (0x1 & accessBlockFlag) == 0) {
					int clipX = currentX <= targetX ? targetX : currentX;
					for (int maxX = destEndX <= srcEndX ? destEndX : srcEndX; clipX < maxX; clipX++) {
						if ((objectClippingMap[-clippingBaseX + clipX][-clippingBaseY + destEndY - 1] & 0x2) == 0) {
							return true;
						}
					}
				} else if (srcEndY == targetY && (0x4 & accessBlockFlag) == 0) {
					int clipX = currentX > targetX ? currentX : targetX;
					for (int maxX = srcEndX >= destEndX ? destEndX : srcEndX; clipX < maxX; clipX++) {
						if ((0x20 & objectClippingMap[-clippingBaseX + clipX][targetY - clippingBaseY]) == 0) {
							return true;
						}
					}
				}
			} else {
				int clipY = currentY <= targetY ? targetY : currentY;
				for (int maxY = destEndY <= srcEndY ? destEndY : srcEndY; clipY < maxY; clipY++) {
					if ((0x80 & objectClippingMap[targetX - clippingBaseX][clipY + -clippingBaseY]) == 0) {
						return true;
					}
				}
			}
		} else {
			int clipY = targetY < currentY ? currentY : targetY;
			for (int maxY = destEndY <= srcEndY ? destEndY : srcEndY; clipY < maxY; clipY++) {
				if ((objectClippingMap[destEndX + (-1 + -clippingBaseX)][-clippingBaseY + clipY] & 0x8) == 0) {
					return true;
				}
			}
		}
		return false;
	}
	
	static void method985(int i, Class343 class343, int i_32_) {
		anInt1132++;
		if (Class320_Sub27.aBoolean8465) {
			i = 0;
			Class320_Sub27.aBoolean8465 = false;
		}
		if (Class20_Sub1.aClass343_5509 == null || !Class20_Sub1.aClass343_5509.method3968(class343)) {
			Class20_Sub1.aClass343_5509 = class343;
			Class181.aLong2157 = Class313.method3650();
			Class312.anInt3955 = Class290_Sub4.anInt8100 = i;
			if (Class312.anInt3955 == 0) {
				CacheNode_Sub14.method2349();
			} else {
				Class44.aClass119_670 = Class245.aClass119_3085;
				Animable_Sub3_Sub1.aFloat11014 = Animable.aFloat5932;
				Class357.aFloat4439 = Class363.aFloat4502;
				Class188_Sub2_Sub2.anInt9360 = Class320_Sub20.anInt8402;
				Class113.aFloat1439 = Class39.aFloat580;
				Class44.aClass270_669 = Class229.aClass270_2733;
				Class284.aFloat3597 = Class69.aFloat944;
				Class191.anInt2353 = Class138.anInt1726;
				CacheNode_Sub17.aFloat8847 = CacheNode_Sub16_Sub2.aFloat11082;
				Class329.aFloat4117 = Node_Sub25.aFloat7236;
				Node_Sub52.anInt7644 = Node_Sub12.anInt5455;
				if (Class245.aClass119_3085 != null) {
					if (Class245.aClass119_3085.method1222()) {
						Class44.aClass119_670 = Class245.aClass119_3085.method1224();
						Class245.aClass119_3085 = Class44.aClass119_670;
					}
					if (Class245.aClass119_3085 != null && Class20_Sub1.aClass343_5509.aClass119_4238 != Class245.aClass119_3085) {
						Class245.aClass119_3085.method1220(Class20_Sub1.aClass343_5509.aClass119_4238);
					}
				}
			}
        }
		if (i_32_ != 1) {
			method985(9, null, -91);
		}
	}
	
	final void method986(boolean bool, boolean bool_33_, int i_34_, int i_35_, int i_36_, int i_37_) {
		anInt1129++;
		i_34_ -= clippingBaseY;
		i_37_ -= clippingBaseX;
		if (i_36_ == 0) {
			if (i_35_ == 0) {
				method995(i_37_, i_34_, 128);
				method995(-1 + i_37_, i_34_, 8);
			}
			if (i_35_ == 1) {
				method995(i_37_, i_34_, 2);
				method995(i_37_, i_34_ + 1, 32);
			}
			if (i_35_ == 2) {
				method995(i_37_, i_34_, 8);
				method995(1 + i_37_, i_34_, 128);
			}
			if (i_35_ == 3) {
				method995(i_37_, i_34_, 32);
				method995(i_37_, i_34_ - 1, 2);
			}
		}
		if (i_36_ == 1 || i_36_ == 3) {
			if (i_35_ == 0) {
				method995(i_37_, i_34_, 1);
				method995(i_37_ - 1, i_34_ + 1, 16);
			}
			if (i_35_ == 1) {
				method995(i_37_, i_34_, 4);
				method995(i_37_ + 1, 1 + i_34_, 64);
			}
			if (i_35_ == 2) {
				method995(i_37_, i_34_, 16);
				method995(i_37_ + 1, -1 + i_34_, 1);
			}
			if (i_35_ == 3) {
				method995(i_37_, i_34_, 64);
				method995(-1 + i_37_, -1 + i_34_, 4);
			}
		}
		if (i_36_ == 2) {
			if (i_35_ == 0) {
				method995(i_37_, i_34_, 130);
				method995(i_37_ - 1, i_34_, 8);
				method995(i_37_, 1 + i_34_, 32);
			}
			if (i_35_ == 1) {
				method995(i_37_, i_34_, 10);
				method995(i_37_, i_34_ + 1, 32);
				method995(i_37_ + 1, i_34_, 128);
			}
			if (i_35_ == 2) {
				method995(i_37_, i_34_, 40);
				method995(i_37_ + 1, i_34_, 128);
				method995(i_37_, i_34_ + -1, 2);
			}
			if (i_35_ == 3) {
				method995(i_37_, i_34_, 160);
				method995(i_37_, -1 + i_34_, 2);
				method995(i_37_ - 1, i_34_, 8);
			}
		}
		if (bool) {
			if (i_36_ == 0) {
				if (i_35_ == 0) {
					method995(i_37_, i_34_, 65536);
					method995(-1 + i_37_, i_34_, 4096);
				}
				if (i_35_ == 1) {
					method995(i_37_, i_34_, 1024);
					method995(i_37_, i_34_ + 1, 16384);
				}
				if (i_35_ == 2) {
					method995(i_37_, i_34_, 4096);
					method995(1 + i_37_, i_34_, 65536);
				}
				if (i_35_ == 3) {
					method995(i_37_, i_34_, 16384);
					method995(i_37_, i_34_ + -1, 1024);
				}
			}
			if (i_36_ == 1 || i_36_ == 3) {
				if (i_35_ == 0) {
					method995(i_37_, i_34_, 512);
					method995(i_37_ - 1, i_34_ + 1, 8192);
				}
				if (i_35_ == 1) {
					method995(i_37_, i_34_, 2048);
					method995(i_37_ + 1, 1 + i_34_, 32768);
				}
				if (i_35_ == 2) {
					method995(i_37_, i_34_, 8192);
					method995(1 + i_37_, i_34_ + -1, 512);
				}
				if (i_35_ == 3) {
					method995(i_37_, i_34_, 32768);
					method995(i_37_ - 1, -1 + i_34_, 2048);
				}
			}
			if (i_36_ == 2) {
				if (i_35_ == 0) {
					method995(i_37_, i_34_, 66560);
					method995(-1 + i_37_, i_34_, 4096);
					method995(i_37_, i_34_ + 1, 16384);
				}
				if (i_35_ == 1) {
					method995(i_37_, i_34_, 5120);
					method995(i_37_, 1 + i_34_, 16384);
					method995(1 + i_37_, i_34_, 65536);
				}
				if (i_35_ == 2) {
					method995(i_37_, i_34_, 20480);
					method995(1 + i_37_, i_34_, 65536);
					method995(i_37_, -1 + i_34_, 1024);
				}
				if (i_35_ == 3) {
					method995(i_37_, i_34_, 81920);
					method995(i_37_, -1 + i_34_, 1024);
					method995(-1 + i_37_, i_34_, 4096);
				}
			}
		}
		if (bool_33_) {
			if (i_36_ == 0) {
				if (i_35_ == 0) {
					method995(i_37_, i_34_, 536870912);
					method995(i_37_ - 1, i_34_, 33554432);
				}
				if (i_35_ == 1) {
					method995(i_37_, i_34_, 8388608);
					method995(i_37_, 1 + i_34_, 134217728);
				}
				if (i_35_ == 2) {
					method995(i_37_, i_34_, 33554432);
					method995(i_37_ + 1, i_34_, 536870912);
				}
				if (i_35_ == 3) {
					method995(i_37_, i_34_, 134217728);
					method995(i_37_, i_34_ + -1, 8388608);
				}
			}
			if (i_36_ == 1 || i_36_ == 3) {
				if (i_35_ == 0) {
					method995(i_37_, i_34_, 4194304);
					method995(-1 + i_37_, i_34_ + 1, 67108864);
				}
				if (i_35_ == 1) {
					method995(i_37_, i_34_, 16777216);
					method995(1 + i_37_, i_34_ + 1, 268435456);
				}
				if (i_35_ == 2) {
					method995(i_37_, i_34_, 67108864);
					method995(i_37_ + 1, -1 + i_34_, 4194304);
				}
				if (i_35_ == 3) {
					method995(i_37_, i_34_, 268435456);
					method995(i_37_ - 1, i_34_ + -1, 16777216);
				}
			}
			if (i_36_ == 2) {
				if (i_35_ == 0) {
					method995(i_37_, i_34_, 545259520);
					method995(-1 + i_37_, i_34_, 33554432);
					method995(i_37_, i_34_ + 1, 134217728);
				}
				if (i_35_ == 1) {
					method995(i_37_, i_34_, 41943040);
					method995(i_37_, 1 + i_34_, 134217728);
					method995(i_37_ + 1, i_34_, 536870912);
				}
				if (i_35_ == 2) {
					method995(i_37_, i_34_, 167772160);
					method995(i_37_ + 1, i_34_, 536870912);
					method995(i_37_, -1 + i_34_, 8388608);
				}
				if (i_35_ == 3) {
					method995(i_37_, i_34_, 671088640);
					method995(i_37_, i_34_ + -1, 8388608);
					method995(-1 + i_37_, i_34_, 33554432);
				}
			}
		}
	}
	
	final void method987(int i, int i_39_, int i_40_, boolean bool, int i_41_, boolean bool_42_) {
		i_41_ -= clippingBaseX;
		i -= clippingBaseY;
		anInt1153++;
		if (i_40_ == 0) {
			if (i_39_ == 0) {
				method983(i, 128, i_41_);
				method983(i, 8, i_41_ + -1);
			}
			if (i_39_ == 1) {
				method983(i, 2, i_41_);
				method983(1 + i, 32, i_41_);
			}
			if (i_39_ == 2) {
				method983(i, 8, i_41_);
				method983(i, 128, 1 + i_41_);
			}
			if (i_39_ == 3) {
				method983(i, 32, i_41_);
				method983(-1 + i, 2, i_41_);
			}
		}
		if (i_40_ == 1 || i_40_ == 3) {
			if (i_39_ == 0) {
				method983(i, 1, i_41_);
				method983(i + 1, 16, -1 + i_41_);
			}
			if (i_39_ == 1) {
				method983(i, 4, i_41_);
				method983(i + 1, 64, i_41_ + 1);
			}
			if (i_39_ == 2) {
				method983(i, 16, i_41_);
				method983(-1 + i, 1, i_41_ + 1);
			}
			if (i_39_ == 3) {
				method983(i, 64, i_41_);
				method983(-1 + i, 4, -1 + i_41_);
			}
		}
		if (i_40_ == 2) {
			if (i_39_ == 0) {
				method983(i, 130, i_41_);
				method983(i, 8, -1 + i_41_);
				method983(1 + i, 32, i_41_);
			}
			if (i_39_ == 1) {
				method983(i, 10, i_41_);
				method983(i + 1, 32, i_41_);
				method983(i, 128, 1 + i_41_);
			}
			if (i_39_ == 2) {
				method983(i, 40, i_41_);
				method983(i, 128, i_41_ + 1);
				method983(-1 + i, 2, i_41_);
			}
			if (i_39_ == 3) {
				method983(i, 160, i_41_);
				method983(-1 + i, 2, i_41_);
				method983(i, 8, i_41_ - 1);
			}
		}
		if (bool_42_) {
			if (i_40_ == 0) {
				if (i_39_ == 0) {
					method983(i, 65536, i_41_);
					method983(i, 4096, -1 + i_41_);
				}
				if (i_39_ == 1) {
					method983(i, 1024, i_41_);
					method983(1 + i, 16384, i_41_);
				}
				if (i_39_ == 2) {
					method983(i, 4096, i_41_);
					method983(i, 65536, 1 + i_41_);
				}
				if (i_39_ == 3) {
					method983(i, 16384, i_41_);
					method983(i + -1, 1024, i_41_);
				}
			}
			if (i_40_ == 1 || i_40_ == 3) {
				if (i_39_ == 0) {
					method983(i, 512, i_41_);
					method983(1 + i, 8192, i_41_ - 1);
				}
				if (i_39_ == 1) {
					method983(i, 2048, i_41_);
					method983(i + 1, 32768, i_41_ + 1);
				}
				if (i_39_ == 2) {
					method983(i, 8192, i_41_);
					method983(i + -1, 512, 1 + i_41_);
				}
				if (i_39_ == 3) {
					method983(i, 32768, i_41_);
					method983(-1 + i, 2048, -1 + i_41_);
				}
			}
			if (i_40_ == 2) {
				if (i_39_ == 0) {
					method983(i, 66560, i_41_);
					method983(i, 4096, -1 + i_41_);
					method983(1 + i, 16384, i_41_);
				}
				if (i_39_ == 1) {
					method983(i, 5120, i_41_);
					method983(i + 1, 16384, i_41_);
					method983(i, 65536, 1 + i_41_);
				}
				if (i_39_ == 2) {
					method983(i, 20480, i_41_);
					method983(i, 65536, 1 + i_41_);
					method983(-1 + i, 1024, i_41_);
				}
				if (i_39_ == 3) {
					method983(i, 81920, i_41_);
					method983(i - 1, 1024, i_41_);
					method983(i, 4096, i_41_ - 1);
				}
			}
		}
		if (bool) {
			if (i_40_ == 0) {
				if (i_39_ == 0) {
					method983(i, 536870912, i_41_);
					method983(i, 33554432, i_41_ + -1);
				}
				if (i_39_ == 1) {
					method983(i, 8388608, i_41_);
					method983(i + 1, 134217728, i_41_);
				}
				if (i_39_ == 2) {
					method983(i, 33554432, i_41_);
					method983(i, 536870912, 1 + i_41_);
				}
				if (i_39_ == 3) {
					method983(i, 134217728, i_41_);
					method983(i + -1, 8388608, i_41_);
				}
			}
			if (i_40_ == 1 || i_40_ == 3) {
				if (i_39_ == 0) {
					method983(i, 4194304, i_41_);
					method983(1 + i, 67108864, i_41_ + -1);
				}
				if (i_39_ == 1) {
					method983(i, 16777216, i_41_);
					method983(i + 1, 268435456, 1 + i_41_);
				}
				if (i_39_ == 2) {
					method983(i, 67108864, i_41_);
					method983(i - 1, 4194304, i_41_ + 1);
				}
				if (i_39_ == 3) {
					method983(i, 268435456, i_41_);
					method983(i + -1, 16777216, i_41_ - 1);
				}
			}
			if (i_40_ == 2) {
				if (i_39_ == 0) {
					method983(i, 545259520, i_41_);
					method983(i, 33554432, -1 + i_41_);
					method983(i + 1, 134217728, i_41_);
				}
				if (i_39_ == 1) {
					method983(i, 41943040, i_41_);
					method983(i + 1, 134217728, i_41_);
					method983(i, 536870912, i_41_ + 1);
				}
				if (i_39_ == 2) {
					method983(i, 167772160, i_41_);
					method983(i, 536870912, 1 + i_41_);
					method983(-1 + i, 8388608, i_41_);
				}
				if (i_39_ == 3) {
					method983(i, 671088640, i_41_);
					method983(i - 1, 8388608, i_41_);
					method983(i, 33554432, i_41_ - 1);
				}
			}
		}
	}
	
	final boolean method988(int i, int i_44_, int i_45_, int i_46_, int i_47_, int i_48_, int i_49_, int i_50_, int i_51_) {
		anInt1150++;
		if (i_48_ > 1) {
			if (Class150_Sub2.checkRectangleInteract(i_46_, i_48_, i_47_, -119, i_50_, i_49_, i_51_, i, i_48_)) {
				return true;
			}
			return checkFilledRectangularInteract(i_48_, i, i_48_, i_47_, i_46_, i_45_, i_49_, i_51_, i_50_);
		}
		if (i_44_ != -7734) {
			method997(-56, 111);
		}
		int i_52_ = i_50_ + i_46_ + -1;
		int i_53_ = i_49_ + i - 1;
		if (i_46_ <= i_47_ && i_47_ <= i_52_ && i_51_ >= i && i_51_ <= i_53_) {
			return true;
		}
		if (i_46_ - 1 == i_47_ && i <= i_51_ && i_51_ <= i_53_ && (0x8 & objectClippingMap[i_47_ + -clippingBaseX][-clippingBaseY + i_51_]) == 0 && (0x8 & i_45_) == 0) {
			return true;
		}
		if (i_47_ == 1 + i_52_ && i_51_ >= i && i_53_ >= i_51_ && (objectClippingMap[i_47_ - clippingBaseX][i_51_ + -clippingBaseY] & 0x80) == 0 && (i_45_ & 0x2) == 0) {
			return true;
		}
		if (i_51_ == -1 + i && i_47_ >= i_46_ && i_52_ >= i_47_ && (0x2 & objectClippingMap[i_47_ - clippingBaseX][-clippingBaseY + i_51_]) == 0 && (0x4 & i_45_) == 0) {
			return true;
		}
        return i_51_ == 1 + i_53_ && i_46_ <= i_47_ && i_52_ >= i_47_ && (0x20 & objectClippingMap[-clippingBaseX + i_47_][-clippingBaseY + i_51_]) == 0 && (0x1 & i_45_) == 0;
    }
	
	final boolean checkWallInteract(int sizeXY, int targetType, int targetY, int currentY, int currentX, int targetX, int rotation, int i_60_) {
		anInt1140++;
		if (sizeXY != 1) {
			if (targetX >= currentX && currentX + sizeXY - 1 >= targetX && targetY <= -1 + targetY + sizeXY) {
				return true;
			}
		} else if (targetX == currentX && currentY == targetY) {
			return true;
		}
		currentX -= clippingBaseX;
		targetY -= clippingBaseY;
		targetX -= clippingBaseX;
		currentY -= clippingBaseY;
		if (i_60_ != 2883842) {
			objectClippingMap = null;
		}
		if (sizeXY == 1) {
			if (targetType == 0) {
				if (rotation == 0) {
					if (currentX == targetX - 1 && targetY == currentY) {
						return true;
					}
					if (currentX == targetX && 1 + targetY == currentY && (0x2c0120 & objectClippingMap[currentX][currentY]) == 0) {
						return true;
					}
					if (targetX == currentX && currentY == targetY - 1 && (0x2c0102 & objectClippingMap[currentX][currentY]) == 0) {
						return true;
					}
				} else if (rotation == 1) {
                    if (currentX == targetX && targetY + 1 == currentY) {
                        return true;
                    }
                    if (-1 + targetX == currentX && targetY == currentY && (0x2c0108 & objectClippingMap[currentX][currentY]) == 0) {
                        return true;
                    }
                    if (targetX + 1 == currentX && currentY == targetY && (objectClippingMap[currentX][currentY] & 0x2c0180) == 0) {
                        return true;
                    }
                } else if (rotation == 2) {
                    if (currentX == targetX + 1 && targetY == currentY) {
                        return true;
                    }
                    if (currentX == targetX && 1 + targetY == currentY && (0x2c0120 & objectClippingMap[currentX][currentY]) == 0) {
                        return true;
                    }
                    if (currentX == targetX && -1 + targetY == currentY && (0x2c0102 & objectClippingMap[currentX][currentY]) == 0) {
                        return true;
                    }
                } else if (rotation == 3) {
                    if (targetX == currentX && currentY == -1 + targetY) {
                        return true;
                    }
                    if (targetX - 1 == currentX && currentY == targetY && (objectClippingMap[currentX][currentY] & 0x2c0108) == 0) {
                        return true;
                    }
                    if (currentX == targetX + 1 && targetY == currentY && (objectClippingMap[currentX][currentY] & 0x2c0180) == 0) {
                        return true;
                    }
                }
            }
			if (targetType == 2) {
				if (rotation == 0) {
					if (-1 + targetX == currentX && currentY == targetY) {
						return true;
					}
					if (currentX == targetX && currentY == targetY + 1) {
						return true;
					}
					if (1 + targetX == currentX && targetY == currentY && (0x2c0180 & objectClippingMap[currentX][currentY]) == 0) {
						return true;
					}
					if (currentX == targetX && -1 + targetY == currentY && (0x2c0102 & objectClippingMap[currentX][currentY]) == 0) {
						return true;
					}
				} else if (rotation == 1) {
                    if (currentX == targetX - 1 && currentY == targetY && (0x2c0108 & objectClippingMap[currentX][currentY]) == 0) {
                        return true;
                    }
                    if (currentX == targetX && targetY + 1 == currentY) {
                        return true;
                    }
                    if (currentX == 1 + targetX && targetY == currentY) {
                        return true;
                    }
                    if (targetX == currentX && currentY == -1 + targetY && (objectClippingMap[currentX][currentY] & 0x2c0102) == 0) {
                        return true;
                    }
                } else if (rotation == 2) {
                    if (-1 + targetX == currentX && currentY == targetY && (objectClippingMap[currentX][currentY] & 0x2c0108) == 0) {
                        return true;
                    }
                    if (targetX == currentX && currentY == 1 + targetY && (0x2c0120 & objectClippingMap[currentX][currentY]) == 0) {
                        return true;
                    }
                    if (targetX + 1 == currentX && currentY == targetY) {
                        return true;
                    }
                    if (targetX == currentX && targetY - 1 == currentY) {
                        return true;
                    }
                } else if (rotation == 3) {
                    if (currentX == targetX - 1 && currentY == targetY) {
                        return true;
                    }
                    if (targetX == currentX && currentY == targetY + 1 && (0x2c0120 & objectClippingMap[currentX][currentY]) == 0) {
                        return true;
                    }
                    if (1 + targetX == currentX && targetY == currentY && (objectClippingMap[currentX][currentY] & 0x2c0180) == 0) {
                        return true;
                    }
                    if (currentX == targetX && currentY == -1 + targetY) {
                        return true;
                    }
                }
            }
			if (targetType == 9) {
				if (targetX == currentX && 1 + targetY == currentY && (0x20 & objectClippingMap[currentX][currentY]) == 0) {
					return true;
				}
				if (currentX == targetX && targetY + -1 == currentY && (objectClippingMap[currentX][currentY] & 0x2) == 0) {
					return true;
				}
				if (-1 + targetX == currentX && currentY == targetY && (0x8 & objectClippingMap[currentX][currentY]) == 0) {
					return true;
				}
                return 1 + targetX == currentX && currentY == targetY && (objectClippingMap[currentX][currentY] & 0x80) == 0;
			}
		} else {
			int i_61_ = -1 + (sizeXY + currentX);
			int i_62_ = -1 + sizeXY + currentY;
			if (targetType == 0) {
				if (rotation == 0) {
					if (-sizeXY + targetX == currentX && currentY <= targetY && targetY <= i_62_) {
						return true;
					}
					if (targetX >= currentX && targetX <= i_61_ && 1 + targetY == currentY && (objectClippingMap[targetX][currentY] & 0x2c0120) == 0) {
						return true;
					}
					if (currentX <= targetX && i_61_ >= targetX && currentY == targetY + -sizeXY && (objectClippingMap[targetX][i_62_] & 0x2c0102) == 0) {
						return true;
					}
				} else if (rotation == 1) {
                    if (targetX >= currentX && i_61_ >= targetX && currentY == targetY + 1) {
                        return true;
                    }
                    if (-sizeXY + targetX == currentX && targetY >= currentY && i_62_ >= targetY && (objectClippingMap[i_61_][targetY] & 0x2c0108) == 0) {
                        return true;
                    }
                    if (1 + targetX == currentX && currentY <= targetY && i_62_ >= targetY && (objectClippingMap[currentX][targetY] & 0x2c0180) == 0) {
                        return true;
                    }
                } else if (rotation == 2) {
                    if (currentX == 1 + targetX && currentY <= targetY && targetY <= i_62_) {
                        return true;
                    }
                    if (currentX <= targetX && targetX <= i_61_ && currentY == targetY + 1 && (objectClippingMap[targetX][currentY] & 0x2c0120) == 0) {
                        return true;
                    }
                    if (targetX >= currentX && targetX <= i_61_ && targetY - sizeXY == currentY && (0x2c0102 & objectClippingMap[targetX][i_62_]) == 0) {
                        return true;
                    }
                } else if (rotation == 3) {
                    if (currentX <= targetX && i_61_ >= targetX && currentY == targetY + -sizeXY) {
                        return true;
                    }
                    if (-sizeXY + targetX == currentX && targetY >= currentY && i_62_ >= targetY && (objectClippingMap[i_61_][targetY] & 0x2c0108) == 0) {
                        return true;
                    }
                    if (currentX == 1 + targetX && currentY <= targetY && i_62_ >= targetY && (0x2c0180 & objectClippingMap[currentX][targetY]) == 0) {
                        return true;
                    }
                }
            }
			if (targetType == 2) {
				if (rotation == 0) {
					if (targetX + -sizeXY == currentX && targetY >= currentY && i_62_ >= targetY) {
						return true;
					}
					if (currentX <= targetX && i_61_ >= targetX && currentY == 1 + targetY) {
						return true;
					}
					if (currentX == targetX + 1 && targetY >= currentY && i_62_ >= targetY && (objectClippingMap[currentX][targetY] & 0x2c0180) == 0) {
						return true;
					}
					if (currentX <= targetX && targetX <= i_61_ && -sizeXY + targetY == currentY && (0x2c0102 & objectClippingMap[targetX][i_62_]) == 0) {
						return true;
					}
				} else if (rotation == 1) {
                    if (-sizeXY + targetX == currentX && currentY <= targetY && targetY <= i_62_ && (objectClippingMap[i_61_][targetY] & 0x2c0108) == 0) {
                        return true;
                    }
                    if (targetX >= currentX && i_61_ >= targetX && targetY + 1 == currentY) {
                        return true;
                    }
                    if (1 + targetX == currentX && targetY >= currentY && i_62_ >= targetY) {
                        return true;
                    }
                    if (targetX >= currentX && i_61_ >= targetX && currentY == targetY + -sizeXY && (objectClippingMap[targetX][i_62_] & 0x2c0102) == 0) {
                        return true;
                    }
                } else if (rotation == 2) {
                    if (currentX == targetX + -sizeXY && targetY >= currentY && targetY <= i_62_ && (0x2c0108 & objectClippingMap[i_61_][targetY]) == 0) {
                        return true;
                    }
                    if (targetX >= currentX && i_61_ >= targetX && currentY == 1 + targetY && (0x2c0120 & objectClippingMap[targetX][currentY]) == 0) {
                        return true;
                    }
                    if (targetX + 1 == currentX && targetY >= currentY && i_62_ >= targetY) {
                        return true;
                    }
                    if (currentX <= targetX && targetX <= i_61_ && currentY == targetY + -sizeXY) {
                        return true;
                    }
                } else if (rotation == 3) {
                    if (currentX == -sizeXY + targetX && currentY <= targetY && targetY <= i_62_) {
                        return true;
                    }
                    if (targetX >= currentX && i_61_ >= targetX && 1 + targetY == currentY && (0x2c0120 & objectClippingMap[targetX][currentY]) == 0) {
                        return true;
                    }
                    if (currentX == targetX + 1 && targetY >= currentY && targetY <= i_62_ && (objectClippingMap[currentX][targetY] & 0x2c0180) == 0) {
                        return true;
                    }
                    if (currentX <= targetX && i_61_ >= targetX && targetY - sizeXY == currentY) {
                        return true;
                    }
                }
            }
			if (targetType == 9) {
				if (currentX <= targetX && targetX <= i_61_ && currentY == 1 + targetY && (objectClippingMap[targetX][currentY] & 0x2c0120) == 0) {
					return true;
				}
				if (targetX >= currentX && i_61_ >= targetX && -sizeXY + targetY == currentY && (0x2c0102 & objectClippingMap[targetX][i_62_]) == 0) {
					return true;
				}
				if (-sizeXY + targetX == currentX && targetY >= currentY && targetY <= i_62_ && (objectClippingMap[i_61_][targetY] & 0x2c0108) == 0) {
					return true;
				}
                return targetX + 1 == currentX && targetY >= currentY && i_62_ >= targetY && (0x2c0180 & objectClippingMap[currentX][targetY]) == 0;
			}
		}
        return false;
	}
	
	final void method990(int i, int i_63_) {
		anInt1142++;
		i_63_ -= clippingBaseX;
		i -= clippingBaseY;
		objectClippingMap[i_63_][i] = Node_Sub16.method2590(objectClippingMap[i_63_][i], 2097152);
	}
	
	final void method991(int i) {
		anInt1143++;
		for (int i_65_ = 0; i_65_ < anInt1133; i_65_++) {
			for (int i_66_ = 0; i_66_ < anInt1146; i_66_++) {
				if (i_65_ != 0 && i_66_ != 0 && -5 + anInt1133 > i_65_ && anInt1146 - 5 > i_66_) {
					objectClippingMap[i_65_][i_66_] = 2097152;
				} else {
					objectClippingMap[i_65_][i_66_] = -1;
				}
			}
		}
		if (i >= -86) {
			method988(-4, -128, 38, 51, 96, -103, 64, 8, -26);
		}
	}
	
	final void method992(boolean bool, boolean bool_67_, int i, int i_68_, int i_69_, int i_70_, int i_71_) {
		anInt1134++;
		int i_72_ = 256;
		if (bool) {
			i_72_ |= 0x20000;
		}
		i_71_ -= clippingBaseY;
		i -= clippingBaseX;
		if (i_70_ == 1 || i_70_ == 3) {
			int i_73_ = i_68_;
			i_68_ = i_69_;
			i_69_ = i_73_;
		}
		if (bool_67_) {
			i_72_ |= 0x40000000;
		}
		int i_74_ = i;
		for (/**/; i_74_ < i_68_ + i; i_74_++) {
			if (i_74_ >= 0 && anInt1133 > i_74_) {
				for (int i_76_ = i_71_; i_71_ + i_69_ > i_76_; i_76_++) {
					if (i_76_ >= 0 && anInt1146 > i_76_) {
						method983(i_76_, i_72_, i_74_);
					}
				}
			}
		}
	}
	
	final void method993(int i, int i_77_, int i_78_) {
		if (i_78_ > -109) {
			anIntArray1147 = null;
		}
		i -= clippingBaseY;
		anInt1137++;
		i_77_ -= clippingBaseX;
		objectClippingMap[i_77_][i] = Node_Sub30.method2723(objectClippingMap[i_77_][i], -2097153);
	}
	
	final void method994(int i, int i_79_) {
		anInt1141++;
		i_79_ -= clippingBaseX;
		i -= clippingBaseY;
		objectClippingMap[i_79_][i] = Node_Sub16.method2590(objectClippingMap[i_79_][i], 262144);
	}
	
	private void method995(int i, int i_82_, int i_83_) {
		objectClippingMap[i][i_82_] = Node_Sub16.method2590(objectClippingMap[i][i_82_], i_83_);
		anInt1131++;
	}
	
	final void method996(int i, int i_86_, int i_88_, boolean bool, int i_89_, boolean bool_90_) {
		anInt1151++;
		int i_91_ = 256;
		if (bool_90_) {
			i_91_ |= 0x20000;
		}
		if (bool) {
			i_91_ |= 0x40000000;
		}
		i_88_ -= clippingBaseY;
		i -= clippingBaseX;
		for (int i_92_ = i; i + i_89_ > i_92_; i_92_++) {
			if (i_92_ >= 0 && i_92_ < anInt1133) {
				for (int i_93_ = i_88_; i_93_ < i_86_ + i_88_; i_93_++) {
					if (i_93_ >= 0 && i_93_ < anInt1146) {
						method995(i_92_, i_93_, i_91_);
					}
				}
			}
		}
	}
	
	final void method997(int i, int i_94_) {
		i -= clippingBaseY;
		i_94_ -= clippingBaseX;
		anInt1149++;
		objectClippingMap[i_94_][i] = Node_Sub30.method2723(objectClippingMap[i_94_][i], -262145);
	}
	
	Class84() {
		/* empty */
	}
	
	static {
		anIntArray1147 = new int[2];
	}
}
