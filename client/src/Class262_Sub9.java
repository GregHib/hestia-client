/* Class262_Sub9 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.Canvas;
import java.awt.Dimension;

public class Class262_Sub9 extends Class262
{
	private int anInt7758;
	private int anInt7759;
	static int anInt7760;
	private int anInt7761;
	private int anInt7762;
	static int anInt7763;
	private int anInt7764;
	static Class318 aClass318_7765 = new Class318(4, 8);
	private int anInt7766;
	
	final void method3148(int i) {
		Class121.aClass206Array1529[anInt7759].method2037(-61).method852(anInt7766, 0, anInt7761, (byte) -119, anInt7764, Class174.anInt2092, anInt7762, anInt7758);
		if (i >= -102) {
			method3171(null, null, -111, null, 38);
		}
		anInt7763++;
	}
	
	public static void method3170() {
		aClass318_7765 = null;
	}
	
	static GraphicsToolkit method3171(Class302 class302, d var_d, int i, Canvas canvas, int i_0_) {
		anInt7760++;
		int i_2_ = 0;
		int i_3_ = 0;
		if (canvas != null) {
			Dimension dimension = canvas.getSize();
			i_2_ = dimension.width;
			i_3_ = dimension.height;
		}
		return GraphicsToolkit.method1240(canvas, i_3_, i_0_, var_d, i, class302, i_2_);
	}
	
	Class262_Sub9(Buffer buffer) {
		super(buffer);
		anInt7759 = buffer.readShort(-130546744);
		int i = buffer.readUnsignedByte(255);
		if ((i & 0x1) == 0) {
			anInt7758 = -1;
			anInt7764 = -1;
		} else {
			anInt7758 = buffer.readShort(-130546744);
			anInt7764 = buffer.readShort(-130546744);
		}
        if ((0x2 & i) == 0) {
            anInt7766 = -1;
            anInt7762 = -1;
        } else {
            anInt7762 = buffer.readShort(-130546744);
            anInt7766 = buffer.readShort(-130546744);
        }
        if ((0x4 & i) == 0) {
			anInt7761 = -1;
		} else {
			int i_4_ = buffer.readShort(-130546744);
			int i_5_ = buffer.readShort(-130546744);
			int i_6_ = i_4_ * 255 / i_5_;
			if (i_4_ > 0 && i_6_ < 1) {
				i_6_ = 1;
			}
			anInt7761 = i_6_;
		}
	}
}
