/* Class328_Sub1 - Decompiled by JODE
 * Visit http://jode.sourceforge.net/
 */
import java.awt.*;
import java.util.Random;

public class Class328_Sub1 extends Class328
{
	static int anInt8500;
	static int anInt8501;
	private int[] anIntArray8502 = new int[512];
	static int anInt8503;
	static int[] anIntArray8504 = new int[50];

	static Class93 method3830(Component component) {
		anInt8503++;
		try {
			return new Class93_Sub1(component, true);
		} catch (Throwable throwable) {
			return new Class93_Sub2(component, true);
		}
	}
	
	public static void method3831() {
		anIntArray8504 = null;
	}
	
	final void method3826(float f, float f_0_, int i, float[] fs, int i_1_, float f_2_, int i_3_, float f_4_, int i_5_, int i_6_) {
		anInt8500++;
		int i_7_ = (int) (-1.0f + f_4_ * (float) i);
		i_7_ &= 0xff;
		int i_9_ = (int) ((float) i_6_ * f - 1.0F);
		i_9_ &= 0xff;
		int i_10_ = (int) (-1.0f + (float) i_5_ * f_2_);
		i_10_ &= 0xff;
		float f_11_ = (float) i_3_ * f_2_;
		int i_12_ = (int) f_11_;
		int i_13_ = i_12_ + 1;
		float f_14_ = f_11_ - (float) i_12_;
		float f_15_ = -f_14_ + 1.0F;
		i_12_ &= i_10_;
		float f_16_ = Class146.method1646((byte) -115, f_14_);
		i_13_ &= i_10_;
		int i_17_ = anIntArray8502[i_12_];
		int i_18_ = anIntArray8502[i_13_];
		for (int i_19_ = 0; i_19_ < i_6_; i_19_++) {
			float f_20_ = f * (float) i_19_;
			int i_21_ = (int) f_20_;
			int i_22_ = i_21_ + 1;
			float f_23_ = f_20_ - (float) i_21_;
			float f_24_ = 1.0F - f_23_;
			float f_25_ = Class146.method1646((byte) -115, f_23_);
			i_21_ &= i_9_;
			i_22_ &= i_9_;
			int i_26_ = anIntArray8502[i_17_ + i_21_];
			int i_27_ = anIntArray8502[i_17_ + i_22_];
			int i_28_ = anIntArray8502[i_21_ + i_18_];
			int i_29_ = anIntArray8502[i_22_ + i_18_];
			for (int i_30_ = 0; i_30_ < i; i_30_++) {
				float f_31_ = (float) i_30_ * f_4_;
				int i_32_ = (int) f_31_;
				int i_33_ = i_32_ + 1;
				float f_34_ = (float) -i_32_ + f_31_;
				float f_35_ = 1.0F - f_34_;
				i_33_ &= i_7_;
				i_32_ &= i_7_;
				float f_36_ = Class146.method1646((byte) -128, f_34_);
				fs[i_1_++] = f_0_ * GraphicsToolkit.method1244(GraphicsToolkit.method1244(GraphicsToolkit.method1244(Class150.method1654(f_14_, f_34_, f_23_, Node_Sub30.method2723(7, anIntArray8502[i_33_ + i_29_])), Class150.method1654(f_14_, f_35_, f_23_, Node_Sub30.method2723(anIntArray8502[i_32_ + i_29_], 7)), f_36_), GraphicsToolkit.method1244(Class150.method1654(f_14_, f_34_, f_24_, Node_Sub30.method2723(7, anIntArray8502[i_28_ + i_33_])), Class150.method1654(f_14_, f_35_, f_24_, Node_Sub30.method2723(7, anIntArray8502[i_32_ + i_28_])), f_36_), f_25_), GraphicsToolkit.method1244(GraphicsToolkit.method1244(Class150.method1654(f_15_, f_34_, f_23_, Node_Sub30.method2723(7, anIntArray8502[i_27_ + i_33_])), Class150.method1654(f_15_, f_35_, f_23_, Node_Sub30.method2723(7, anIntArray8502[i_27_ + i_32_])), f_36_), GraphicsToolkit.method1244(Class150.method1654(f_15_, f_34_, f_24_, Node_Sub30.method2723(anIntArray8502[i_26_ + i_33_], 7)), Class150.method1654(f_15_, f_35_, f_24_, Node_Sub30.method2723(anIntArray8502[i_32_ + i_26_], 7)), f_36_), f_25_), f_16_);
			}
		}
	}
	
	Class328_Sub1() {
		Random random = new Random((long) 419684);
		for (int i_37_ = 0; i_37_ < 256; i_37_++)
			anIntArray8502[i_37_] = anIntArray8502[256 + i_37_] = i_37_;
		for (int i_38_ = 0; i_38_ < 256; i_38_++) {
			int i_39_ = 0xff & random.nextInt();
			int i_40_ = anIntArray8502[i_39_];
			anIntArray8502[i_39_] = anIntArray8502[256 + i_39_] = anIntArray8502[i_38_];
			anIntArray8502[i_38_] = anIntArray8502[i_38_ + 256] = i_40_;
		}
	}
}
